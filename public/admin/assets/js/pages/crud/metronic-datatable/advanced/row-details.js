"use strict";
// Class definition

var KTDatatableAutoColumnHideDemo = function() {
	// Private functions

	// basic demo
	var demo = function() {

		var datatable = $('.kt-datatable').KTDatatable({
			// datasource definition
			data: {
				type: 'remote',
				source: {
					read: {
						url: '/Owner/jsondata',
						method:'GET',
					},
				},
				pageSize: 10,
				saveState: false,
				serverPaging: true,
				serverFiltering: true,
				serverSorting: true,
			},

			layout: {
				scroll: true,
				height: 550,
			},

			// column sorting
			sortable: true,

			pagination: true,

			search: {
				input: $('#generalSearch'),
			},

			// columns definition
			columns: [{
				field: '',
				title: '#',
				sortable: false,
				width: 20,
				selector: {
					class: 'kt-checkbox--solid'
				},
				textAlign: 'center',
			}, {
				field: 'owner',
				title: 'Owner Name',
			}, {
				field: 'email',
				title: 'Email',
			}, {
				field: 'contactno',
				title: 'Contact No',
			},  {
				field: 'pg_id',
				title: 'Actions',
				sortable: false,
				width: 150,
				overflow: 'visible',
				textAlign: 'center',
				autoHide: false,
				template: function(row) {
					return '\
						<a href="/Owner/remove/'+ row.pg_id +'" style="margin-right:80px;" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Delete">\
							<i class="flaticon-delete-1"></i>\
						</a>\
					';
				},
			}],
		});

    $('#kt_form_status').on('change', function() {
      datatable.search($(this).val().toLowerCase(), 'Status');
    });

    $('#kt_form_type').on('change', function() {
      datatable.search($(this).val().toLowerCase(), 'Type');
    });

    $('#kt_form_status,#kt_form_type').selectpicker();

	};

	return {
		// public functions
		init: function() {
			demo();
		},
	};
}();

jQuery(document).ready(function() {
	KTDatatableAutoColumnHideDemo.init();
});