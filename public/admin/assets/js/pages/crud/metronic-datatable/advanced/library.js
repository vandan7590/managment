"use strict";
// Class definition

var KTDatatableRecordSelectionDemo = function() {
    // Private functions

    var options = {
        // datasource definition
        data: {
            type: 'remote',
            source: {
                read: {
                    url: '/Library/Jsondata',
                    method:'GET',
                },
            },
            pageSize: 10,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
        },

        // layout definition
        layout: {
            scroll: false, // enable/disable datatable scroll both horizontal and
            // vertical when needed.
            height: 350, // datatable's body's fixed height
            footer: false // display/hide footer
        },

        // column sorting
        sortable: true,

        pagination: true,

        // columns definition

        columns: [{
            field: '',
            title: '#',
            sortable: false,
            width: 20,
            selector: {
                class: 'kt-checkbox--solid'
            },
            textAlign: 'center',
        }, {
            field: 'name',
            title: 'Name'
        }, {
            field: 'email',
            title: 'Email',
        }, {
            field: 'contact_no',
            title: 'Contact No',
        }, {
            field: 'address',
            title: 'Address',
        }, {
            field: 'status',
            title: 'Status',
            // callback function support for column rendering
            template: function(row) {
                if(row.status == "active"){
                    return '' +
                    '<span style="width: 202px;"><span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">Active</span></span>'
                }
                else{
                    return '' +
                    '<span style="width: 202px;"><span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill">Inactive</span></span>'
                }
            },
        }, {
            field: 'id',
            title: 'Actions',
            sortable: false,
            width: 150,
            overflow: 'visible',
            textAlign: 'center',
	        autoHide: false,
            template: function(row) {
	            return '\
                <a href="/Library/Profile/'+ row.id +'" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="View Profile">\
                <i class="flaticon2-user"></i>\
            </a>\
            <a href="/library/edit/'+ row.id + '" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Edit">\
                <i class="flaticon2-file"></i>\
            </a>\
            <a href="/Library/Review/' + row.id +'" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Renew">\
                <i class="flaticon2-settings"></i>\
            </a>\
            <button class="btn btn-sm btn-clean btn-icon btn-icon-sm" data-toggle="modal" data-target="#kt_modal_7" title="Delete">\
                <i class="flaticon2-delete"></i>\
            </button>\
            <div class="modal modal-stick-to-bottom fade" id="kt_modal_7" role="dialog" data-backdrop="false">\
                <div class="modal-dialog" role="document">\
                    <div class="modal-content">\
                        <div class="modal-body">\
                            <p>Are You Sure You Want To Delete ?</p>\
                                <button type="button" style="float:right" class="btn btn-default" data-dismiss="modal">No</button>\
                                <a href="/library/delete/' + row.id +'" style="float:right; margin-right: 10px;" class="btn btn-info">Yes</a>\
                        </div>\
                    </div>\
                </div>\
            </div>\
                ';
            },
        }],
    };

    // basic demo
    var localSelectorDemo = function() {

        options.search = {
            input: $('#generalSearch'),
        };

        var datatable = $('#local_record_selection').KTDatatable(options);

        $('#kt_form_status').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#kt_form_type').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Type');
        });

        $('#kt_form_status,#kt_form_type').selectpicker();

        datatable.on(
            'kt-datatable--on-check kt-datatable--on-uncheck kt-datatable--on-layout-updated',
            function(e) {
                var checkedNodes = datatable.rows('.kt-datatable__row--active').nodes();
                var count = checkedNodes.length;
                $('#kt_datatable_selected_number').html(count);
                if (count > 0) {
                    $('#kt_datatable_group_action_form').collapse('show');
                } else {
                    $('#kt_datatable_group_action_form').collapse('hide');
                }
            });

        $('#kt_modal_fetch_id').on('show.bs.modal', function(e) {
            var ids = datatable.rows('.kt-datatable__row--active').
            nodes().
            find('.kt-checkbox--single > [type="checkbox"]').
            map(function(i, chk) {
                return $(chk).val();
            });
            var c = document.createDocumentFragment();
            for (var i = 0; i < ids.length; i++) {
                var li = document.createElement('li');
                li.setAttribute('data-id', ids[i]);
                li.innerHTML = 'Selected record ID: ' + ids[i];
                c.appendChild(li);
            }
            $(e.target).find('.kt-datatable_selected_ids').append(c);
        }).on('hide.bs.modal', function(e) {
            $(e.target).find('.kt-datatable_selected_ids').empty();
        });

    };

    var serverSelectorDemo = function() {

        // enable extension
        options.extensions = {
            checkbox: {},
        };
        options.search = {
            input: $('#generalSearch1'),
        };

        var datatable = $('#server_record_selection').KTDatatable(options);

        $('#kt_form_status1').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#kt_form_type1').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Type');
        });

        $('#kt_form_status1,#kt_form_type1').selectpicker();

        datatable.on(
            'kt-datatable--on-click-checkbox kt-datatable--on-layout-updated',
            function(e) {
                // datatable.checkbox() access to extension methods
                var ids = datatable.checkbox().getSelectedId();
                var count = ids.length;
                $('#kt_datatable_selected_number1').html(count);
                if (count > 0) {
                    $('#kt_datatable_group_action_form1').collapse('show');
                } else {
                    $('#kt_datatable_group_action_form1').collapse('hide');
                }
            });

        $('#kt_modal_fetch_id_server').on('show.bs.modal', function(e) {
            var ids = datatable.checkbox().getSelectedId();
            var c = document.createDocumentFragment();
            for (var i = 0; i < ids.length; i++) {
                var li = document.createElement('li');
                li.setAttribute('data-id', ids[i]);
                li.innerHTML = 'Selected record ID: ' + ids[i];
                c.appendChild(li);
            }
            $(e.target).find('.kt-datatable_selected_ids').append(c);
        }).on('hide.bs.modal', function(e) {
            $(e.target).find('.kt-datatable_selected_ids').empty();
        });

    };

    return {
        // public functions
        init: function() {
            localSelectorDemo();
            serverSelectorDemo();
        },
    };
}();

jQuery(document).ready(function() {
    KTDatatableRecordSelectionDemo.init();
});