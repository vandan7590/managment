<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pg_id')->unsigned();
            $table->foreign('pg_id')->references('id')->on('pgs');
            $table->string('start_date')->nullable();
            $table->string('end_date')->nullable();
            $table->string('renew_reminder_date')->nullable();
            $table->string('expire_date')->nullable();
            $table->string('package')->nullable();
            $table->string('date_of_payment')->nullable();
            $table->string('date_of_renew')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
