<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePgupdatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pgupdates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id')->nullable();
            $table->string('pg_id')->nullable();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('contact_no')->nullable();
            $table->string('address')->nullable();
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('pincode')->nullable();
            $table->string('status')->nullable();
            $table->string('owner')->nullable();
            $table->string('owner_email')->nullable();
            $table->string('contactno')->nullable();
            $table->string('mgt_name')->nullable();
            $table->string('mobile_verification')->nullable();
            $table->string('email_verification')->nullable();
            $table->string('access_control')->nullable();
            $table->string('enable_feature')->nullable();
            $table->string('status_approval')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pgupdates');
    }
}
