<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryownersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('libraryowners', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('library_id')->unsigned();
            $table->foreign('library_id')->references('id')->on('libraries');
            $table->string('owner')->nullable();
            $table->string('email')->nullable();
            $table->string('contactno')->nullable();
            $table->string('verify')->nullable();
            $table->string('emailotp')->nullable();
            $table->string('contactverify')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('libraryowners');
    }
}
