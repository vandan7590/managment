<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePgmanagmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pgmanagments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pg_id')->unsigned();
            $table->foreign('pg_id')->references('id')->on('pgs')->onDelete('cascade');
            $table->string('name')->nullable();
            $table->string('mobile_verification')->nullable();
            $table->string('email_verification')->nullable();
            $table->string('access_control')->nullable();
            $table->string('enable_feature')->nullable();
            $table->string('verify')->nullable();
            $table->string('emailotp')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pgmanagments');
    }
}
