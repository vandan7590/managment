<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOwnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owners', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pg_id')->unsigned();
            $table->foreign('pg_id')->references('id')->on('pgs')->onDelete('cascade');
            $table->string('owner')->nullable();
            $table->string('email')->nullable();
            $table->string('contactno')->nullable();
            $table->string('verify')->nullable();
            $table->string('emailotp')->nullable();
            $table->string('contactverify')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owners');
    }
}
