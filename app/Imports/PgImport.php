<?php

namespace App\Imports;

use App\Pg;
use App\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class PgImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Pg([
            'user_id'=> optional(User::find($row[0]))->id,
            'unique_id' => $row[1],
            'name' => $row[2],
            'email'=> $row[3],
            'contact_no'=> $row[4],
            'address'=>$row[5],
            'pincode'=>$row[6],
            'state'=>$row[7],
            'city'=>$row[8],
            'landlineno'=>$row[9],
        ]);
    }

}
