<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = [
        'floor_id','room','bed_type','no_of_beds','status'
    ];

    public function floors()
    {
        return $this->hasMany('App\Floor','id');
    }

}
