<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pgmanagment extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'pg_id','name','mobile_verification','email_verification','access_control','enable_feature','verify'
    ];
}
