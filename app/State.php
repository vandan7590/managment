<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    
    protected $fillable = [
        'pid_id','state'];

        public function pincodes()
        {
            return $this->hasOne('App\Pincode','pid_id','id');
        }
}
