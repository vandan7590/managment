<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pincode extends Model
{
    
    protected $fillable = [
        'pincode'];

        public function states()
        {
            return $this->belongsTo('App\State','id');
        }

        public function cities()
        {
            return $this->belongsTo('App\City','id');
        }
}
