<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Libraryrenewupdate extends Model
{
    protected $fillable = [
        'library_id','start_date','end_date','renew_reminder_date','expire_date','package','date_of_payment','date_of_renew'
    ];
}
