<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{    
    protected $fillable = [
        'pid_id','city'];

        public function pincodes()
        {
            return $this->hasOne('App\Pincode','pid_id');
        }
}
