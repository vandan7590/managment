<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{ 
    protected $fillable = [
        'pg_id','doc_type','doc_name','doc_number','multiple_doc'
    ];

    public function multiple_doc()
    {
        return $this->belongsTo('App\Multipledocument','pg_id');
    }

}
