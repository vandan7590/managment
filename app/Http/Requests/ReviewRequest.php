<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_date'=>'required|after:today',
            'end_date'=>'required|after:today',
            'renew_reminder_date'=>'required',
            'expire_date'=>'required',
            'package'=>'required',
            'date_of_payment'=>'required',
            'date_of_renew'=>'required'
        ];
    }
}
