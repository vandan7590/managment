<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PgRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'=>'required|unique:pgs',
            'address'=>'required|max:1055',
            'pincode'=>'required',
        ];
    }
}
