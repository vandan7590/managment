<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ManagmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|max:255',
            'mobile_verification'=>'required|numeric|digits:10',
            'email_verification'=>'required|email',
            'access_control'=>'required',
            'enable_feature'=>'required',
        ];
    }
}
