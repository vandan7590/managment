<?php

namespace App\Http\Controllers\pg;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Floor;
use App\Room;

class PgdetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $floor = Floor::all();
        $room = Room::distinct('floor_id')->get();
        return view('pg.pages.pgdetails',compact('floor','room'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $floor = new Floor(array(
            'floor'=>$request->get('floor')
        ));
        $floor->save();
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->get('room');
        $room = '10'.$data;
        $room = new Room(array(
            'floor_id'=>$request->get('floor_id'),
            'room'=>$room,
            'bed_type'=>$request->get('bed_type'),
            'no_of_beds'=>$request->get('no_of_beds'),
            'status'=>'assign'
        ));
        $room->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
