<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pg;
use App\User;

class DashboardController extends Controller
{
    public function dashboard()
    {
        $user = User::all();
        return view('admin.pages.dashboard',compact('user'));
    }

    public function pg_dashboard()
    {
        return view('pg.pages.dashboard');
    }

    public function pgdetails()
    {
        return view('pg.pages.pgdetails');
    }
}
