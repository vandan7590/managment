<?php

namespace App\Http\Controllers\Session;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SignupRequest;
use App\Http\Requests\ConfirmpasswordRequest;
use App\Notifications\VerifyNotification;
use App\Notifications\EmailNotification;
use App\Notifications\PgInfoNotification;
use Twilio\Rest\Client;
use App\User;
use App\Pg;

class SessionController extends Controller
{
    public function login()
    {
        return view('login.login');
    }

    public function otp()
    {
        return view('login.verifyemail');
    }

    public function mail()
    {
        return view('mail.email');
    }

    public function signup()
    {
        return view('login.signup');
    }

    public function login_store(Request $request)
    {
        if(User::loginUser($request->email,$request->password))//Check UserName and Password
        {
            $role = User::where('email','=',$request->email)->pluck('role')->first();
            $verify = User::where('email','=',$request->email)->pluck('verify')->first();

            if($role=='admin'){
                notify()->success('Login Successfully !');
                return \Redirect::route('dashboard');
            }            
            elseif($role=='pg'){                
                // $uid = Pg::where('user_id',\Auth::user()->id)->first();
                notify()->success('Login Successfully !');
                return \Redirect::route('pgdetail');
            }
            elseif($role=='library'){
                notify()->success('Login Successfully !');
                return \Redirect::route('librarydashboard');
            }
            elseif($role=="user" && $verify=="1"){
                notify()->success('Login Successfully !');
                return \Redirect::route('userdashboard');
            }
            else
            {
                notify()->error('Invalid Email Or Password !');
                return redirect()->back();
            }
        }
        else
        {
            notify()->error('Invalid Email Or Password !');
            return redirect()->back();
        }
    }

    public function signup_store(SignupRequest $request)
    {
        $contact_otp = rand(1000,9999);
        $email_otp = rand(1000,9999);

        $user = new User(array(
            'role'=>'user',
            'email_otp'=>$email_otp,
            'contact_otp'=>$contact_otp,
            'name'=>$request->get('name'),
            'password'=>'',
            'contactno'=>$request->get('contactno'),
            'email'=>$request->get('email'),
            'verify'=>'0'
        ));
        $user->save();

        $userid = $user->id;   
        $email = $user->email;

        // $account_sid = getenv("TWILIO_SID");
        // $auth_token = getenv("TWILIO_AUTH_TOKEN");
        // $twilio_number = getenv("TWILIO_NUMBER");
        // $client = new Client($account_sid, $auth_token);
        // $client->messages->create($user->contactno,
        //     ['from' => $twilio_number, 'body' => $message] );
        
        $sendmail = User::where('id',$userid)->first();
        $sendmail->notify(new VerifyNotification($email_otp));
        $sendmail->notify(new EmailNotification($email));
        
        notify()->success('Registration Successfully !');
        return view('login.verifyemail',compact('userid'));
    }

    public function verify_email_otp(Request $request)
    {
        $get_otp = $request->get('otp');
        $user = User::where('role','=','user')->where('email_otp','=',$get_otp)->first();
        $userid=$user->id;

        if($user){    
            notify()->success('Email Verified');
            return view('login.verifycontact',compact('userid'));
        }
        else{
            notify()->error('Your OTP Incorrect Try Again !');
            return back();
        }
    }

    public function resend_otp($id)
    {
        $email_otp = rand(1000,9999);

        \DB::table('users')
            ->where('id',$id)
            ->update(['email_otp'=>$email_otp]);

        $sendmail = User::where('id',$id)->first();
        $userid = User::where('id',$id)->first();
        $sendmail->notify(new VerifyNotification($email_otp));

        notify()->success('OTP Send Successfully');
        return view('login.verifyemail',compact('userid'));
    }

    public function verify_contact_otp(Request $request)
    {
        $get_otp = $request->get('otp');
        $user = User::where('role','=','user')->where('contact_otp','=',$get_otp)->first();
        $userid=$user->id;
        
        if($user){
            notify()->success('Contact Verified');
            return view('login.verify',compact('user'));
        }
        else{
            notify()->error('Your OTP Incorrect Try Again !');
            return view('login.verifycontact',compact('userid'));
        }
    }

    public function resend_contactotp($id)
    {
        $contact_otp = rand(1000,9999);

        \DB::table('users')
            ->where('id',$id)
            ->update(['contact_otp'=>$contact_otp]);

        $sendmail = User::where('id',$id)->first();
        $userid = User::where('id',$id)->first();
        $sendmail->notify(new VerifyNotification($contact_otp));

        notify()->success('OTP Send Successfully');
        return view('login.verifycontact',compact('userid'));
    }

    public function confirm_password_store(Request $request)
    {
        $userid = $request->get('userid');
        $user = User::where('role','=','user')->where('id','=',$userid)->first();
        // $pg = PG::all();
        $name = $user->name;

        \DB::table('users')
            ->where('id',$userid)
            ->update([
                'password'=>bcrypt($request->get('password'))
            ]);

        $user_notification = $name. " | Profile Verification Request";
        $user->notify(new PgInfoNotification($user_notification));
        notify()->success('Registration Successfully !');
        return \Redirect::route('pglogin');
    }

    public function pg_verify(Request $request)
    {
        $pg = PG::pluck('unique_id');
        $unqiueid = $request->get('unqiueid');
        $userid = $request->get('userid');
        $user = User::where('role','=','user')->where('id','=',$userid)->first(); 

        foreach($pg as $pgs)
        {
            if($pgs == $unqiueid){
                notify()->success('PG Unqiue Code Successfully Matched !');
                return view('login.confirmpassword',compact('user'));
            }
        }
        notify()->error('PG Unqiue Code Incorrect !');
        return view('login.verify',compact('userid','user'));
    }

    public function logout()
    {
       \Auth::logout();
        return \Redirect::route('pglogin');
    }
}
