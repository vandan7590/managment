<?php

namespace App\Http\Controllers\pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Librarydocument;
use App\User;
use App\Library;
use File;


class LibrarydocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->get('user_id');
        if(request()->has('multiple_doc'))
        {
            $files = Input::file('multiple_doc');
            $picName = str_random(30).'.'.$files->getClientOriginalExtension(); //Insert path in database
            $files->move(public_path().'/Library/Doc/'.$id.'/',$picName);       //Insert image in folder
        }

        $doc = new Librarydocument(array(
            'library_id'=>$request->get('library_id'),
            'doc_type'=>$request->get('doc_type'),
            'doc_name'=>$request->get('doc_name'),
            'doc_number'=>$request->get('doc_number'),
            'multiple_doc'=>$picName
        ));
        $doc->save();

        notify()->success('Library Document Successfully Added !');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $document = Librarydocument::where('id',$id)->first();
        $users = Library::where('id',$document->library_id)->first();
        $doc_path = public_path().'/Library/Doc/'.$users->user_id.'/'.$document->multiple_doc;  // Value is not URL but directory file path
        if(File::exists($doc_path)) {
            File::delete($doc_path);
        }
        $document->delete();
        notify()->success('Profile Document Remove Successfully !');
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
