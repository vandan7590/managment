<?php

namespace App\Http\Controllers\pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Libraryreview;
use App\Libraryrenewupdate;
use App\Package;
use App\Library;

class LibraryreviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $review = new Libraryreview(array(
            'library_id'=>$request->get('library_id'),
            'start_date'=>$request->get('start_date'),
            'end_date'=>$request->get('end_date'),
            'renew_reminder_date'=>$request->get('renew_reminder_date'),
            'expire_date'=>$request->get('expire_date'),
            'package'=>$request->get('package'),
            'date_of_payment'=>$request->get('date_of_payment'),
            'date_of_renew'=>$request->get('date_of_renew'),
        ));
        $review->save();

        $update = new Libraryrenewupdate(array(
            'library_id'=>$request->get('library_id'),
            'start_date'=>$request->get('start_date'),
            'end_date'=>$request->get('end_date'),
            'renew_reminder_date'=>$request->get('renew_reminder_date'),
            'expire_date'=>$request->get('expire_date'),
            'package'=>$request->get('package'),
            'date_of_payment'=>$request->get('date_of_payment'),
            'date_of_renew'=>$request->get('date_of_renew'),
        ));
        $update->save();

        notify()->success('Review Successfully Added !');
        return \Redirect::route('library_view');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::all();
        $update = Libraryrenewupdate::where('library_id',$id)->get();
        $package = Package::all();
        $edit = Libraryreview::where('library_id','=',$id)->first();
        $pg = Library::where('id',$id)->pluck('id')->first();
        foreach($package as $list){
            if($list){
                return view('admin.pages.library.updatereview',compact('pg','edit','update','user','package'));
            }
        }
        notify()->info('Please First Add Package !');
        return \Redirect::route('library_view');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $id = $request->get('library_id');
        \DB::table('libraryreviews')
        ->where('library_id',$id)
        ->update([
            'start_date'=>$request->get('start_date'),
            'end_date'=>$request->get('end_date'),
            'renew_reminder_date'=>$request->get('renew_reminder_date'),
            'expire_date'=>$request->get('expire_date'),
            'package'=>$request->get('package'),
            'date_of_payment'=>$request->get('date_of_payment'),
            'date_of_renew'=>$request->get('date_of_renew'),
        ]);

        $update = new Libraryrenewupdate(array(
            'library_id'=>$id,
            'start_date'=>$request->get('start_date'),
            'end_date'=>$request->get('end_date'),
            'renew_reminder_date'=>$request->get('renew_reminder_date'),
            'expire_date'=>$request->get('expire_date'),
            'package'=>$request->get('package'),
            'date_of_payment'=>$request->get('date_of_payment'),
            'date_of_renew'=>$request->get('date_of_renew'),
        ));
        $update->save();

        notify()->success('Review Updated Successfully !');
        return \Redirect::route('library_view');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
