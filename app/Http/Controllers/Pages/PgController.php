<?php

namespace App\Http\Controllers\pages;

use App\Pg;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\EmailNotification;
use App\Http\Requests\PgRequest;
use Notification;
use App\User;
use App\Pincode;

class PgController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Pg $pg)
    {
        $user = Pg::all();
        $pincode = \DB::table('pincodes')->pluck("pincode","id");
        $pin = Pincode::all();
        return view('admin.pages.pg.viewpg',compact('user','pincode','pin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {              
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PgRequest $request)
    {
        $email = $request->get('email');
        $contactno = $request->get('contact_no');
        $landlineno = $request->get('landlineno');
        
        $arrayKeys = array_keys($email);
        $first_email=$email[$arrayKeys[0]];

        $uid = 'pg'.strtolower(str_random(4));
        $users = new User(array(
            'name'=>$request->get('name'),
            'email'=>$first_email,
            'password'=>'-',
            'role'=>'pg'
        ));
        $users->save();
        $userid = $users->id;
        
        $implode_email = implode(" , ",$email);
        $implode_contact = implode(" , ",$contactno);
        $implode_landlineno = implode(" , ",$landlineno);

        $pg_name = new Pg(array(
            'user_id'=>$userid,
            'unique_id'=>$uid,
            'name'=>$request->get('name'),
            'pincode'=>$request->get('pincode'),
            'state'=>$request->get('state'),
            'city'=>$request->get('city'),
            'address'=>$request->get('address'),
            'email'=>$implode_email,
            'contact_no'=>$implode_contact,
            'landlineno'=>$implode_landlineno,
            'status'=>'0',
        ));
        $pg_name->save();
        $users->notify(new EmailNotification($uid,$first_email));
        notify()->success('PG Name Successfully Added !');
        return \Redirect::route('profile',['id'=>$userid]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pg  $pg
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pg = Pg::where('id',$id)->first();
        if($pg != null){
            $pg->delete();
            notify()->success('Pg Removed Successfully !');
            return \Redirect::route('pg_view');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pg  $pg
     * @return \Illuminate\Http\Response
     */
    public function edit(PgRequest $request)
    {
        $id = $request->get('id');
        $email = $request->get('email');
        $contactno = $request->get('contact_no');
        $landlineno = $request->get('landlineno');

        $implode_email = implode(" , ",$email);
        $implode_contact = implode(" , ",$contactno);
        $implode_landlineno = implode(" , ",$landlineno);

        \DB::table('pgs')
            ->where('id',$id)
            ->update([               
                'name'=>$request->get('name'),
                'pincode'=>$request->get('pincode'),
                'state'=>$request->get('state'),
                'city'=>$request->get('city'),
                'address'=>$request->get('address'),
                'email'=>$implode_email,
                'contact_no'=>$implode_contact,
                'landlineno'=>$implode_landlineno,                
            ]);
            notify()->success('PG Details Updated Successfully !');
            return \Redirect::route('pg_view');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pg  $pg
     * @return \Illuminate\Http\Response
     */
    public function update()
    {        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pg  $pg
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
           
    }
}
