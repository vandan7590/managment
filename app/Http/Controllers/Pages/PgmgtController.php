<?php

namespace App\Http\Controllers\pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pgmanagment;

class PgmgtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $chk = $request->get('enable_feature');
        $chk1 = implode(" , ",$chk);

        $managment = new Pgmanagment(array(
            'pg_id'=>$request->get('pg_id'),
            'name'=>$request->get('name'),
            'mobile_verification'=>$request->get('mobile_verification'),
            'email_verification'=>$request->get('email_verification'),
            'access_control'=>$request->get('access_control'),
            'enable_feature'=>$chk1
        ));
        $managment->save();

        notify()->success('Managment User Successfully Added !');
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mgt = Pgmanagment::where('id',$id)->first();
        if($mgt != null){
            $mgt->delete();
            notify()->success('Pg Managment Removed Successfully !');
            return back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $chk = $request->get('enable_feature');        
        $id = $request->get('id');
        
        if($chk){ $chk1 = implode(" , ",$chk);} else{ $chk1 = ""; }

        \DB::table('pgmanagments')
            ->where('id',$id)
            ->update([
                'name'=>$request->get('name'),
                'mobile_verification'=>$request->get('mobile_verification'),
                'email_verification'=>$request->get('email_verification'),
                'access_control'=>$request->get('access_control'),
                'enable_feature'=>$chk1
            ]);

        notify()->success('Managment User Updated !');
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
