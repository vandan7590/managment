<?php

namespace App\Http\Controllers\pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\PgRequest;
use Notification;
use App\Document;
use App\Owner;
use App\Review;
use App\Pgmanagment;
use App\Pincode;
use App\State;
use App\City;
use Carbon\Carbon;
use App\Pg;
use App\User;
use Response;
use File;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $id = $request->get('user_id');
        $picName = Pg::where('user_id',$id)->first();        
        $image_path = public_path().'/PG/PGProfile/'.$id.'/'.$picName->pgimage;  // Value is not URL but directory file path
        if(File::exists($image_path)) {
            File::delete($image_path);
        }
        notify()->success('Profile Image Remove Successfully !');
        return back();   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->get('user_id');
        if(request()->has('pgimage'))
        {
            $files = Input::file('pgimage');
            $picName = str_random(30).'.'.$files->getClientOriginalExtension(); //Insert path in database
            $files->move(public_path().'/PG/PGProfile/'.$id.'/',$picName);       //Insert image in folder

            \DB::table('pgs')
            ->where('user_id',$id)
            ->update([
                'pgimage'=>$picName
            ]);
        }
        notify()->success('Profile Image Updated Successfully !');
        // return \Redirect::route('profile');
        return back()->withInput(['tab'=>'kt_tabs_7_4']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $view = Pg::where('user_id','=',$id)->first();
        $doc = Document::where('pg_id','=',$view->id)->get();
        $pincode = \DB::table('pincodes')->pluck("pincode","id");
        $states = \DB::table('states')->pluck("state","id");
        $user = User::all();
        $pincodes = Pincode::all();
        $city = City::all();
        $state = State::all();
        $renew = Review::where('pg_id',$view->id)->get();
        $owner = Owner::where('pg_id',$view->id)->get();
        $updateowner = Owner::where('pg_id',$view->id)->first();
        $managment = Pgmanagment::where('pg_id',$view->id)->get();        
        return view('admin.pages.pg.viewprofile',compact('renew','user','managment','owner','updateowner','pincode','pincodes','doc','view','city','state'));   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $id = $request->get('userid');
        \DB::table('pgs')
            ->where('user_id',$id)
            ->update([
                'status'=>$request->get('status'),
            ]);
            notify()->success('Pg Status Updated Successfully !');
            return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
    }
}
