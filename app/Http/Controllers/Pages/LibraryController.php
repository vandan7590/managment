<?php

namespace App\Http\Controllers\pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Library;
use App\User;

class LibraryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Library::all();
        $country = \DB::table('countries')->pluck("name","id");
        return view('admin.pages.library.viewlibrary',compact('user','country'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $email = $request->get('email');
        $contactno = $request->get('contact_no');
        $landlineno = $request->get('landlineno');
        
        $arrayKeys = array_keys($email);
        $first_email=$email[$arrayKeys[0]];

        $uid = str_random(4);
        $users = new User(array(
            'name'=>$request->get('name'),
            'email'=>$first_email,
            'password'=>'-',
            'role'=>'library'
        ));
        $users->save();
        $userid = $users->id;

        $implode_email = implode(" , ",$email);
        $implode_contact = implode(" , ",$contactno);
        $implode_landlineno = implode(" , ",$landlineno);

        $pg_name = new Library(array(
            'user_id'=>$userid,
            'unique_id'=>$uid,
            'name'=>$request->get('name'),
            'pincode'=>$request->get('pincode'),
            'state'=>$request->get('state'),
            'city'=>$request->get('city'),
            'address'=>$request->get('address'),
            'email'=>$implode_email,
            'contact_no'=>$implode_contact,
            'landlineno'=>$implode_landlineno,
            'status'=>'0',
        ));
        $pg_name->save();

        notify()->success('Library Successfully Added !');
        return \Redirect::route('libraryprofile',['id'=>$userid]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pg = Library::where('id',$id)->first();
        if($pg != null){
            $pg->delete();
            notify()->success('Library Removed Successfully !');
            return \Redirect::route('library_view');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $id = $request->get('id');
        $email = $request->get('email');
        $contactno = $request->get('contact_no');
        $landlineno = $request->get('landlineno');

        $implode_email = implode(" , ",$email);
        $implode_contact = implode(" , ",$contactno);
        $implode_landlineno = implode(" , ",$landlineno);

        \DB::table('libraries')
            ->where('id',$id)
            ->update([               
                'name'=>$request->get('name'),
                'pincode'=>$request->get('pincode'),
                'state'=>$request->get('state'),
                'city'=>$request->get('city'),
                'address'=>$request->get('address'),
                'email'=>$implode_email,
                'contact_no'=>$implode_contact,
                'landlineno'=>$implode_landlineno,                
            ]);
            notify()->success('Library Details Updated Successfully !');
            return \Redirect::route('library_view');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
