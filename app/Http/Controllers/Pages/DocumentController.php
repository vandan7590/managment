<?php

namespace App\Http\Controllers\pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\EmailNotification;
use Illuminate\Support\Facades\Input;
use App\Document;
use App\User;
use App\Pg;
use File;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->get('user_id');
        if(request()->has('multiple_doc'))
        {
            $files = Input::file('multiple_doc');
            $picName = str_random(30).'.'.$files->getClientOriginalExtension(); //Insert path in database
            $files->move(public_path().'/PG/Doc/'.$id.'/',$picName);       //Insert image in folder
        }

        $doc = new Document(array(
            'pg_id'=>$request->get('pg_id'),
            'doc_type'=>$request->get('doc_type'),
            'doc_name'=>$request->get('doc_name'),
            'doc_number'=>$request->get('doc_number'),
            'multiple_doc'=>$picName
        ));
        $doc->save();

        notify()->success('PG Document Successfully Added !');
        return back()->withInput(['tab'=>'kt_tabs_7_1']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $document = Document::where('id',$id)->first();
        $users = Pg::where('id',$document->pg_id)->first();
        $doc_path = public_path().'/PG/Doc/'.$users->user_id.'/'.$document->multiple_doc;  // Value is not URL but directory file path
        if(File::exists($doc_path)) {
            File::delete($doc_path);
        }
        $document->delete();
        notify()->success('Profile Document Remove Successfully !');
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
