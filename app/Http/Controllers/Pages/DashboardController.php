<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pg;
use App\User;

class DashboardController extends Controller
{
    public function dashboard()
    {
        $user = User::all();
        return view('admin.pages.dashboard',compact('user'));
    }

    public function pg_profile()
    {
        $user = \Auth::user();
        $view = Pg::where('user_id','=',$user->id)->first();
        return view('pg.pages.pgprofile',compact('view','user'));
    }

    public function pg_dashboard()
    {
        return view('pg.pages.dashboard');
    }

    public function library_dashboard()
    {
        return view('library.pages.dashboard');
    }

    public function user_dashboard()
    {
        return view('user.dashboard');
    }

}


