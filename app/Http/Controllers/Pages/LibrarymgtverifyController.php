<?php

namespace App\Http\Controllers\pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\VerifyNotification;
use App\User;
use App\Library;
use App\Librarymanagment;

class LibrarymgtverifyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id)
    {
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $library = Librarymanagment::where('id',$id)->first();
        $userid = Library::where('id',$library->library_id)->first();

        $email_otp = rand(1000,9999);   
        \DB::table('librarymanagments')
            ->where('id',$id)
            ->update([
                'emailotp'=>$email_otp
            ]);
        $sendmail = User::where('id',$userid->user_id)->first();
        $sendmail->notify(new VerifyNotification($email_otp));

        notify()->success('Otp Send Successfully !');
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $emailotp = $request->get('emailotp');
        $id = $request->get('library_id');
        
        $mgt = Librarymanagment::where('library_id',$id)->first();
        if($emailotp == $mgt->emailotp){
            \DB::table('librarymanagments')
                ->where('library_id',$id)
                ->update([
                    'verify'=>'1'
                ]);
                notify()->success('Email Verified Successfully !');
                return back();
        }
        else{
            notify()->error('Enter Valid OTP!');
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
