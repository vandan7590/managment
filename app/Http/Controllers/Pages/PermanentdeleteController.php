<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pg;
use App\Owner;
use App\Library;
use App\Libraryowner;
use App\Pgmanagment;
use App\Librarymanagment;

class PermanentdeleteController extends Controller
{
    public function pg_permanentdelete($id)
    {
        $note = Pg::onlyTrashed()->find($id);
        if (!is_null($note)) { $note->forceDelete(); return back(); } 
        else { return back(); }
        return back();
    }

    public function pg_owner_permanentdelete($id)
    {
        $note = Owner::onlyTrashed()->find($id);
        if (!is_null($note)) { $note->forceDelete(); return back(); } 
        else { return back(); }
        return back();
    }

    public function pg_mgtuser_permanentdelete($id)
    {
        $note = Pgmanagment::onlyTrashed()->find($id);
        if (!is_null($note)) { $note->forceDelete(); return back(); } 
        else { return back(); }
        return back();
    }

    public function library_permanentdelete($id)
    {
        $note = Library::onlyTrashed()->find($id);
        if (!is_null($note)) { $note->forceDelete(); return back(); } 
        else { return back(); }
        return back();
    }

    public function library_owner_permanentdelete($id)
    {
        $note = Libraryowner::onlyTrashed()->find($id);
        if (!is_null($note)) { $note->forceDelete(); return back(); } 
        else { return back(); }
        return back();
    }

    public function library_mgtuser_permanentdelete($id)
    {
        $note = Librarymanagment::onlyTrashed()->find($id);
        if (!is_null($note)) { $note->forceDelete(); return back(); } 
        else { return back(); }
        return back();
    }
}
