<?php

namespace App\Http\Controllers\pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraryowner;

class LibraryownerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $owner = $request->get('owner');
        $email = $request->get('email');  
        $contactno = $request->get('contactno');

        $owners = implode(" , ",$owner);
        $mail = implode(" , ",$email);
        $contact = implode(" , ",$contactno);  

        $owner = new Libraryowner(array(
            'library_id'=>$request->get('library_id'),
            'owner'=>$owners,
            'email'=>$mail,
            'contactno'=>$contact,
            'verify'=>'0',
            'contactverify'=>'0'
        ));
        $owner->save();

        notify()->success('Owner Details Successfully Added !');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $owner = Libraryowner::where('id',$id)->first();
        if($owner != null){
            $owner->delete();
            notify()->success('Library Owner Removed Successfully !');
            return back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $id = $request->get('id');        
        \DB::table('libraryowners')
            ->where('id',$id)
            ->update([
                'owner'=>$request->get('owner'),
                'email'=>$request->get('email'),
                'contactno'=>$request->get('contactno')
            ]);

        notify()->success('Library Owner Updated !');
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
