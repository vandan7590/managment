<?php

namespace App\Http\Controllers\pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\PgRequest;
use Notification;
use App\Librarydocument;
use App\Libraryowner;
use App\Librarymanagment;
use App\Country;
use App\State;
use App\City;
use Carbon\Carbon;
use App\Library;
use App\User;
use Response;
use File;

class LibraryprofileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $id = $request->get('user_id');
        $picName = Library::where('user_id',$id)->first();        
        $image_path = public_path().'/Library/Libraryprofile/'.$id.'/'.$picName->libraryimage;  // Value is not URL but directory file path
        if(File::exists($image_path)) {
            File::delete($image_path);
        }
        notify()->success('Profile Image Remove Successfully !');
        return back();   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->get('user_id');
        if(request()->has('libraryimage'))
        {
            $files = Input::file('libraryimage');
            $picName = str_random(30).'.'.$files->getClientOriginalExtension(); //Insert path in database
            $files->move(public_path().'/Library/Libraryprofile/'.$id.'/',$picName);       //Insert image in folder

            \DB::table('libraries')
            ->where('user_id',$id)
            ->update([
                'libraryimage'=>$picName
            ]);
        }
        notify()->success('Profile Image Updated Successfully !');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $view = Library::where('user_id','=',$id)->first();
        $doc = Librarydocument::where('library_id','=',$view->id)->get();
        $country = \DB::table('countries')->pluck("name","id");
        $user = User::all();
        $countries = Country::all();
        $city = City::all();
        $state = State::all();
        $owner = Libraryowner::where('library_id',$view->id)->get();
        $managment = Librarymanagment::where('library_id',$view->id)->get();        
        return view('admin.pages.library.libraryprofile',compact('user','country','countries','doc','owner','managment','view','city','state'));   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $id = $request->get('userid');
        \DB::table('libraries')
            ->where('user_id',$id)
            ->update([
                'status'=>$request->get('status'),
            ]);
            notify()->success('Pg Status Updated Successfully !');
            return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
