<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use App\Pg;
use App\Library;
use App\Document;
use App\Librarydocument;
use App\City;
use App\Admin;
use App\User;
use App\Package;

class PgController extends Controller
{
    public function subdomain()
    {
        // $url = parse_url(Pg::all());
        foreach($url as $list){
            $domain = explode('.', $url->unique_id);
        }
    }

    public function downloadfile($id)
    {         
        $file = Document::where('id',$id)->first();
        $user = Pg::where('id',$file->pg_id)->first();
        $myfile = public_path('/PG/Doc/'.$file->multiple_doc);
        return response()->download($myfile);
    }

    public function librarydownloadfile($id)
    {         
        $file = Librarydocument::where('id',$id)->first();
        $user = Library::where('id',$file->library_id)->first();
        $myfile = public_path('/Library/Doc/'.$file->multiple_doc);
        return response()->download($myfile);
    }

    public function city_store(Request $request)
    {
        $city = new City(array(
            'pid_id'=>$request->get('pid_id'),
            'city'=>$request->get('city')
        ));
        $city->save();
        return back();  
    }

    public function getStateList(Request $request)
    {
        $states = \DB::table("states")
        ->where("pid_id",$request->pid_id)
        ->pluck("state","id");
        return response()->json($states);
    }

    public function getCityList(Request $request)
    {
        $cities = \DB::table("cities")
        ->where("pid_id",$request->pid_id)
        ->pluck("city","id");
        return response()->json($cities);
    }

    public function json_view()
    {
        $view = Admin::all();
        return response()->json(['data'=>$view]);
    }

    public function package_json_view()
    {
        $view = Package::all();
        return response()->json(['data'=>$view]);
    }
}
