<?php

namespace App\Http\Middleware;

use Closure;

class PgMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(\Auth::check() && \Auth::user()->isPg())
        {
            return $next($request);
        }        
        return \Redirect::route('pglogin');
    }
}
