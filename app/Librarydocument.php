<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Librarydocument extends Model
{
    protected $fillable = [
        'library_id','doc_type','doc_name','doc_number','multiple_doc'
    ];
}
