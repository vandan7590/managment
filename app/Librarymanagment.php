<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Librarymanagment extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'library_id','name','mobile_verification','email_verification','access_control','enable_feature','emailotp','verify'
    ];
}
