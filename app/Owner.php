<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Owner extends Model
{
    use SoftDeletes; 
    
    protected $fillable = [
        'pg_id','owner','email','contactno','verify','contactverify','emailotp'
    ];
}
