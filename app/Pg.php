<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pg extends Model
{
    use SoftDeletes; 
    use Notifiable;
    
    protected $fillable = [
        'user_id','unique_id','pgimage','name','email','contact_no','address','pincode','state','city','landlineno','status','password','approval'];

        public static function getUsers(){
            $records = \DB::table('pgs')->select('user_id','unique_id','name','email','contact_no','address','pincode','state','city','landlineno')->orderBy('id', 'asc')->get()->toArray();  
            return $records;
        }

        public function users()
        {
            return $this->belongsTo('App\User','user_id');
        }
}
