<?php

namespace App\Exports;

use App\Demo;
use Maatwebsite\Excel\Concerns\FromCollection;

class DemosExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Demo::all();
    }
}
