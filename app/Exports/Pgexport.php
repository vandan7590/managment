<?php

namespace App\Exports;

use App\Pg;
use Maatwebsite\Excel\Concerns\FromCollection;

class Pgexport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect(Pg::getUsers());
    }
}
