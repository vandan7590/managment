<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $fillable = [
        'name', 'contact_no', 'email','doj','status'
    ];

    public static function store($admin,$email,$contactno)
    {        
        $email1 = implode(" , ",$email);
        $contactno1 = implode(" , ",$contactno);

        $admin = Admin::create([
            'name'=>$admin->name,
            'contact_no'=>$contactno1,
            'email'=>$email1,
            'doj'=>$admin->doj,
            'status'=>$admin->status,
        ]);                   
    }

    public static function updateRecord($admin,$email,$contactno,$id)
    {
        $email1 = implode(" , ",$email);
        $contactno1 = implode(" , ",$contactno);

        $admin = Admin::where('id','=',$id)->update([
            'name'=>$admin->name,
            'contact_no'=>$contactno1,
            'email'=>$email1,
            'doj'=>$admin->doj,
            'status'=>$admin->status,
        ]);
    }
}
