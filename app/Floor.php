<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Floor extends Model
{
    protected $fillable = [
        'floor'
    ];

    public function rooms()
    {
        return $this->belongsTo('App\Room','id');
    }

}
