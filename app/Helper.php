<?php
  function setActive($path, $active = 'kt-menu__item--active'){
    return call_user_func_array('Request::is', (array) $path) ? $active : '';
  }
