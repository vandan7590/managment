<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Library extends Model
{
    use SoftDeletes;
    use Notifiable;
    
    protected $fillable = [
        'user_id','unique_id','libraryimage','name','email','contact_no','address','pincode','state','city','landlineno','status','password','approval'];

}
