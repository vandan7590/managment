<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable = [
        'pg_id','start_date','end_date','renew_reminder_date','expire_date','package','date_of_payment','date_of_renew'
    ];
}
