<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Libraryupdate extends Model
{
    protected $fillable = [
        'user_id','library_id','name','email','contact_no','address','country','city','pincode','status','owner','owner_email','contactno','mgt_name','mobile_verification','email_verification','access_control','enable_feature','status_approval'
    ];
}
