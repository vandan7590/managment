<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Libraryowner extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'library_id','owner','email','contactno','emailotp','contactverify','verify'
    ];
}
