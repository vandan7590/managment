<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Notifications\ForgotNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPassword;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role','name', 'email', 'password','contactno','contact_otp','email_otp','verify'
    ];

    public function owner()
    {
        return $this->hasMany('App\Owner');
    }

    public function pgs()
    {
        return $this->hasMany('App\Pg');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function loginUser($email,$password)
    {
        return \Auth::attempt(array('email' => $email, 'password' => $password));
    }

    public function isAdmin()
    {
        return($this->role=='admin');
    }

    public function isPg()
    {
        return($this->role=='pg');
    }

    public function isLibrary()
    {
        return($this->role=='library');
    }

    public function isUser()
    {
        return ($this->role=="user");
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }
}
