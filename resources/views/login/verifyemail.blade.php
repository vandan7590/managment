<!DOCTYPE html>
<html lang="en">
	<head>
		<base href="../../../">
		<meta charset="utf-8" />
		<title>Metronic | Wizard 4</title>
		<meta name="description" content="Wizard examples">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
        <link href="{{asset('admin/assets/css/pages/wizard/wizard-1.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="{{asset('admin/assets/media/logos/favicon.ico')}}" />
		<style>
			.back{
				background-color: white
			}
		</style>
		@notifyCss
	</head>
	<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-aside--minimize kt-page--loading back">
		@include('notify::messages')
		<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
			<div class="kt-portlet" style="margin-top: 100px;">
				<div class="kt-portlet__body kt-portlet__body--fit">
					<div class="kt-grid kt-wizard-v1 kt-wizard-v1--white" id="kt_wizard_v1" data-ktwizard-state="step-first">
						<div class="kt-grid__item">
							<div class="kt-wizard-v1__nav">
								<div class="kt-wizard-v1__nav-items kt-wizard-v1__nav-items--clickable">
									<div class="kt-wizard-v1__nav-item" data-ktwizard-type="step" data-ktwizard-state="current">
										<div class="kt-wizard-v1__nav-body">
											<div class="kt-wizard-v1__nav-icon">
												<i class="flaticon2-email"></i>
											</div>
											<div class="kt-wizard-v1__nav-label">
												1. Email Verification
											</div>
										</div>
									</div>
									<div class="kt-wizard-v1__nav-item" data-ktwizard-type="step">
										<div class="kt-wizard-v1__nav-body">
											<div class="kt-wizard-v1__nav-icon">
												<i class="flaticon-support"></i>
											</div>
											<div class="kt-wizard-v1__nav-label">
												2. Contact Verification
											</div>
										</div>
									</div>
									<div class="kt-wizard-v1__nav-item" data-ktwizard-type="step">
										<div class="kt-wizard-v1__nav-body">
											<div class="kt-wizard-v1__nav-icon">
												<i class="flaticon2-laptop"></i>
											</div>
											<div class="kt-wizard-v1__nav-label">
												3. PG/Library Verification
											</div>
										</div>
									</div>
									<div class="kt-wizard-v1__nav-item" data-ktwizard-type="step">
										<div class="kt-wizard-v1__nav-body">
											<div class="kt-wizard-v1__nav-icon">
												<i class="flaticon2-gear"></i>
											</div>
											<div class="kt-wizard-v1__nav-label">
												4. Change Password
											</div>
										</div>
									</div>
									<div class="kt-wizard-v1__nav-item" data-ktwizard-type="step">
										<div class="kt-wizard-v1__nav-body">
											<div class="kt-wizard-v1__nav-icon">
												<i class="flaticon-globe"></i>
											</div>
											<div class="kt-wizard-v1__nav-label">
												5. Confirmation & Review
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="container"> 
							<div class="kt-grid__item kt-grid__item--fluid kt-wizard-v1__wrapper">
								<div class="kt-form">
									<form method="post" action="{{route('verify_email')}}">
										{{csrf_field()}}
										<div class="kt-wizard-v4__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
											<div class="kt-heading kt-heading--md">Enter your Email OTP</div>
											<div class="kt-form__section kt-form__section--first">
												<div class="kt-wizard-v4__form">
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label>Enter Your Email OTP to Verify Your Account</label>
																<input type="text" class="form-control" name="otp" placeholder="Enter OTP" id="myText">
															</div>
															<a href="{{route('resend_otp',$userid)}}" style="float:right"> Resend OTP </a>
															<div class="alert-text" style="font-size:15px;" id="some_div"></div>
														</div>
													</div>
												</div>
												<button class="btn btn-primary" type="submit" style="float:right">Next Step</button>
											</div>												
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
        <script>
            var KTAppOptions = {
                "colors": {
                    "state": {
                        "brand": "#22b9ff",
                        "light": "#ffffff",
                        "dark": "#282a3c",
                        "primary": "#5867dd",
                        "success": "#34bfa3",
                        "info": "#36a3f7",
                        "warning": "#ffb822",
                        "danger": "#fd3995"
                    },
                    "base": {
                        "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                        "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
                    }
                }
            };
		</script>
		{{-- otp script --}}
		<script>
			var timeLeft = 120;
			var elem = document.getElementById('some_div');
			var timerId = setInterval(countdown, 1000);
		
			function countdown() {
				if (timeLeft == -1) {
					clearTimeout(timerId);
					doSomething();
				} else {
					elem.innerHTML = timeLeft + ' seconds remaining';
					timeLeft--;
				}
			}
			function doSomething() {
				document.getElementById("myText").disabled = true;
			}
		</script>
		@notifyJs
        <script src="{{asset('admin/assets/plugins/global/plugins.bundle.js')}}" type="text/javascript"></script>
        <script src="{{asset('admin/assets/js/scripts.bundle.js')}}" type="text/javascript"></script>
		<script src="{{asset('admin/assets/js/pages/custom/wizard/wizard-1.js')}}" type="text/javascript"></script>
	</body>
</html>