<!DOCTYPE html>
<html lang="en">
	<head>
		<base href="../../../">
		<meta charset="utf-8" />
		<title>PG-MANAGMENT | Login</title>
		<meta name="description" content="Login page example">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
		<link href="{{asset('admin/assets/css/pages/login/login-1.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('admin/assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('admin/assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="{{asset('admin/assets/media/logos/favicon.ico')}}" />
		<style>
			.hover1:hover{
				color:dodgerblue;
			}
			.color{
				color:white;
			}
		</style>
		@notifyCss
	</head>
	<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-aside--minimize kt-page--loading">
		@include('notify::messages')
		<div class="kt-grid kt-grid--ver kt-grid--root kt-page">
			<div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v1" id="kt_login">
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
					<div class="kt-grid__item kt-grid__item--order-tablet-and-mobile-2 kt-grid kt-grid--hor kt-login__aside" style="background-image: url({{asset('admin/assets/media/bg/bg-4.jpg')}});">
						<div class="kt-grid__item">	
							<a href="#" class="kt-login__logo">
								<img src="{{asset('admin/assets/media/users/info.png')}}">
							</a>						
						</div>
						<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver">
							<div class="kt-grid__item kt-grid__item--middle">
								<h3 class="kt-login__title">Welcome to UMS!</h3>
								<h4 class="kt-login__subtitle">Don't have an account yet? <a href="{{route('signup')}}" class="hover1 color">Sign up! </a></h4>
							</div>
						</div>
					</div>
					<div class="kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper">
						<div class="kt-login__head">
						</div>
						<div class="kt-login__body">
							<div class="kt-login__form">
								<div class="kt-login__title">
									<h3>Sign In To UMS</h3>
								</div>
								<form class="kt-form" action="{{route('login_store')}}" novalidate="novalidate" method="POST">
									{{csrf_field()}}
									<div class="form-group">
										<input class="form-control" type="text" placeholder="Email" name="email" autocomplete="off">
									</div>
									<div class="form-group">
										<input class="form-control" type="password" placeholder="Password" name="password" autocomplete="off">
									</div>
									<div class="kt-login__actions">
										<a href="{{ route('password.request') }}" class="kt-link kt-login__link-forgot">
											Forgot Password ?
										</a>
										<button class="btn btn-primary" type="submit" style="border-radius: 5px;">Sign In</button>
									</div>
								</form>								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@notifyJs
		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#22b9ff",
						"light": "#ffffff",
						"dark": "#282a3c",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
		</script>
		<script src="{{asset('admin/assets/plugins/global/plugins.bundle.js')}}" type="text/javascript"></script>
		<script src="{{asset('admin/assets/js/scripts.bundle.js')}}" type="text/javascript"></script>
		<script src="{{asset('admin/assets/js/pages/custom/login/login-1.js')}}" type="text/javascript"></script>
	</body>
</html>