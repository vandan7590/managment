<!DOCTYPE html>
<html lang="en">
	<head>
		<base href="../../../">
		<meta charset="utf-8" />
		<title>REGISTER | PG-MANAGMENT</title>
		<meta name="description" content="Login page example">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
		<link href="{{asset('admin/assets/css/pages/login/login-1.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('admin/assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('admin/assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="{{asset('admin/assets/media/logos/favicon.ico')}}" />
		@notifyCss
	</head>
	<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-aside--minimize kt-page--loading">
		@include('notify::messages')
		<div class="kt-grid kt-grid--ver kt-grid--root kt-page">
			<div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v1" id="kt_login">
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
					<div class="kt-grid__item kt-grid__item--order-tablet-and-mobile-2 kt-grid kt-grid--hor kt-login__aside" style="background-image: url({{asset('admin/assets/media/bg/bg-4.jpg')}});">
						<div class="kt-grid__item">	
							<a href="#" class="kt-login__logo">
								<img src="{{asset('admin/assets/media/users/info.png')}}">
							</a>						
						</div>
						<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver">
							<div class="kt-grid__item kt-grid__item--middle">
								<h3 class="kt-login__title">Welcome to UMS!</h3>
								<h4 class="kt-login__subtitle">Register Your Account Here!</h4>
							</div>
						</div>
					</div>
					<div class="kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper">
						<div class="kt-login__head">
						</div>
						<div class="kt-login__body">
							<div class="kt-login__form">
								<div class="kt-login__title">
									<h3>Register UMS</h3>
								</div>
								<form class="kt-form" action="{{route('signup_store')}}" novalidate="novalidate" method="POST">
									{{csrf_field()}}
									<div class="form-group">
										<input class="form-control" type="text" placeholder="Enter Name" name="name">
									</div>
									<div class="form-group">
										<input class="form-control" type="text" placeholder="Enter Email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required>
										<span class="text-red error" style="color:white">{{$errors->first('email')}}</span>
									</div>
									<div class="form-group">
										<input class="form-control" type="text" placeholder="Enter Mobile No" name="contactno" pattern="[1-9]{1}[0-9]{9}" title="Please Enter 10 Digit Mobile No" required>
									</div>
									<div class="kt-login__actions" style="float:right">										
										<button class="btn btn-primary btn-elevate kt-login__btn-primary" style="border-radius: 10px" type="submit">Register</button>&nbsp;&nbsp;&nbsp;&nbsp;
										<button class="btn btn-primary btn-elevate kt-login__btn-primary" style="border-radius: 10px" type="reset"> Cancel </button>
									</div>
								</form>								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@notifyJs
		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#22b9ff",
						"light": "#ffffff",
						"dark": "#282a3c",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
		</script>
		<script src="{{asset('admin/assets/plugins/global/plugins.bundle.js')}}" type="text/javascript"></script>
		<script src="{{asset('admin/assets/js/scripts.bundle.js')}}" type="text/javascript"></script>
		<script src="{{asset('admin/assets/js/pages/custom/login/login-1.js')}}" type="text/javascript"></script>
	</body>
</html>

