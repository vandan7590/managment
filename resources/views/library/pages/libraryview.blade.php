@extends('library.layouts.app')
@section('title')
    <title>VIEW LIBRARY DETAILS | PG-MANAGMENT</title>
@endsection
@section('styles')
@endsection
@section('content')
    <div>
        <span class="dropdown dropdown-inline" style="float:right; margin-top: 15px; margin-right: 10px; margin-bottom: 10px;">
            <button class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal" data-target="#modal_1" title="Add Library"><span class="flaticon2-menu-2"> </span></button>
            <button type="button" class="btn btn-bold btn-label-brand btn-sm"  data-toggle="modal"  data-target="#modal_2" title="Export"><span class="flaticon2-layers"></span></button>    
            <button class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal"  data-target="#modal_3" title="Import"><span class="flaticon2-user"></span></button>    
        </span>
    </div>
    {{-- Add Floor --}}
    <div class="modal fade" id="modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Enter Floor</h5>
                </button>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </div>
                <form method="POST" action="">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="kt-portlet__body">
                            <div class="form-group">
                                <label>Enter Floor</label>
                                <input type="text" class="form-control" placeholder="Enter Floor" name="name">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn_border-radius" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn_border-radius">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- Add Room --}}
    <div class="modal fade" id="modal_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Select Room</h5>
                </button>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </div>
                <form method="POST" action="">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="kt-portlet__body">
                            <div class="form-group">
                                <label>Select Room</label>
                                <select class="form-control form-control-sm" id="exampleSelects">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                                <div class="form-group">
                                    <label>Enter Room</label>
                                    <input type="text" class="form-control" placeholder="Enter Room" name="name">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn_border-radius" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn_border-radius">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- Add Bed --}}
    <div class="modal fade" id="modal_3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Select Bed</h5>
                </button>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </div>
                <form method="POST" action="">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="kt-portlet__body">
                            <div class="form-group">
                                <label>Select Floor</label>
                                <select class="form-control form-control-sm" id="exampleSelects">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                                <label>Select Room</label>
                                <select class="form-control form-control-sm" id="exampleSelects">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                                <div class="form-group">
                                    <label>Enter Bed</label>
                                    <input type="text" class="form-control" placeholder="Enter Bed" name="name">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn_border-radius" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn_border-radius">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- box modal --}}
    <div class="modal fade" id="modal_user" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Select Bed</h5>
                </button>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </div>
                <form method="POST" action="">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="kt-portlet__body">
                            <div class="form-group">
                                <label>Enter Your Name</label>
                                <input type="text" class="form-control" placeholder="Enter Your Name" name="name">
                            </div>
                            <div class="form-group">
                                <label>Today's date</label>
                                {{-- <input type="text" class="form-control" placeholder="Enter Your Name" name="name"> --}}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn_border-radius" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn_border-radius">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- box modal end --}}
@endsection
@section('scripts')
<script src="{{asset('admin/assets/js/scripts.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/assets/js/pages/crud/metronic-datatable/advanced/library.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
