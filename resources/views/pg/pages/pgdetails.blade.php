@extends('pg.layouts.app')
@section('title')
    <title>PG Details</title>
@endsection
@section('styles')
<link href="{{asset('admin/pgdetails.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="col-xl-12" style="margin-top:0.8rem">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-toolbar">
                <h4>Room Details</h4>
            </div>
        </div>
        <div class="kt-portlet__body" style="height:auto">
            <div id="item_table">  
                <div class="col-xl-12">
                    @foreach($floor as $list)
                    <div class="kt-portlet kt-portlet--height-fluid">
                        <div class="kt-portlet__head">                            
                            <div class="kt-portlet__head-label">                            
                                <h3 class="kt-portlet__head-title">
                                    Floor
                                </h3>&nbsp;&nbsp;&nbsp;
                                <h3 class="kt-portlet__head-title">
                                    {{$list->floor}}                                    
                                </h3>
                            </div>                            
                            <div style="float:right;">
                                <span style="float:right; margin: 10px;">
                                <button style="padding-top:1rem" class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal" data-target="#modal_addrom-{{$list->id}}">
                                <h5 class="add_room_button flaticon2-add-1">&nbsp;Add Room</h5></button>
                                </span>
                            </div>
                        </div>
                        @foreach($room as $lists)
                        @if($list->id == $lists->floor_id)
                        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" style="margin-top: 10px;">
                            <div class="kt-portlet">
                                <div class="kt-portlet__body  kt-portlet__body--fit">
                                    <div class="row row-no-padding row-col-separator-lg">                                        
                                        <div class="col-md-12 col-lg-12 col-xl-12">                                            
                                            <div class="kt-widget24">
                                                <div class="kt-widget24__details">
                                                    <div class="kt-widget24__info">
                                                        <h4 style="line-height:0" class="kt-widget24__title">Room&nbsp;&nbsp;&nbsp;
                                                            <span>{{$lists->room}}</span>
                                                        </h4>
                                                        <span style="color: #48465b; font-size:12px">{{$lists->bed_type}}</span>  
                                                    </div>
                                                    <div>
                                                        <a>
                                                            <i class="la la-remove"></i>
                                                        </a>
                                                    </div>
                                                </div> 
                                                <div class="kt-widget24__action" style="margin-top:5px;">
                                                    @if($list->id == $lists->floor_id)
                                                    @for($i=1; $i<=$lists->no_of_beds; $i++)
                                                    <div class="col-lg-3" style="border:1px solid rgba(0, 0, 0, 0.1); border-radius:10px; margin:2px; padding:5px; font-size:13px;">
                                                        <div class="kt-widget24__info">
                                                            <span style="float:left; margin-top:-0.1rem" class="addcenter kt-widget4__icon">
                                                                <span style="padding-left:2px;" class="addcenter kt-widget24__title">{{$list->rooms->status}}&nbsp;&nbsp;
                                                            <span class="kt-badge kt-badge--outline kt-badge--danger">+</span></span>                                                                    
                                                            </span>                                                            
                                                            <span class="addcenter" style="padding-left:6px;">David Miller
                                                                <span style="padding-left:6px;">01-01-2020</span>&nbsp;to&nbsp;
                                                                <span style="">01-02-2020</span>
                                                            </span>
                                                        </div>
                                                    </div>                                                   
                                                    @endfor 
                                                    @endif
                                                </div>
                                            </div>                                            
                                        </div>                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        {{-- Add Room --}}
                        <div class="modal fade" id="modal_addrom-{{$list->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Add Room</h5>
                                    </button>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    </div>
                                    <form method="post" action="{{route('pgdetails_store')}}">
                                        {{csrf_field()}}
                                        <input type="hidden" name="floor_id" value="{{$list->id}}">
                                        <div class="modal-body">
                                            <div class="kt-portlet__body">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <label>Enter Room</label>
                                                        <input type="text" class="form-control" placeholder="Enter room" name="room">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Bed Type</label>
                                                        <input type="text" class="form-control" placeholder="Bed type" name="bed_type">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Number of beds</label>
                                                        <input type="text" class="form-control" placeholder="Number of beds" name="no_of_beds">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary btn-sm" style="border-radius: 5px" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-brand btn-sm" style="border-radius: 5px">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @endforeach
                </div>                
            </div>
            <span class="addcenter" style="float:left;padding-top:5px">
                {{-- onclick="add_element_to_array();display_array();/* addfloor();addone(); */" --}}
                <a  href="#" style="padding-top:1rem" class="btn btn-bold btn-label-brand btn-sm add" data-toggle="modal" data-target="#modal_1">
                    <h5 class=" flaticon2-add-1">&nbsp;Add Floor</h5>
                </a>
            </span>
        </div>           
    </div>
    <!--end::Portlet-->        
</div>


{{-- Add Floor --}}
<div class="modal fade" id="modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Enter Floor</h5>
            </button>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            </div>
            <form method="get" action="{{route('floor_store')}}">
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="form-group">
                            <label>Enter Floor</label>
                            <input type="text" class="form-control" placeholder="Enter Floor" name="floor">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" style="border-radius: 5px" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-brand btn-sm" style="border-radius: 5px">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- Add Room --}}
<div class="modal fade" id="modal_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Select Room</h5>
            </button>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            </div>
            <form method="" action="">
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="form-group">
                            <label>Select Floor</label>
                            <select class="form-control form-control-sm" name="floor_id">
                                {{-- @foreach($floor as $list) --}}
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                {{-- @endforeach --}}
                            </select>
                            <div class="form-group">
                                <label>Enter Room</label>
                                <input type="text" class="form-control" placeholder="Enter Room" name="room">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn_border-radius" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn_border-radius">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- Add Bed --}}
<div class="modal fade" id="modal_3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Select Bed</h5>
            </button>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            </div>
            <form method="POST" action="">
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="form-group">
                            <label>Select Floor</label>
                            <select class="form-control form-control-sm" name="floor_id" id="floor">
                                {{-- @foreach($floors as $key => $list) --}}
                                   <option value="1">1</option>
                                   <option value="2">2</option>
                                   <option value="3">3</option>
                                {{-- @endforeach --}}
                            </select>
                            <label>Select Room</label>
                            <select class="form-control form-control-sm" name="room_id" id="room">                                
                            </select>
                            <div class="form-group">
                                <label>Enter Bed</label>
                                <input type="text" class="form-control" placeholder="Enter Bed" name="bed">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn_border-radius" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn_border-radius">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- Add Empty modal --}}
<div class="modal fade" id="modal_empty" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Select Bed</h5>
            </button>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            </div>
            <form method="POST" action="">
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="form-group">
                            <div class="form-group">
                                <label>Enter Name</label>
                                <input type="text" class="form-control" placeholder="Enter name" name="name">
                            </div>
                            <div class="form-group">
                                <label>Enter Email</label>
                                <input type="email" class="form-control" placeholder="Enter email" name="email">
                            </div>
                            <div class="form-group">
                                <label>Enter Contact</label>
                                <input type="text" class="form-control" placeholder="Enter contact" name="contact">
                            </div>
                            <div class="form-group">
                                <label>Enter Date</label>
                                <input type="date" class="form-control" name="date">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn_border-radius" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn_border-radius">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

@section('scripts')
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
@endsection
@stop
