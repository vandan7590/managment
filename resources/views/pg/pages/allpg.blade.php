@extends('pg.layouts.app')
@section('title')
    <title>All PG</title>
@endsection
@section('styles')
{{-- <link href="{{asset('admin/assets/css/hirenpg/allpg.css')}}" rel="stylesheet"> --}}
<link rel="stylesheet" href="{{asset('admin/pgdetails.css')}}">
@endsection
@section('content')

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" style="margin-top:0.8rem;">

    <!--Begin::Row-->
    <div class="row">
        <div class="col-lg-6 col-xl-4">
            <div class="kt-portlet">
                <div class="kt-infobox__section">
                    <div class="kt-infobox__content">
                        <!--begin::Total Profit-->
                        <div class="kt-widget kt-widget--user-profile-3">
                            <div class="kt-widget__top" style="padding: 5px;">
                                <div class="kt-widget__media kt-hidden-">
                                    <img src="{{asset('admin/assets/media/users/100_13.jpg')}}" alt="image">
                                </div>
                                <div class="kt-widget__content">
                                    <div class="kt-widget__head">
                                        <a href="#" class="kt-widget__username">
                                            Stay Safe
                                            <i class="flaticon2-correct"></i>
                                        </a>
                                        <div class="kt-widget__action" style="float:right;">
                                            <button type="button" class="btn btn-label-success btn-sm btn-upper">4.5</button>&nbsp;
                                        </div>
                                    </div>

                                    <div class="kt-widget__subhead">
                                        <a href="#"><i class="flaticon2-calendar-3"></i>PG Manager </a>
                                        <a href="#"><i class="flaticon2-new-email"></i>staysafe@gmail.com</a><br>
                                        <a href="#"><i class="flaticon-placeholder-3"></i>Krishna Complex, Near Iscon, Ahmedabad, Gujarat</a>
                                    </div>

                                    <div class="kt-widget__info">
                                        <div class="kt-widget__desc">
                                            <a href="{{ url ('openpg')}}"><span class="kt-font-primary">Register Now<span><a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Total Profit-->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-xl-4">
            <div class="kt-portlet">
                <div class="kt-infobox__section">
                    <div class="kt-infobox__content">
                        <!--begin::Total Profit-->
                        <div class="kt-widget kt-widget--user-profile-3">
                            <div class="kt-widget__top" style="padding: 5px;">
                                <div class="kt-widget__media kt-hidden-">
                                    <img src="{{asset('admin/assets/media/users/100_13.jpg')}}" alt="image">
                                </div>
                                <div class="kt-widget__content">
                                    <div class="kt-widget__head">
                                        <a href="#" class="kt-widget__username">
                                            Stay Safe
                                            <i class="flaticon2-correct"></i>
                                        </a>
                                    </div>
                                    <div class="kt-widget__action" style="float:right; margin-top:-1.8rem;">
                                        <button type="button" class="btn btn-label-success btn-sm btn-upper">4.5</button>&nbsp;
                                    </div>
                                    <div class="kt-widget__subhead">
                                        <i class="flaticon2-calendar-3"></i>PG Manager
                                        <a href="#"><i class="flaticon2-new-email"></i>staysafe@gmail.com</a><br>
                                        <a href="#"><i class="flaticon-placeholder-3"></i>Krishna Complex, Near Iscon, Ahmedabad, Gujarat</a>
                                    </div>

                                    <div class="kt-widget__info">
                                        <div class="kt-widget__desc">
                                            <a href="{{ url ('openpg')}}"><span class="kt-font-primary">Register Now<span><a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Total Profit-->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-xl-4">
            <div class="kt-portlet">
                <div class="kt-infobox__section">
                    <div class="kt-infobox__content">
                        <!--begin::Total Profit-->
                        <div class="kt-widget kt-widget--user-profile-3">
                            <div class="kt-widget__top" style="padding: 5px;">
                                <div class="kt-widget__media kt-hidden-">
                                    <img src="{{asset('admin/assets/media/users/100_13.jpg')}}" alt="image">
                                </div>
                                <div class="kt-widget__content">
                                    <div class="kt-widget__head">
                                        <a href="#" class="kt-widget__username">
                                            Stay Safe
                                            <i class="flaticon2-correct"></i>
                                        </a>
                                         
                                    </div>
                                    <div class="kt-widget__action" style="float:right; margin-top:-1.8rem;">
                                        <button type="button" class="btn btn-label-success btn-sm btn-upper">4.5</button>&nbsp;
                                    </div>
                                    <div class="kt-widget__subhead">
                                        <i class="flaticon2-calendar-3"></i>PG Manager
                                        <a href="#"><i class="flaticon2-new-email"></i>staysafe@gmail.com</a><br>
                                        <a href="#"><i class="flaticon-placeholder-3"></i>Krishna Complex, Near Iscon, Ahmedabad, Gujarat</a>
                                    </div>

                                    <div class="kt-widget__info">
                                        <div class="kt-widget__desc">
                                            <a href="{{ url ('openpg')}}"><span class="kt-font-primary">Register Now<span><a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Total Profit-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End::Row-->

</div>


@section('scripts')
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
@endsection
@stop