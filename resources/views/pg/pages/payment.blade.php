@extends('pg.layouts.app')
@section('title')
    <title>PG Payment</title>
@endsection
@section('styles')
<link href="{{ asset ('admin/assets/css/hirenpg/') }}" rel="stylesheet">
<style>
.modal-dialog.modal-xl {
    max-width: 1730px;
}
</style>
@endsection
@section('content')

<!--Begin::Section-->
    <div class="col-xl-12" style="margin-top:0.8rem">
        <!--begin::Portlet-->
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-toolbar">
                    <h4>Payment</h4>
                </div>
                <div>
                    <span style="float:right; margin-top: 15px; margin-right: 10px; margin-bottom: 10px;">
                        <button class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal" data-target="#modal_addpayment">
                            <span class="flaticon2-plus">&nbsp;&nbsp;Add Payment</span>
                        </button>
                    </span>
                </div>
            </div>
            <div class="col-xl-12" style="">
                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-toolbar">
                            <ul class="nav nav-pills nav-pills-sm" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#kt_tabs_6_11" role="tab">
                                        Bill Generate
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#kt_tabs_6_22" role="tab">
                                        Pending Payment
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#kt_tabs_6_33" role="tab">
                                        Succesful Payment 
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#kt_tabs_6_44" role="tab">
                                        Payment Reminder 
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="kt-portlet__body" style="height:480px">
                        <div class="tab-content">
                            <div class="tab-pane active" id="kt_tabs_6_11" role="tabpanel">
                                 Outer tab 11
                            </div>
                            <div class="tab-pane" id="kt_tabs_6_22" role="tabpanel">
                                Outer Tab 22
                            </div>
                            <div class="tab-pane " id="kt_tabs_6_33" role="tabpanel">
                                Outer Tab 33
                            </div>
                            <div class="tab-pane " id="kt_tabs_6_44" role="tabpanel">
                                Outer Tab 44
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>       
        </div>
        <!--end::Portlet-->
    </div>
<!--End::Section-->

<!--begin::Modal-->
    <div class="modal fade" style="padding-left:15px" id="modal_addpayment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Payment Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>

                <div class="modal-body">
                    <form>

                        <div class="form-group row">
                            <label style="text-align:center; padding-top:8px" for="example-search-input" class="col-2">Search</label>
                            <div class="col-9">
                                <div class="kt-input-icon kt-input-icon--left">
                                <input style="border-radius:25px" type="text" class="form-control" placeholder="Search User..." id="generalSearch">
                                <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                    <span><i class="la la-search"></i></span>
                                </span>
                            </div>
                            </div>
                            
                        </div>

                        <div class="col-xl-12" style="">
                            <!--begin::Portlet-->
                            <div class="kt-portlet">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-toolbar">
                                        <ul class="nav nav-pills nav-pills-sm" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" data-toggle="tab" href="#kt_tabs_in_1" role="tab">
                                                    User Info
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#kt_tabs_in_2" role="tab">
                                                    Type of Payment
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#kt_tabs_in_3" role="tab">
                                                    Payment Refund
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#kt_tabs_in_4" role="tab">
                                                    Bill Generate pdf 
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="kt-portlet__body" style="height:520px">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="kt_tabs_in_1" role="tabpanel">
                                            modal 111111
                                        </div>
                                        <div class="tab-pane" id="kt_tabs_in_2" role="tabpanel">
                                            modal 2222
                                        </div>
                                        <div class="tab-pane " id="kt_tabs_in_3" role="tabpanel">
                                            modal 333
                                        </div>
                                        <div class="tab-pane " id="kt_tabs_in_4" role="tabpanel">
                                            modal 4444
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Portlet-->
                        </div> 

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>
<!--end::Modal-->

@section('scripts')
@endsection
@stop