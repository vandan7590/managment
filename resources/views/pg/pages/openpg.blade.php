@extends('pg.layouts.app')
@section('title')
    <title>Open PG</title>
@endsection
@section('styles')
<link href="{{asset('admin/assets/css/hirenpg/allpg.css')}}" rel="stylesheet">
@endsection
@section('content')

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" style="margin-top:0.8rem;">

    <!--Begin::Row-->
    <div class="row">
        <div class="col-xl-12">

            <!--begin:: Widgets/Applications/User/Profile3-->
            <div class="kt-portlet kt-portlet--height-fluid">   
                <div class="kt-portlet__body">
                    <div class="row kt-widget kt-widget--user-profile-3">
                        <div class="kt-widget__top col-sm-12">
                            <div class="kt-widget__media kt-hidden-">
                                <img src="{{asset('admin/assets/media/users/100_13.jpg')}}" alt="image">
                            </div>
                            <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden">
                                JM
                            </div>
                            <div class="kt-widget__content">
                                <div class="kt-widget__head">
                                    <a href="#" class="kt-widget__username">
                                        Stay Safe
                                        <i class="flaticon2-correct"></i>
                                    </a>
                                </div>
                                <div class="kt-widget5__info" style="padding-top:5px;">
                                    <span>Email:</span>
                                    <span class="kt-font-info">&nbsp;   staysafe@gmail.com</span>
                                </div>
                                <div class="kt-widget5__info" style="padding-top:5px;">
                                    <span>Contact No:</span>
                                    <span class="kt-font-info">&nbsp;+91 968788786</span>
                                </div>
                                <div class="kt-widget5__info" style="padding-top:5px;">
                                    <span>Address:</span>
                                    <span class="kt-font-info">&nbsp; Krishna complex, Near Iscon,</span>
                                    <span style="padding-left:10px;">City:</span>
                                    <span class="kt-font-info">&nbsp;Ahmedabad</span>
                                    <span style="padding-left:10px;">PinCode:</span>
                                    <span class="kt-font-info">&nbsp;363636</span>
                                    <span style="padding-left:10px;">Country:</span>
                                    <span class="kt-font-info">&nbsp;Gujarat</span>
                                </div>
                            </div>
                        </div>
                        <div class="kt-widget__top col-sm-6">
                            
                        </div> 
                    </div>
                </div>
            </div>

            <!--end:: Widgets/Applications/User/Profile3-->
        </div>

        <div class="col-xl-12" style="margin-top:-1.3rem">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-toolbar">
                        <ul class="nav nav-pills nav-pills-sm" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#kt_tabs_7_1" role="tab">
                                    Room
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#kt_tabs_7_2" role="tab">
                                    Payment
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <span class="dropdown dropdown-inline" style="float:right; margin-top: 15px; margin-right: 10px; margin-bottom: 10px;">
                            <button class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal" data-target="#modal_1" title="Add Library"><span class="flaticon2-menu-2"> </span></button>
                            <button class="btn btn-bold btn-label-brand btn-sm"  data-toggle="modal"  data-target="#modal_2" title="Export"><span class="flaticon2-layers"></span></button>    
                            <button class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal"  data-target="#modal_3" title="Import"><span class="flaticon2-user"></span></button>    
                        </span>
                    </div>
                </div>
                <div class="kt-portlet__body" style="height:auto">
                    <div class="tab-content" style="">
                        <div class="tab-pane active" id="kt_tabs_7_1" role="tabpanel">

                            <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

                                <!--Begin::Dashboard 6-->

                                <!--begin:: Widgets/Stats-->
                                <div class="kt-portlet">
                                    <div class="kt-portlet__body  kt-portlet__body--fit">
                                        <div class="row row-no-padding row-col-separator-lg">
                                            
                                            <div class="col-md-12 col-lg-6 col-xl-3">
                                                <!--begin::Total Profit-->
                                                <div class="kt-widget24">
                                                    <div class="kt-widget24__details">
                                                        <div class="kt-widget24__info">
                                                            <h4 class="kt-widget24__title">Floor&nbsp;&nbsp;&nbsp;
                                                                <span class="kt-font-warning">01</span>
                                                            </h4> 
                                                        </div>
                                                        <div class="kt-widget24__info">
                                                            <h4 class="kt-widget24__title">Room&nbsp;&nbsp;&nbsp;
                                                                <span class="kt-font-danger">101</span>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                    <div class="kt-widget24__action">
                                                        <div class="kt-widget24__info" data-toggle="modal" data-target="#modal_single" title="Single Bed">
                                                            <h6 style="float:left; padding:2px;" class="kt-widget24__title bed-hover">Single Bed&nbsp;&nbsp;&nbsp;</h6> 
                                                            <span class="kt-font-primary">01</span>
                                                        </div>
                                                        <div class="kt-widget24__info" data-toggle="modal" data-target="#modal_double" title="Double Bed">
                                                            <h6 style="float:left; padding:2px;" class="kt-widget24__title bed-hover">Double Bed&nbsp;&nbsp;&nbsp;</h6> 
                                                            <span class="kt-font-primary">01</span>
                                                        </div>
                                                        <div class="kt-widget24__info" data-toggle="modal" data-target="#modal_2bed" title="Two Bed Sharing">
                                                            <h6 style="float:left; padding:2px;" class="kt-widget24__title bed-hover">2 Bed&nbsp;&nbsp;&nbsp;</h6> 
                                                            <span class="kt-font-primary">01</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end::Total Profit-->
                                            </div>
                                            <div class="col-md-12 col-lg-6 col-xl-3">
                                                <!--begin::Total Profit-->
                                                <div class="kt-widget24">
                                                    <div class="kt-widget24__details">
                                                        <div class="kt-widget24__info">
                                                            <h4 class="kt-widget24__title">Floor&nbsp;&nbsp;&nbsp;
                                                                <span class="kt-font-warning">01</span>
                                                            </h4> 
                                                        </div>
                                                        <div class="kt-widget24__info">
                                                            <h4 class="kt-widget24__title">Room&nbsp;&nbsp;&nbsp;
                                                                <span class="kt-font-danger">102</span>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                    <div class="kt-widget24__action">
                                                        <div class="kt-widget24__info" data-toggle="modal" data-target="#modal_single" title="Single Bed">
                                                            <h6 style="float:left; padding:2px;" class="kt-widget24__title bed-hover">Single Bed&nbsp;&nbsp;&nbsp;</h6> 
                                                            <span class="kt-font-primary">01</span>
                                                        </div>
                                                        <div class="kt-widget24__info" data-toggle="modal" data-target="#modal_double" title="Double Bed">
                                                            <h6 style="float:left; padding:2px;" class="kt-widget24__title bed-hover">Double Bed&nbsp;&nbsp;&nbsp;</h6> 
                                                            <span class="kt-font-primary">01</span>
                                                        </div>
                                                        <div class="kt-widget24__info" data-toggle="modal" data-target="#modal_2bed" title="Two Bed Sharing">
                                                            <h6 style="float:left; padding:2px;" class="kt-widget24__title bed-hover">2 Bed&nbsp;&nbsp;&nbsp;</h6> 
                                                            <span class="kt-font-primary">01</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end::Total Profit-->
                                            </div>
                                            <div class="col-md-12 col-lg-6 col-xl-3">
                                                <!--begin::Total Profit-->
                                                <div class="kt-widget24">
                                                    <div class="kt-widget24__details">
                                                        <div class="kt-widget24__info">
                                                            <h4 class="kt-widget24__title">Floor&nbsp;&nbsp;&nbsp;
                                                                <span class="kt-font-warning">01</span>
                                                            </h4> 
                                                        </div>
                                                        <div class="kt-widget24__info">
                                                            <h4 class="kt-widget24__title">Room&nbsp;&nbsp;&nbsp;
                                                                <span class="kt-font-danger">103</span>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                    <div class="kt-widget24__action">
                                                        <div class="kt-widget24__info" data-toggle="modal" data-target="#modal_single" title="Single Bed">
                                                            <h6 style="float:left; padding:2px;" class="kt-widget24__title bed-hover">Single Bed&nbsp;&nbsp;&nbsp;</h6> 
                                                            <span class="kt-font-primary">01</span>
                                                        </div>
                                                        <div class="kt-widget24__info" data-toggle="modal" data-target="#modal_double" title="Double Bed">
                                                            <h6 style="float:left; padding:2px;" class="kt-widget24__title bed-hover">Double Bed&nbsp;&nbsp;&nbsp;</h6> 
                                                            <span class="kt-font-primary">01</span>
                                                        </div>
                                                        <div class="kt-widget24__info" data-toggle="modal" data-target="#modal_2bed" title="Two Bed Sharing">
                                                            <h6 style="float:left; padding:2px;" class="kt-widget24__title bed-hover">2 Bed&nbsp;&nbsp;&nbsp;</h6> 
                                                            <span class="kt-font-primary">01</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end::Total Profit-->
                                            </div>
                                            <div class="col-md-12 col-lg-6 col-xl-3">
                                                <!--begin::Total Profit-->
                                                <div class="kt-widget24">
                                                    <div class="kt-widget24__details">
                                                        <div class="kt-widget24__info">
                                                            <h4 class="kt-widget24__title">Floor&nbsp;&nbsp;&nbsp;
                                                                <span class="kt-font-warning">01</span>
                                                            </h4> 
                                                        </div>
                                                        <div class="kt-widget24__info">
                                                            <h4 class="kt-widget24__title">Room&nbsp;&nbsp;&nbsp;
                                                                <span class="kt-font-danger">104</span>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                    <div class="kt-widget24__action">
                                                        <div class="kt-widget24__info" data-toggle="modal" data-target="#modal_single" title="Single Bed">
                                                            <h6 style="float:left; padding:2px;" class="kt-widget24__title bed-hover">Single Bed&nbsp;&nbsp;&nbsp;</h6> 
                                                            <span class="kt-font-primary">01</span>
                                                        </div>
                                                        <div class="kt-widget24__info" data-toggle="modal" data-target="#modal_double" title="Double Bed">
                                                            <h6 style="float:left; padding:2px;" class="kt-widget24__title bed-hover">Double Bed&nbsp;&nbsp;&nbsp;</h6> 
                                                            <span class="kt-font-primary">01</span>
                                                        </div>
                                                        <div class="kt-widget24__info" data-toggle="modal" data-target="#modal_2bed" title="Two Bed Sharing">
                                                            <h6 style="float:left; padding:2px;" class="kt-widget24__title bed-hover">2 Bed&nbsp;&nbsp;&nbsp;</h6> 
                                                            <span class="kt-font-primary">01</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end::Total Profit-->
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane" id="kt_tabs_7_2" role="tabpanel">
                            <!--Begin::Row-->
                            <div class="row" style="margin-left:7%; margin-right:7%">
								<div class="col-xl-9 col-lg-6 order-lg-1 order-xl-1">

									<!--begin:: Widgets/Download Files-->
									<div class="kt-portlet kt-portlet--height-fluid">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Payment
												</h3>
											</div>
										</div>
										<div class="kt-portlet__body">

											<!--begin::k-widget4-->
											<div class="kt-widget4">
												<div class="kt-widget4__item">
													<span class="kt-widget4__icon">
                                                        <i class="flaticon-piggy-bank  kt-font-success"></i>
                                                    </span>
													<a href="#" class="kt-widget4__title">
														Token Money
													</a>
                                                    <span class="kt-widget24__stats kt-font-danger">
														Rs. 00.0
													</span>
												</div>
												<div class="kt-widget4__item">
													<span class="kt-widget4__icon">
                                                        <i class="flaticon2-lock  kt-font-success"></i>
                                                    </span>
													<a href="#" class="kt-widget4__title">
														Security Money
													</a>
													<span class="kt-widget24__stats kt-font-danger">
														Rs. 00.0
													</span>
												</div>
												<div class="kt-widget4__item">
													<span class="kt-widget4__icon">
                                                        <i class="flaticon-home  kt-font-success"></i>
                                                    </span>
													<a href="#" class="kt-widget4__title">
														Room Rent
													</a>
													<span class="kt-widget24__stats kt-font-danger">
														Rs. 00.0
													</span>
												</div>
												<div class="kt-widget4__item">
													<span class="kt-widget4__icon">
                                                        <i class="flaticon-interface-7  kt-font-success"></i>
                                                    </span>
													<a href="#" class="kt-widget4__title">
														Electricity
													</a>
													<span class="kt-widget24__stats kt-font-danger">
														Rs. 00.0
													</span>
												</div>
												<div class="kt-widget4__item">
													<span class="kt-widget4__icon">
                                                        <i class="flaticon-cart  kt-font-success"></i>
                                                    </span>
													<a href="#" class="kt-widget4__title">
														Food
													</a>
													<span class="kt-widget24__stats kt-font-danger">
														Rs. 00.0
													</span>
												</div>
												<div class="kt-widget4__item">
													<span class="kt-widget4__icon">
                                                        <i class="flaticon-bag  kt-font-success"></i>
                                                    </span>
													<a href="#" class="kt-widget4__title">
														Refund
													</a>
													<span class="kt-widget24__stats kt-font-danger">
														Rs. 00.0
													</span>
												</div>
                                                <div class="kt-widget4__item">
													<span class="kt-widget4__icon">
                                                        <i class="flaticon-info  kt-font-success"></i>
                                                    </span>
													<a href="#" class="kt-widget4__title">
														Other
													</a>
													<span class="kt-widget24__stats kt-font-danger">
														Rs. 00.0
													</span>
												</div>
											</div>

											<!--end::Widget 9-->
										</div>
									</div>

									<!--end:: Widgets/Download Files-->
								</div>
								<div class="col-xl-3 col-lg-6 order-lg-1 order-xl-1">

									<!--begin:: Widgets/New Users-->
									<div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
										
										<div class="kt-portlet__body" style="margin-top:120px; text-align:center">
											<div class="tab-content">
												<div class="tab-pane active" id="kt_widget4_tab1_content">
													<div class="kt-widget4">
														<div class="kt-widget4__item" style="padding:20px">
															<div class="kt-widget4__pic kt-widget4__pic--pic">
																<img src="assets/media/users/100_4.jpg" alt="">
															</div>
															<div class="kt-widget4__info">
																<a href="#" class="kt-widget4__username">
																	Payment Reminder
																</a>
															</div>
														</div>
														<div class="kt-widget4__item" style="padding:20px">
															<div class="kt-widget4__pic kt-widget4__pic--pic">
																<img src="assets/media/users/100_14.jpg" alt="">
															</div>
															<div class="kt-widget4__info">
																<a href="#" class="kt-widget4__username">
																	Generate Bill
																</a>
															</div>
														</div>
														<div class="kt-widget4__item" style="padding:20px">
															<div class="kt-widget4__pic kt-widget4__pic--pic">
																<img src="assets/media/users/100_11.jpg" alt="">
															</div>
															<div class="kt-widget4__info">
																<a href="#" class="kt-widget4__username">
																	Pay Bill & Generate Invoice
																</a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<!--end:: Widgets/New Users-->
								</div>
							</div>
							<!--End::Row-->
                        </div>
                        <div class="tab-pane " id="kt_tabs_7_3" role="tabpanel">
                            <div class="col-xl-12" style="margin-top:-1.3rem">
                                <!--begin::Portlet-->
                                <div class="kt-portlet">
                                    <div class="kt-portlet__head">
                                        <div class="kt-portlet__head-toolbar">
                                            <ul class="nav nav-pills nav-pills-sm" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab" href="#kt_tabs_6_11" role="tab">
                                                        Notice
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#kt_tabs_6_22" role="tab">
                                                        Announcement
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#kt_tabs_6_33" role="tab">
                                                        Food Menu 
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#kt_tabs_6_44" role="tab">
                                                        About PG 
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#kt_tabs_6_55" role="tab">
                                                        Term and Condition
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#kt_tabs_6_66" role="tab">
                                                        About Software 
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__body" style="height:480px">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="kt_tabs_6_11" role="tabpanel">
                                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.
                                            </div>
                                            <div class="tab-pane" id="kt_tabs_6_22" role="tabpanel">
                                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.
                                            </div>
                                            <div class="tab-pane " id="kt_tabs_6_33" role="tabpanel">
                                                Lorem Ipsum is simply dummy text of the printing and typesetting industry.When an unknown printer took a galley of type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.
                                            </div>
                                            <div class="tab-pane " id="kt_tabs_6_44" role="tabpanel">
                                                444444444444444444444444444 is simply dummy text of the printing and typesetting industry.When an unknown printer took a galley of type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.
                                            </div>
                                            <div class="tab-pane " id="kt_tabs_6_55" role="tabpanel">
                                                555555555555555555555555555   simply dummy text of the printing and typesetting industry.When an unknown printer took a galley of type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.
                                            </div>
                                            <div class="tab-pane " id="kt_tabs_6_66" role="tabpanel">
                                                6666666666666666666666666666666  text of the printing and typesetting industry.When an unknown printer took a galley of type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Portlet-->
                            </div>
                        </div>
                        <div class="tab-pane" id="kt_tabs_7_4" role="tabpanel">
                            <!--Begin::Row-->
							<div class="row" style="margin-left:7%; margin-right:7%">
								<div class="col-xl-9 col-lg-6 order-lg-1 order-xl-1">

									<!--begin:: Widgets/Download Files-->
									<div class="kt-portlet kt-portlet--height-fluid">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Notice
												</h3>
											</div>
										</div>
									</div>

									<!--end:: Widgets/Download Files-->
								</div>
							</div>

							<!--End::Row-->
                        </div>
                        <div class="tab-pane" id="kt_tabs_7_5" role="tabpanel">
                            <!--Begin::Row-->
							<div class="row" style="margin-left:7%; margin-right:7%">
								<div class="col-xl-9 col-lg-6 order-lg-1 order-xl-1">

									<!--begin:: Widgets/Download Files-->
									<div class="kt-portlet kt-portlet--height-fluid">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Full And Final
												</h3>
											</div>
										</div>
									</div>

									<!--end:: Widgets/Download Files-->
								</div>
							</div>

							<!--End::Row-->
                        </div>
                        <div class="tab-pane" id="kt_tabs_7_6" role="tabpanel">
                            <!--Begin::Row-->
							<div class="row" style="margin-left:7%; margin-right:7%">
								<div class="col-xl-9 col-lg-6 order-lg-1 order-xl-1">

									<!--begin:: Widgets/Download Files-->
									<div class="kt-portlet kt-portlet--height-fluid">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Asset
												</h3>
											</div>
										</div>
									</div>

									<!--end:: Widgets/Download Files-->
								</div>
							</div>

							<!--End::Row-->
                        </div>
                        <div class="tab-pane" id="kt_tabs_7_7" role="tabpanel">
                            <!--Begin::Row-->
							<div class="row" style="margin-left:7%; margin-right:7%">
								<div class="col-xl-9 col-lg-6 order-lg-1 order-xl-1">

									<!--begin:: Widgets/Download Files-->
									<div class="kt-portlet kt-portlet--height-fluid">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Night Out
												</h3>
											</div>
										</div>
									</div>

									<!--end:: Widgets/Download Files-->
								</div>
							</div>

							<!--End::Row-->
                        </div>
                        <div class="tab-pane" id="kt_tabs_7_8" role="tabpanel">
                            <!--Begin::Row-->
							<div class="row" style="margin-left:7%; margin-right:7%">
								<div class="col-xl-9 col-lg-6 order-lg-1 order-xl-1">

									<!--begin:: Widgets/Download Files-->
									<div class="kt-portlet kt-portlet--height-fluid">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Police Verification
												</h3>
											</div>
										</div>
									</div>

									<!--end:: Widgets/Download Files-->
								</div>
							</div>

							<!--End::Row-->
                        </div>
                        <div class="tab-pane" id="kt_tabs_7_9" role="tabpanel">
                            <!--Begin::Row-->
							<div class="row" style="margin-left:7%; margin-right:7%">
								<div class="col-xl-9 col-lg-6 order-lg-1 order-xl-1">

									<!--begin:: Widgets/Download Files-->
									<div class="kt-portlet kt-portlet--height-fluid">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Room Notice
												</h3>
											</div>
										</div>
									</div>

									<!--end:: Widgets/Download Files-->
								</div>
							</div>

							<!--End::Row-->
                        </div>
                        <div class="tab-pane" id="kt_tabs_7_10" role="tabpanel">
                            <!--Begin::Row-->
							<div class="row" style="margin-left:7%; margin-right:7%">
								<div class="col-xl-9 col-lg-6 order-lg-1 order-xl-1">

									<!--begin:: Widgets/Download Files-->
									<div class="kt-portlet kt-portlet--height-fluid">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Room Change Request
												</h3>
											</div>
										</div>
									</div>

									<!--end:: Widgets/Download Files-->
								</div>
							</div>

							<!--End::Row-->
                        </div>
                        <div class="tab-pane" id="kt_tabs_7_11" role="tabpanel">
                            <!--Begin::Row-->
							<div class="row" style="margin-left:7%; margin-right:7%">
								<div class="col-xl-9 col-lg-6 order-lg-1 order-xl-1">

									<!--begin:: Widgets/Download Files-->
									<div class="kt-portlet kt-portlet--height-fluid">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Complains And Feedback
												</h3>
											</div>
										</div>
									</div>

									<!--end:: Widgets/Download Files-->
								</div>
							</div>

							<!--End::Row-->
                        </div>
                        
                    </div>
                </div>
            </div>
            <!--end::Portlet-->
        </div>
    </div>
    <!--End-->
    <!-- End::Row-->

</div>

{{-- Add Floor --}}
<div class="modal fade" id="modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Enter Floor</h5>
            </button>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            </div>
            <form method="" action="">
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="form-group">
                            <label>Enter Floor</label>
                            <input type="text" class="form-control" placeholder="Enter Floor" name="floor">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" style="border-radius: 5px" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-brand btn-sm" style="border-radius: 5px">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- Add Room --}}
<div class="modal fade" id="modal_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Select Room</h5>
            </button>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            </div>
            <form method="" action="">
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="form-group">
                            <label>Select Floor</label>
                            <select class="form-control form-control-sm" name="floor_id">
                                {{-- @foreach($floor as $list) --}}
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                {{-- @endforeach --}}
                            </select>
                            <div class="form-group">
                                <label>Enter Room</label>
                                <input type="text" class="form-control" placeholder="Enter Room" name="room">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn_border-radius" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn_border-radius">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- Add Bed --}}
<div class="modal fade" id="modal_3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Select Bed</h5>
            </button>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            </div>
            <form method="POST" action="">
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="form-group">
                            <label>Select Floor</label>
                            <select class="form-control form-control-sm" name="floor_id" id="floor">
                                {{-- @foreach($floors as $key => $list) --}}
                                   <option value="1">1</option>
                                   <option value="2">2</option>
                                   <option value="3">3</option>
                                {{-- @endforeach --}}
                            </select>
                            <label>Select Room</label>
                            <select class="form-control form-control-sm" name="room_id" id="room">                                
                            </select>
                            <div class="form-group">
                                <label>Enter Bed</label>
                                <input type="text" class="form-control" placeholder="Enter Bed" name="bed">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn_border-radius" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn_border-radius">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- model single --}}
<div class="modal fade show" id="modal_single" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Single bed</h5>
            </button>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            </div>
            <form method="" action="">
                {{csrf_field()}}
                <div class="modal-body" style="font-weight: 100;color: #595d6e;">
                    <div style="display:block">
                        <h5 style="float:left">
                            <i class="flaticon-layer"></i>&nbsp;&nbsp;
                            <span class="kt-font-brand">01</span>
                        </h5>
                        <h5 style="float:right">
                            <i class="flaticon-buildings"></i>&nbsp;&nbsp;
                            <span class="kt-font-brand">101</span>
                        </h5>
                    </div><br><hr>
                    <div style="display:block">
                        <h6 style="float:right">
                            Total Single Bed&nbsp;&nbsp;<span class="kt-font-brand">03</span>
                        </h6>
                    </div><br><br>

                    <div class="kt-widget4 kt-widget4--sticky">
                        <div class="kt-widget4__items kt-portlet__space-x kt-margin-t-15">
                            <div class="kt-widget4__item">
                                <span class="kt-widget4__icon">
                                    <span class="kt-badge kt-badge--warning kt-badge--md kt-badge--rounded"></span>
                                </span>	
                                <a href="{{ url ('allocate') }}" class="kt-widget4__title">
                                    <i class="flaticon2-user"></i>&nbsp;&nbsp;Pitter Dywien
                                </a> 		
                                <h6 style="">Booked<h6>
                            </div>			
                            <div class="kt-widget4__item">
                                <span class="kt-widget4__icon">
                                    <span class="kt-badge kt-badge--dark kt-badge--md kt-badge--rounded"></span>
                                </span>	
                                <a href="#" class="kt-widget4__title">
                                    
                                </a> 		
                                <h6>Block<h6>
                            </div>
                            <div class="kt-widget4__item">
                                <span class="kt-widget4__icon">
                                    <span class="kt-badge kt-badge--success kt-badge--md kt-badge--rounded"></span>
                                </span>	
                                <a href="#" class="kt-widget4__title">
                                    
                                </a> 		
                                <h6>Empty</h6>&nbsp;&nbsp;
                                <button class="btn btn-bold btn-label-brand btn-sm">
                                <span class="flaticon2-plus"  data-toggle="modal"  data-target="#modal_bed"></span></button>
                            </div>
                        </div>
                    </div> 
                </div>
            </form>
        </div>
    </div>
</div>
{{-- model Double --}}
<div class="modal fade" id="modal_double" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Double bed</h5>
            </button>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            </div>
            <form method="" action="">
                {{csrf_field()}}
                <div class="modal-body" style="font-weight: 100;color: #595d6e;">
                    <div style="display:block">
                        <h5 style="float:left">
                            <i class="flaticon-layer"></i>&nbsp;&nbsp;
                            <span class="kt-font-brand">01</span>
                        </h5>
                        <h5 style="float:right">
                            <i class="flaticon-buildings"></i>&nbsp;&nbsp;
                            <span class="kt-font-brand">101</span>
                        </h5>
                    </div><br><hr>
                    <div style="display:block">
                        <h6 style="float:right">
                            Total Double Bed&nbsp;&nbsp;<span class="kt-font-brand">03</span>
                        </h6>
                    </div><br><br>

                    <div class="kt-widget4 kt-widget4--sticky">
                        <div class="kt-widget4__items kt-portlet__space-x kt-margin-t-15">
                            <div class="kt-widget4__item">
                                <span class="kt-widget4__icon">
                                    <span class="kt-badge kt-badge--warning kt-badge--md kt-badge--rounded"></span>
                                </span>	
                                <a href="{{ url ('allocate') }}" class="kt-widget4__title">
                                    <i class="flaticon2-user"></i>&nbsp;&nbsp;Pravin Sharma
                                </a> 		
                                <h6 style="">Booked<h6>
                            </div>			
                            <div class="kt-widget4__item">
                                <span class="kt-widget4__icon">
                                    <span class="kt-badge kt-badge--dark kt-badge--md kt-badge--rounded"></span>
                                </span>	
                                <a href="#" class="kt-widget4__title">
                                    
                                </a> 		
                                <h6>Block<h6>
                            </div>
                            <div class="kt-widget4__item">
                                <span class="kt-widget4__icon">
                                    <span class="kt-badge kt-badge--success kt-badge--md kt-badge--rounded"></span>
                                </span>	
                                <a href="#" class="kt-widget4__title">
                                    
                                </a> 		
                                <h6>Empty</h6>&nbsp;&nbsp;
                                <button class="btn btn-bold btn-label-brand btn-sm"><span class="flaticon2-plus"> </span></button>
                            </div>
                        </div>
                    </div> 
                </div>
            </form>
        </div>
    </div>
</div>
{{-- model 2bed --}}
<div class="modal fade" id="modal_2bed" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Two Bed Sharing</h5>
            </button>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            </div>
            <form method="" action="">
                {{csrf_field()}}
                <div class="modal-body" style="font-weight: 100;color: #595d6e;">
                    <div style="display:block">
                        <h5 style="float:left">
                            <i class="flaticon-layer"></i>&nbsp;&nbsp;
                            <span class="kt-font-brand">01</span>
                        </h5>
                        <h5 style="float:right">
                            <i class="flaticon-buildings"></i>&nbsp;&nbsp;
                            <span class="kt-font-brand">101</span>
                        </h5>
                    </div><br><hr>
                    <div style="display:block">
                        <h6 style="float:right">
                            Total Two Bed&nbsp;&nbsp;<span class="kt-font-brand">03</span>
                        </h6>
                    </div><br><br>

                    <div class="kt-widget4 kt-widget4--sticky">
                        <div class="kt-widget4__items kt-portlet__space-x kt-margin-t-15">
                            <div class="kt-widget4__item">
                                <span class="kt-widget4__icon">
                                    <span class="kt-badge kt-badge--warning kt-badge--md kt-badge--rounded"></span>
                                </span>	
                                <a href="{{ url ('allocate') }}" class="kt-widget4__title">
                                    <i class="flaticon2-user"></i>&nbsp;&nbsp;Aarohi Gill
                                </a> 		
                                <h6 style="">Booked<h6>
                            </div>			
                            <div class="kt-widget4__item">
                                <span class="kt-widget4__icon">
                                    <span class="kt-badge kt-badge--dark kt-badge--md kt-badge--rounded"></span>
                                </span>	
                                <a href="#" class="kt-widget4__title">
                                    
                                </a> 		
                                <h6>Block<h6>
                            </div>
                            <div class="kt-widget4__item">
                                <span class="kt-widget4__icon">
                                    <span class="kt-badge kt-badge--success kt-badge--md kt-badge--rounded"></span>
                                </span>	
                                <a href="#" class="kt-widget4__title">
                                    
                                </a> 		
                                <h6>Empty</h6>&nbsp;&nbsp;
                                <button class="btn btn-bold btn-label-brand btn-sm"><span class="flaticon2-plus"> </span></button>
                            </div>
                        </div>
                    </div> 
                </div>
            </form>
        </div>
    </div>
</div>
{{-- Add allocation bed --}}
<div class="modal fade show" id="modal_bed" tabindex="1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add Bed</h5>
            </button>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            </div>
            <form method="" action="">
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="form-group">
                            <label>Enter Name</label>
                            <input type="text" class="form-control" placeholder="Enter Name" name="floor">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" style="border-radius: 5px" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-brand btn-sm" style="border-radius: 5px">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end:: Content -->

@section('scripts')
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
@endsection
@stop

