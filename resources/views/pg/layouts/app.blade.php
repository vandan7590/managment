<!DOCTYPE html>
<html lang="en">
<head>
    @yield('title')
    @include('pg.layouts.partials.head')
    @yield('styles')
</head>

<body style="" class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-aside--minimize kt-page--loading">

    <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
        <div class="kt-header-mobile__logo">
            <a href="#">
                <img alt="Logo" src="{{asset('admin/assets/media/logos/logo-6-sm.png')}}"/>
            </a>
        </div>
        <div class="kt-header-mobile__toolbar">
            <div class="kt-header-mobile__toolbar-toggler kt-header-mobile__toolbar-toggler--left" id="kt_aside_mobile_toggler"><span></span></div>
            <div class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></div>
            <div class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></div>
        </div>
    </div>

    <div class="kt-grid kt-grid--hor kt-grid--root">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
            @include('pg.layouts.partials.sidebar')

            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
                <!-- begin:: Header -->
                @include('pg.layouts.partials.header')
                <!-- end:: Header -->
                
                {{-- Contain --}}
                @yield('content')
                {{-- End Contain --}}

                <!-- begin:: Footer -->
                @include('pg.layouts.partials.footer')
                <!-- end:: Footer -->
            </div>
        </div>
    </div>

    <!-- end:: Page -->

    <!-- begin::Scrolltop -->
    <div id="kt_scrolltop" class="kt-scrolltop">
        <i class="fa fa-arrow-up"></i>
    </div>
    <!-- end::Scrolltop -->


{{-- add room bed --}}
<div class="modal fade" id="modal_addroombed" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add Bed</h5>
            </button>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            </div>
            <form method="" action="">
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="form-group">
                            <label>Enter Bed Name</label>
                            <input type="text" class="form-control" placeholder="Enter bed name" name="bedname">
                        </div>
                        <div class="form-group">
                            <label>Add Bed</label>
                            <input type="text" class="form-control" placeholder="Add number of bed" name="addbed">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn_border-radius" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn_border-radius">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- Script --}}
@include('pg.layouts.partials.js')
<script>
    // var cnt = 0;
    function addfloor () {
        // cnt++;
        // $( this ).after( "<p>Another paragraph! " + (++count) + "</p>" );
        // document.write(' floor --> ' + cnt +);
        document.querySelector('#floor').insertAdjacentHTML(
            'beforeend',
            `<div class="input-form kt-portlet kt-portlet--height-fluid">\
                <div class="kt-portlet__head">\
                    <div class="kt-portlet__head-label">\
                        <h4 class="">\
                            <div id="floor_fileds">\
                                FLOOR<span class="label"></span>\
                            </div>\
                        </h4>\
                    </div>\
                    <div style="float:right;">\
                        <span style="float:right; margin: 10px;">\
                            <button style="padding-top:1rem" class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal" data-target="#modal_addrom" onclick="addroom()">\
                                <h5 class="add_room_button flaticon2-add-1">&nbsp;Add Room</h5>\
                            </button>\
                        </span>\
                    </div>\
                </div>\
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" style="margin-top:1rem;">\
                    <div class="row">\
                        <div class="col-lg-6 col-xl-4">\
                            
                            <div class="kt-portlet" style="background:#F9F9FC;">\
                                <div class="kt-infobox__section">\
                                    <div class="kt-infobox__content">\
                                        <div class="kt-widget kt-widget--user-profile-3">\
                                            <div class="kt-widget24">\
                                                <div class="kt-widget24__action">\
                                                    <div class="kt-widget24__info">\
                                                        <div style="background-color: rgba(34, 185, 255, 0.1);border-radius:50px; border:2px solid rgba(34, 185, 255, 0.1); padding:10px; margin-top:-0.5rem;">
                                                            <span style="color:black; font-size:16px">Room&nbsp;&nbsp;&nbsp;\
                                                                <span class="kt-font-primary">101</span>\
                                                            </span> \
                                                        </div>
                                                    </div>\
                                                    <div class="martop foricon" style="background: #ffb822;">\
                                                        <span class="flaticon2-add-1"></span>\
                                                    </div>\
                                                    <div class="martop foricon" style="background: #ffb822;">\
                                                        <span class="flaticon2-add-1"></span>\
                                                    </div>\
                                                    <div class="martop foriconblock" style="background: #282a3c;">\
                                                        <span class="forcolor flaticon2-add-1"></span>\
                                                    </div>\
                                                    <div class="martop foriconempty" style="background: #1dc9b7;">\
                                                        <span class="forcolor flaticon2-add-1"></span>\
                                                    </div>\
                                                    <div style="padding-top: 8px"><span style="float:right;"><span class="flaticon2-menu-2"> </span></span></div>
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>
                            <div class="room"></div>

                        </div>\
                    </div>\
                </div>\
            </div>`      
        )
    }
    // --}}
</script>
{{-- Add Floor Script --}}
<script>
    $(document).ready(function(){
        $(document).on('click', '.add', function(){
            var html = '';
                html += '<div class="col-xl-12">\
                            <div class="kt-portlet kt-portlet--height-fluid">\
                                <div class="kt-portlet__head">\
                                    <div class="kt-portlet__head-label">\
                                        <h3 class="kt-portlet__head-title">\
                                            Floor\
                                        </h3>&nbsp;&nbsp;&nbsp;\
                                        <h3 class="kt-portlet__head-title">\
                                            01\
                                        </h3>\
                                    </div>\
                                    <div style="float:right;">\
                                        <span style="float:right; margin: 10px;">\
                                        <button style="padding-top:1rem" class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal" data-target="#modal_addrom">\
                                        <h5 class="add_room_button flaticon2-add-1">&nbsp;Add Room</h5></button>\
                                        </span>\
                                    </div>\
                                </div>\
                                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" style="margin-top: 10px;">\
                                    <div class="kt-portlet">\
                                        <div class="kt-portlet__body  kt-portlet__body--fit">\
                                            <div class="row row-no-padding row-col-separator-lg">\
                                                <div class="col-md-12 col-lg-12 col-xl-12">\
                                                    <div class="kt-widget24">\
                                                        <div class="kt-widget24__details">\
                                                            <div class="kt-widget24__info">\
                                                                <h4 style="line-height:0" class="kt-widget24__title">Room&nbsp;&nbsp;&nbsp;\
                                                                    <span>101</span>\
                                                                </h4>\
                                                                <span style="color: #48465b; font-size:12px">Single Bed</span>\
                                                            </div>\
                                                            <div>\
                                                                <a>\
                                                                    <i class="la la-remove"></i>\
                                                                </a>\
                                                            </div>\
                                                        </div>\
                                                        \
                                                        <div class="kt-widget24__action" style="margin-top:5px;">\
                                                            <div class="col-lg-3" style="border:1px solid rgba(0, 0, 0, 0.1); border-radius:10px; margin:2px; padding:5px; font-size:13px;">\
                                                                <div class="kt-widget24__info">\
                                                                    <span style="float:left; margin-top:-0.1rem" class="addcenter kt-widget4__icon">\
                                                                        <span style="padding-left:2px;" class="addcenter kt-widget24__title">Booked&nbsp;&nbsp;\
                                                                    <span class="kt-badge kt-badge--outline kt-badge--warning">+</span></span>\
                                                                    </span>\
                                                                    <span class="addcenter" style="padding-left:6px;">David Miller\
                                                                        <span style="padding-left:6px;">01-01-2020</span>&nbsp;to&nbsp;\
                                                                        <span style="">01-02-2020</span>\
                                                                    </span>\
                                                                </div>\
                                                            </div>\
                                                            <div class="col-lg-3" style="border:1px solid rgba(0, 0, 0, 0.1); border-radius:10px; margin:2px; padding:5px; font-size:13px;">\
                                                                <div class="kt-widget24__info">\
                                                                    <span style="padding-left:2px;" class="addcenter kt-widget24__title">Assign&nbsp;&nbsp;\
                                                                    <span class="kt-badge kt-badge--outline kt-badge--danger">+</span></span>\
                                                                </div>\
                                                            </div>\
                                                            <div class="col-lg-3" style="border:1px solid rgba(0, 0, 0, 0.1); border-radius:10px; margin:2px; padding:5px; font-size:13px;">\
                                                                <div class="kt-widget24__info">\
                                                                    <span style="padding-left:2px;" class="addcenter kt-widget24__title">Free&nbsp;&nbsp;\
                                                                    <span class="kt-badge kt-badge--outline kt-badge--success">+</span></span>\
                                                                </div>\
                                                            </div>\
                                                            <div class="col-lg-3" style="border:1px solid rgba(0, 0, 0, 0.1); border-radius:10px; margin:2px; padding:5px; font-size:13px;">\
                                                                <div class="kt-widget24__info">\
                                                                    <span style="float:left; margin-top:-0.1rem" class="addcenter kt-widget4__icon">\
                                                                        <span style="padding-left:2px;" class="addcenter kt-widget24__title">Block&nbsp;&nbsp;\
                                                                        <span class="kt-badge kt-badge--outline kt-badge--dark">+</span></span>\
                                                                    </span>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>';                       
            $('#item_table').append(html);
        });
        $(document).on('click', '.roomclass', function(){
            var html = '';
                html += '<div class="row"><div class="col-lg-6 col-xl-4"><div class="kt-portlet" style="background:#F9F9FC;"><div class="kt-infobox__section"><div class="kt-infobox__content"><div class="kt-widget kt-widget--user-profile-3"><div class="kt-widget24"><div class="kt-widget24__action"><div class="kt-widget24__info"><div style="background-color: rgba(34, 185, 255, 0.1);border-radius:50px; border:2px solid rgba(34, 185, 255, 0.1); padding:10px; margin-top:-0.5rem;">\
                <h4 >Room&nbsp;&nbsp;&nbsp;\
                <span >101</span>\
                </h4>\
                </div></div><div class="martop foricon" style="background: #ffb822;"><span class="flaticon2-add-1"></span></div><div class="martop foricon" style="background: #ffb822;"><span class="flaticon2-add-1"></span></div><div class="martop foriconblock" style="background: #282a3c;"><span class="forcolor flaticon2-add-1"></span></div><div class="martop foriconempty" style="background: #1dc9b7;"><span class="forcolor flaticon2-add-1"></span></div><div style="padding-top: 8px"><span style="float:right;"><span class="flaticon2-menu-2"> </span></span></div></div></div></div>';                       
            $('#roomid').append(html);
        });
    });
</script>
</body>
</html>
