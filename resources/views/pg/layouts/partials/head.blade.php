<base href="">
<meta charset="utf-8" />
<meta name="description" content="Latest updates and statistic charts">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
<link href="{{ asset ('admin/assets/plugins/custom/fullcalendar/fullcalendar.bundle.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset ('admin/assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset ('admin/assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="{{ asset ('admin/assets/media/logos/favicon.ico')}}" />
{{-- add floor --}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>