<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
    <div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">
        <!-- begin:: Aside Menu -->
        <div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
            <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
                <ul class="kt-menu__nav">
                    <li class="kt-menu__item {{ setActive('dashboard') }}" aria-haspopup="true"><a href="{{route('dashboard')}}" class="kt-menu__link "><i class="kt-menu__link-icon fa fa-tachometer-alt"></i><span class="kt-menu__link-text">Dashboard</span></a></li>
                    <li class="kt-menu__item {{ setActive('admin/resource') }}" aria-haspopup="true"><a href="{{route('addadmin_view')}}" class="kt-menu__link "><i class="kt-menu__link-icon fa fa-user"></i><span class="kt-menu__link-text">Admin</span></a></li>
                    <li class="kt-menu__item {{ setActive('package') }}" aria-haspopup="true"><a href="{{route('package')}}" class="kt-menu__link "><i class="kt-menu__link-icon fa fa-th-large"></i><span class="kt-menu__link-text">Packages</span></a></li>
                    <li class="kt-menu__item {{ setActive('pg/resource') }}" aria-haspopup="true"><a href="{{route('pg_view')}}" class="kt-menu__link "><i class="kt-menu__link-icon fa fa-building"></i><span class="kt-menu__link-text">PG</span></a></li>
                    <li class="kt-menu__item {{ setActive('Library/View') }}" aria-haspopup="true"><a href="{{route('library_view')}}" class="kt-menu__link "><i class="kt-menu__link-icon fa fa-book"></i><span class="kt-menu__link-text">Library</span></a></li>
                    {{-- <li class="kt-menu__item {{ setActive('Pg/History') }}" aria-haspopup="true"><a href="{{route('pghistory')}}" class="kt-menu__link "><i class="kt-menu__link-icon fa fa-book"></i><span class="kt-menu__link-text">History</span></a></li> --}}
                </ul>
            </div>
        </div>

        <!-- end:: Aside Menu -->
    </div>
    <!-- end:: Aside -->

    @include('notify::messages')