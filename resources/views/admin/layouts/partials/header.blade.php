@section('styles')
<style>
    ul.nav a {
        cursor: pointer;
    }
    .active {
        color:skyblue;
        font-weight:bolder;
    }

    .highlight {
        background-color: #00FFFF;
    }
</style>
@endsection
<div id="kt_header" class="kt-header kt-grid kt-grid--ver  kt-header--fixed ">
    {{-- <div class="kt-header__brand kt-grid__item " id="kt_header_brand">
        <div class="kt-header">        
        </div>
    </div> --}}
    <a href="">
        <img alt="Logo" src="{{asset('admin/assets/media/users/cybinfo_logo.1png.png')}}" style="padding:5px; height: 75px"/>
    </a>
    {{-- <h3 class="kt-header__title kt-grid__item">
        UMS
    </h3> --}}
    <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
    <div class="kt-header-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_header_menu_wrapper">
        <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">            
        </div>   
    </div>
    <div class="kt-header__topbar">
        <span style="margin-top: 30px; color:slategrey"><b>{{\Auth::user()->name}}</b></span>
        <div class="kt-header__topbar-item dropdown">
            <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                <span class="kt-header__topbar-icon kt-header__topbar-icon--success">
                    <i class="flaticon2-bell-alarm-symbol"></i>
                </span> 
            </div>
            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">
                <form>                   
                    <div class="tab-content">
                        <div class="tab-pane active show" id="topbar_notifications_notifications" role="tabpanel">
                            <div class="kt-notification kt-margin-t-10 kt-margin-b-10 kt-scroll" data-scroll="true" data-height="300" data-mobile-height="200">
                                @foreach($user as $users)
                                    @foreach ($users->notifications as $notification)
                                    <span class="kt-notification__item">                                       
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">
                                                <i class="flaticon2-box-1 kt-font-brand"></i>           
                                                <span style="color:navy; font-size:14px;"> {{ $notification->data['message'] }} </span>
                                            </div>        
                                        </div>
                                    </span>
                                    @endforeach 
                                @endforeach
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="kt-header__topbar-item kt-header__topbar-item--user">
            <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                <img class="kt-hidden" alt="Pic" src="assets/media/users/300_21.jpg" />
                <span class="kt-header__topbar-icon kt-hidden-"><i class="flaticon2-user-outline-symbol"></i></span>
            </div>
            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">
                <div class="kt-notification">
                    <a href="custom/apps/user/profile-1/personal-information.html" class="kt-notification__item">
                        <div class="kt-notification__item-icon">
                            <i class="flaticon2-calendar-3 kt-font-success"></i>
                        </div>
                        <div class="kt-notification__item-details">
                            <div class="kt-notification__item-title kt-font-bold">
                                My Profile
                            </div>
                            <div class="kt-notification__item-time">
                                Change Profile details
                            </div>
                        </div>
                    </a>
                    <div class="kt-notification__custom kt-space-between">
                        <a href="{{route('logout')}}" class="btn btn-label btn-label-brand btn-sm btn-bold">Sign Out</a>
                    </div>
                </div>
            </div>            
        </div>    
        <div class="kt-header__topbar-item kt-header__topbar-item--user">            
            <a href="" class="btn btn-brand btn-elevate btn-circle btn-icon" style="margin-top: 20px"><i class="flaticon2-settings"></i></a>
        </div>     
    </div>    
</div>