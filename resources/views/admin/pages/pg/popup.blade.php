{{-- edit profile details --}}
<div class="modal fade" id="kt_modal_pgname" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Profile details</h5>
            </button>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            </div>
            <form method="get" action="{{route('libraryupdate_store',['id'=>$view->user_id])}}">
                {{csrf_field()}}
                <input type="hidden" value="{{$view->id}}" name="id">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="exampleSelect1"> Pg Name</label>
                                    <input type="text" name="name" value="{{$view->name}}" class="form-control" style="width:500px;">
                                </div>
                                <div class="col-md-6">
                                    <label for="exampleSelect1">Select Pincode</label><br>
                                    <select class="pincode ui-select" id="kt_select2_4" name="pincode" style="width: 485px; ">
                                        <option></option>
                                        <optgroup label="Select Pincode">
                                            @foreach($pincode as $key => $pincode)
                                                <option value="{{$key}}">{{$pincode}}</option>
                                            @endforeach
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="exampleSelect1">State</label>
                                    <select class="form-control" disabled id="state" name="state">
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="exampleSelect1">City</label>
                                    <select class="form-control" id="city" disabled name="city">
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="exampleSelect1"> Address</label>
                                    <input type="text" name="address" class="form-control" value="{{$view->address}}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-4 wrapper1">
                                    <div class="row">
                                        <input type="email" name="email[]" style="margin-left: 10px;" class="form-control" value="{{$view->email}}">
                                        <button class="btn btn-info btn-sm add_email" style="height: 32px; margin-top: 8px; margin-left: 10px"> <i class="flaticon2-plus" style="margin-left:5px; margin-bottom: 2px"> </i></button>
                                    </div>
                                </div>
                                <div class="col-lg-4 wrapper2">
                                    <div class="row">
                                        <input type="text" name="landlineno[]" style="margin-left: 10px;" class="form-control" value="{{$view->landlineno}}">
                                        <button class="btn btn-info btn-sm add_landline" style="height: 32px; margin-top: 8px; margin-left: 10px"> <i class="flaticon2-plus" style="margin-left:5px; margin-bottom: 2px"> </i></button>
                                    </div>
                                </div>
                                <div class="col-lg-4 wrapper3">
                                    <div class="row">
                                        <input type="text" name="contact_no[]" style="margin-left: 10px;" class="form-control" value="{{$view->contact_no}}">
                                        <button class="btn btn-info btn-sm add_contact" style="height: 32px; margin-top: 8px; margin-left: 10px"> <i class="flaticon2-plus" style="margin-left:5px; margin-bottom: 2px"> </i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" style="border-radius:10px" data-dismiss="modal">Close</button>
                    <button type="submit" style="border-radius:10px" class="btn btn-info">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- edit profile status --}}
<div class="modal fade" id="kt_modal_top" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Update Profile Status</h5>
            </button>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            </div>
            <form method="get" action="{{route('profile_status',$view->user_id)}}">
                {{csrf_field()}}
                <input type="hidden" name="userid" value="{{$view->user_id}}">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="form-group">
                            <div class="row" style="margin-top:40px">
                                <div class="col-sm-4">
                                    <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand"> 
                                        <input type="radio" value="0" name="status"> Active <span></span>
                                    </label>
                                </div>
                                <div class="col-sm-4">
                                    <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand"> 
                                        <input type="radio" value="1" name="status"> Inactive <span></span>
                                    </label>
                                </div>
                                <div class="col-sm-4">
                                    <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand"> 
                                        <input type="radio" value="2" name="status"> Pending <span></span>
                                    </label>
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" style="border-radius:10px" data-dismiss="modal">Close</button>
                    <button type="submit" style="border-radius:10px" class="btn btn-info btn-sm">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- document detials --}}
<div class="modal fade" id="kt_modal_doc" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add Multiple Documents</h5>
            </button>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            </div>
            <form method="POST" action="{{route('documentmodal_store',['id'=>$view->user_id])}}" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="modal-body">
                    <input type="hidden" name="pg_id" value="{{$view->id}}">
                    <div class="kt-portlet__body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="field-wrapper">
                                        <input type="text" name="doc_type" style="width:250px;">
                                        <div class="field-placeholder"><span style="font-size: 14px;">Enter Document Type</span></div>
                                    </div>                        
                                </div>
                                <div class="col-sm-4">
                                    <div class="field-wrapper">
                                        <input type="text" name="doc_name" style="width:250px">
                                        <div class="field-placeholder"><span style="font-size: 14px">Enter Document Name</span></div>
                                    </div>                                
                                </div>
                                <div class="col-sm-4">
                                    <div class="field-wrapper">
                                        <input type="text" name="doc_number" style="width:250px">
                                        <div class="field-placeholder"><span style="font-size: 14px">Enter Document Number</span></div>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">                                                                                  
                            <div class="kt-avatar kt-avatar--outline" id="kt_user_avatar">                                                                                                  
                                <div class="kt-avatar__holder" style="background-image: url({{asset('admin/assets/media/users/doc.png')}})"></div>
                                <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change avatar">
                                    <i class="fa fa-pen"></i>
                                    <input type="file" name="multiple_doc">
                                </label>
                                <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
                                    <i class="fa fa-times"></i>
                                </span>                                                                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" style="border-radius: 5px" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-brand" style="border-radius: 5px">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- Add Owner --}}
<div class="modal fade" id="kt_modal_owner" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Owner Details</h5>
                </button>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            </div>
            <form method="POST" action="{{route('owner_details_store',['id'=>$view->user_id])}}" enctype="multipart/form-data">
                {{csrf_field()}}
                <input type="hidden" name="pg_id" value="{{$view->id}}">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group wrapper12">                                                                                                        
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="field-wrapper" >
                                                <input type="text" name="owner[]">
                                                <div class="field-placeholder"><span>Enter Owner Name</span></div>                                    
                                            </div>
                                        </div>
                                        <div class="col-lg-4"> 
                                            <button class="btn btn-brand btn-sm add_owner" style="margin-top: 8px;"><i class="flaticon2-plus" style="margin-left:5px; margin-bottom: 2px"> </i></button>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group wrapper13">
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="field-wrapper">
                                                <input type="email" name="email[]">
                                                <div class="field-placeholder"><span>Enter Email Address</span></div>                                    
                                            </div>                                            
                                        </div>
                                        <div class="col-lg-4">
                                            <button class="btn btn-brand btn-sm add_email" style="margin-top: 8px;"><i class="flaticon2-plus" style="margin-left:5px; margin-bottom: 2px"> </i></button>  
                                        </div>         
                                    </div>                                                               
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group wrapper14">                                 
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="field-wrapper">
                                                <input type="text" name="contactno[]">
                                                <div class="field-placeholder"><span>Enter Contact No</span></div>                                    
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <button class="btn btn-brand btn-sm add_contact" style="margin-top: 8px;"><i class="flaticon2-plus" style="margin-left:5px; margin-bottom: 2px"> </i></button>   
                                        </div>
                                    </div>                                
                                </div>
                            </div>
                        </div>                                                 
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="border-radius: 5px">Close</button>
                    <button type="submit" class="btn btn-brand" style="border-radius: 5px">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- owner Contact otp --}}
<div class="modal fade" id="kt_modal_v1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"> Enter Your Contact No Otp</h5>
            </button>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            </div>
            <form method="POST" action="" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-8 nopadding">
                            <div class="field-wrapper">
                                <input type="text" name="email" id="contact_timer">
                                <div class="field-placeholder"><span>Enter Your Otp</span></div>
                            </div>
                        </div>
                        <div class="col-sm-4 nopadding">
                            <button type="button" class="btn btn-brand btn-elevate btn-sm" style="margin-top: 5px; border-radius: 5px"> Resend OTP</button>
                        </div>
                    </div>
                    <div class="alert-text" style="font-size:15px;" id="contact_div"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" style="border-radius: 5px" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info btn-sm" style="border-radius: 5px">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- Add Mgtuser --}}
<div class="modal fade" id="kt_modal_mgtuser" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Update Managment User Details</h5>
            </button>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            </div>
            <form method="get" action="{{route('pgmanagment_store',$view->user_id)}}">
                {{csrf_field()}}
                <input type="hidden" name="pg_id" value="{{$view->id}}">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="form-group row">
                            <div class="col-lg-4">
                                <div class="field-wrapper" >
                                    <input type="text" name="name" style="width: 210px">
                                    <div class="field-placeholder"><span style="font-size: 13px">Enter Managmentuser</span></div>                                    
                                </div>                                
                            </div>
                            <div class="col-lg-4">
                                <div class="field-wrapper" >
                                    <input type="email" name="email_verification" style="width:210px">
                                    <div class="field-placeholder"><span style="font-size: 13px"> Enter Email Address</span></div>                                    
                                </div>                                
                            </div>
                            <div class="col-lg-4">
                                <div class="field-wrapper" >
                                    <input type="text" name="mobile_verification" style="width:210px">
                                    <div class="field-placeholder"><span style="font-size: 13px">Enter Contact No</span></div>                                    
                                </div>                                
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>Access Control</label>
                                <div></div>
                                <label class="kt-radio kt-radio--bold kt-radio--info">
                                    <input type="radio" name="access_control" value="read"> Read Access
                                    <span></span>
                                </label> 
                                <label class="kt-radio kt-radio--bold kt-radio--info" style="margin-left: 15px;">
                                    <input type="radio" name="access_control" value="write"> Write Access
                                    <span></span>
                                </label>
                            </div>
                            <div class="col-lg-6">
                                <label>Enable Feature</label>
                                <div></div>
                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--info">
                                    <input type="checkbox" name="enable_feature[]" value="payment"> Payment
                                    <span></span>
                                </label> 
                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--info" style="margin-left: 15px;">
                                    <input type="checkbox" name="enable_feature[]" value="complains"> Complains
                                    <span></span>
                                </label>
                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--info" style="margin-left: 15px;">
                                    <input type="checkbox" name="enable_feature[]" value="userprofile"> User Profile
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" style="border-radius: 5px" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-brand btn-sm" style="border-radius: 5px">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
