@extends('admin.layouts.app')
@section('title')
    <title>VIEW PG DETAILS | PG-MANAGMENT</title>
@endsection
@section('styles')
<link href="{{asset('admin/textbox.css')}}" rel="stylesheet">
<link href="{{asset('admin/assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
<style>
    .ui-select{width: 100%}
        /* This is to remove the arrow of select element in IE */
        select::-ms-expand {	display: none !important; }
        select{
            -webkit-appearance: none !important;
            appearance: none !important;
        }
    .dropdown {
        position: relative;
        display: inline-block;
    }
    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f9f9f9;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        padding: 12px 16px;
        z-index: 1;
    }
    .dropdown:hover .dropdown-content {
        display: block;
    }
	.btnn{
        background-color: #22b9ff;
        color:rgb(245, 243, 243);
        font-size: 12px;
        outline: none;
        margin-bottom: 5px;
    }
</style>
@endsection
@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <span style="float: left; margin-top: 15px; margin-bottom: 10px;">
            <div class="col-sm-1 dropdown">
                <span class="btnn dropdown btn btn-bold btn-label-brand btn-sm" data-toggle="dropdown" title="column Visibility"><span class="flaticon-visible"> </span></span>
                <div id="checkbox_div" class="dropdown-content">
                    <div class="form-group">
                        <div class="kt-checkbox-list">
                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
                                <input type="checkbox" value="hide" id="name_col" onchange="hide_show_table(this.id);">Name<br>
                                <span></span>
                            </label>
                        </div>
                        <div class="kt-checkbox-list">
                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
                                <input type="checkbox" value="hide" id="email_col" onchange="hide_show_table(this.id);">Email<br>
                                <span></span>
                            </label>
                        </div>
                        <div class="kt-checkbox-list">
                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
                                <input type="checkbox" value="hide" id="address_col" onchange="hide_show_table(this.id);">Address<br>
                                <span></span>
                            </label>
                        </div>
                        <div class="kt-checkbox-list">
                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
                                <input type="checkbox" value="hide" id="contactno_col" onchange="hide_show_table(this.id);">Contact No<br>
                                <span></span>
                            </label>
                        </div>                        
                        <div class="kt-checkbox-list">
                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
                                <input type="checkbox" value="hide" id="action_col" onchange="hide_show_table(this.id);">Action<br>
                                <span></span>
                            </label>
                        </div>
                    </div>  
                </div>
            </div>
        </span>
        {{-- <a href="{{route('pg_alldelete')}}" style="display: none; margin-top: 15px;" class="btn btn-bold btn-label-brand btn-sm" id="dvPassport"><span class="flaticon-delete"></span></a> --}}
        <span id="AddPassport"></span>
        <span class="dropdown dropdown-inline" style="float:right; margin-top: 15px; margin-right:10px; margin-bottom: 10px;">
            <button class="btn btn-bold btn-label-brand btn-sm" data-toggle="dropdown" title="Import"><span class="flaticon-upload"></span></button>
            <span class="dropdown-menu dropdown-menu-right"style="">
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#kt_modal_importpg"><i class="flaticon-upload-1"></i> Import </a>
                <a class="dropdown-item" href="{{route('pgexport')}}"><i class="flaticon2-download-2"></i> Export </a>
            </span>
        </span>
        <span class="dropdown dropdown-inline" style="float:right; margin-top: 15px; margin-right: 10px; margin-bottom: 10px;">
            <button type="button" class="btn btn-bold btn-label-brand btn-sm" data-toggle="dropdown" title="Export"><span class="flaticon-download-1"></span></button>
            <span class="dropdown-menu dropdown-menu-right">
                <ul class="kt-nav">
                    <li class="kt-nav__item">
                        <a href="#" class="kt-nav__link" onclick="selectElementContents( document.getElementById('kt_table_1'));">
                            <i class="kt-nav__link-icon flaticon2-copy"></i>
                            <span class="kt-nav__link-text">Copy</span>
                        </a>
                    </li>
                    <li class="kt-nav__item">
                        <a href="#" class="kt-nav__link" onclick="exportTableToExcel('kt_table_1')">
                            <i class="kt-nav__link-icon flaticon2-sheet"></i>
                            <span class="kt-nav__link-text">Excel</span>
                        </a>
                    </li>
                    <li class="kt-nav__item">
                        <a href="#" class="kt-nav__link export">
                            <i class="kt-nav__link-icon flaticon2-open-text-book"></i>
                            <span class="kt-nav__link-text">CSV</span>
                        </a>
                    </li>
                    <li class="kt-nav__item">
                        <a href="#" class="kt-nav__link" id="btnExport" onclick="Export()">
                            <i class="kt-nav__link-icon flaticon-file-2"></i>
                            <span class="kt-nav__link-text">PDF</span>
                        </a>
                    </li>
                    <li class="kt-nav__item">
                        <a href="#" class="kt-nav__link" id="print">
                            <i class="kt-nav__link-icon flaticon2-print"></i>
                            <span class="kt-nav__link-text">Print</span>
                        </a>
                    </li>
                </ul>
            </span>
        </span>
        <button class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal" style="float:right; margin-top: 15px; margin-right:10px; margin-bottom: 10px;" data-target="#kt_modal_6" title="Add PG"><span class="flaticon2-plus"> </span></button>
        <div class="kt-portlet__body kt-portlet__body--fit" style="margin-top: 70px;">
            <table class="table table-striped-  table-hover table-checkable" id="kt_table_1" style="background-color: white;">
                <thead>
                    <tr>
                        <td class="record_col">
                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand" style="margin-bottom: 12px"> 
                                <input type="checkbox" id="selectAll"> <span></span>
                            </label>
                        </td>
                        <th>Date</th>
                        <th id="name_col_head">Name</th>
                        <th id="email_col_head">Email</th>
                        <th id="contactno_col_head">Contact No</th>
                        <th id="address_col_head">Address</th>
                        <th id="status_col_head">Status</th>
                        <th id="action_col_head">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($user as $list)
                    <tr>
                        <td class="record_col">
                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand" style="margin-bottom: 12px"> 
                                <input type="checkbox"> <span></span>
                            </label>
                        </td>
                        <td>{{$list->created_at->format('m/d/y')}}</td>
                        <td class="name_col">{{$list->name}}</td>
                        <td class="email_col">{{$list->email}}</td>
                        <td class="contactno_col">{{$list->contact_no}}</td>
                        <td class="address_col">{{$list->address}}</td>
                        <td>
                            @if($list->status == "0")
                                <span class="kt-font-bold kt-font-success">Active</span>
                            @elseif($list->status == "1")
                                <span class="kt-font-bold kt-font-primary">Inactive</span>
                            @else
                                <span class="kt-font-bold kt-font-warning">Pending</span>
                            @endif
                        </td>
                        <td class="action_col">
                            <a href="{{route('profile',$list->user_id)}}" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="View Profile"> <i class="flaticon2-user"></i></a>
                            <a href="#" data-toggle="modal" data-target="#kt_modal_pgedit-{{$list->id}}" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Edit"><i class="flaticon2-file"></i></a>
                            <a href="{{route('review',$list->id)}}" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Renew"><i class="flaticon2-settings"></i></a>
                            <a href="#" data-toggle="modal" data-target="#kt_modal_del-{{$list->id}}" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Delete"><i class="flaticon2-delete"></i></a>
                        </td>                        
                    </tr>                    
                    {{-- update pg --}}
                    <div class="modal fade" id="kt_modal_pgedit-{{$list->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog modal-xl" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Edit Profile details</h5>
                                </button>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                </div>
                                <form method="get" action="{{route('pgupdate_store',['id'=>$list->user_id])}}">
                                    {{csrf_field()}}
                                    <input type="hidden" value="{{$list->id}}" name="id">
                                    <div class="modal-body">
                                        <div class="kt-portlet__body">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label for="exampleSelect1"> Pg Name</label>
                                                        <input type="text" name="name" value="{{$list->name}}" class="form-control">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="exampleSelect1">Select Pincode</label><br>
                                                        <select class="pincode ui-select" id="kt_select2_5" name="pincode" style="width:100% !important">
                                                            <optgroup label="Select Pincode">                                                                
                                                                @foreach($pincode as $key => $pincodes)
                                                                    <option value="{{$key}}">{{$pincodes}}</option>
                                                                @endforeach
                                                            </optgroup>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label for="exampleSelect1">State</label>
                                                        <select class="form-control" id="state" name="state">                                                             
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label for="exampleSelect1">City</label>
                                                        <select class="form-control" id="city" name="city">
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Address</label>
                                                        <input type="text" name="address" value="{{$list->address}}" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>Email</label>
                                                        <input type="email" name="email[]" value="{{$list->email}}" class="form-control">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Landline Number</label>
                                                        <input type="text" name="landlineno[]" value="{{$list->landlineno}}" class="form-control">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Contact Number</label>
                                                        <input type="text" name="contact_no[]" value="{{$list->contact_no}}" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" style="border-radius:10px" data-dismiss="modal">Close</button>
                                        <button type="submit" style="border-radius:10px" class="btn btn-info">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    {{-- delete confirmation --}}
                    <div class="modal fade" id="kt_modal_del-{{$list->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle">
                        <div class="modal-dialog modal-sm" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <h5> Are you sure to delete records ? </h5>
                                    <div class="swal2-actions">
                                        <a href="{{route('pg_destroy',$list->id)}}" class="swal2-confirm btn btn-sm btn-bold btn-danger" aria-label="" style="display: inline-block;">Yes, delete!</a>&nbsp;&nbsp;
                                        <button type="button" class="swal2-cancel btn btn-sm btn-bold btn-brand" aria-label="" style="display: inline-block;">No, cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
{{-- Pg Name --}}
<div class="modal fade" id="kt_modal_6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Register Your Pg</h5>
            </button>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            </div>
            <form method="POST" action="{{route('pg_details_store')}}">
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">                                  
                                    <label for="exampleSelect1" >Pg Name</label>  
                                    <div class="field-wrapper">                                        
                                        <input type="text" name="name">
                                        <div class="field-placeholder"><span>Name</span></div>
                                        <span class="text-red error">{{$errors->first('name')}}</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="exampleSelect1">Select Pincode</label><br>
                                    <select class="pincode1 ui-select" id="kt_select2_4" name="pincode" style="width:100% !important">
                                        <option></option>
                                        <optgroup label="Select Pincode">
                                            @foreach($pincode as $key => $pincodes)
                                                <option value="{{$key}}">{{$pincodes}}</option>
                                            @endforeach
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="exampleSelect1">State</label>
                                    <select class="form-control" id="state1" name="state">
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="exampleSelect1">City</label>
                                    <select class="form-control" id="city1" name="city">
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="exampleSelect1" >Address</label>  
                                    <div class="field-wrapper">
                                        <input type="text" name="address">
                                        <div class="field-placeholder"><span>Address</span></div>
                                    </div>
                                    <span class="text-red error">{{$errors->first('address')}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-4 wrapper1">
                                    <label for="exampleSelect1">Email</label>  
                                    <div class="row">                                        
                                        <div class="field-wrapper" style="margin-left: 10px;">
                                            <input type="email" name="email[]" required title="Enter Valid Email">
                                            <div class="field-placeholder "><span>Enter Your Email</span></div>
                                        </div>
                                        <button class="btn btn-info btn-sm add_email" style="height: 32px; margin-top: 8px; margin-left: 10px"> <i class="flaticon2-plus" style="margin-left:5px; margin-bottom: 2px"> </i></button> 
                                    </div>                                    
                                </div>
                                <div class="col-lg-4 wrapper2">
                                    <label for="exampleSelect1" >Landline Number</label>  
                                    <div class="row">
                                        <div class="field-wrapper" style="margin-left: 10px;">
                                            <input type="text" name="landlineno[]" pattern="^\d{10}$" required title="Enter Valid Landline Number">
                                            <div class="field-placeholder"><span>Enter Your Landline No</span></div>
                                        </div>
                                        <button class="btn btn-info btn-sm add_landline" style="height: 32px; margin-top: 8px; margin-left: 10px"> <i class="flaticon2-plus" style="margin-left:5px; margin-bottom: 2px"> </i></button>
                                    </div>
                                </div>
                                <div class="col-lg-4 wrapper3">
                                    <label for="exampleSelect1" >Contact Number</label>  
                                    <div class="row">
                                        <div class="field-wrapper" style="margin-left: 10px;">
                                            <input type="text" name="contact_no[]" pattern="^\d{10}$" required title="Enter Valid Contact Number">
                                            <div class="field-placeholder"><span>Enter Your Contact No</span></div>
                                        </div>
                                        <button class="btn btn-info btn-sm add_contact" style="height: 32px; margin-top: 8px; margin-left: 10px"> <i class="flaticon2-plus" style="margin-left:5px; margin-bottom: 2px"> </i></button>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" style="border-radius:10px" data-dismiss="modal">Close</button>
                    <button type="submit" style="border-radius:10px" class="btn btn-info">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- PG Import --}}
<div class="modal fade" id="kt_modal_importpg" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Import Pg Data</h5>
                </button>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            </div>
            <form method="post" action="{{route('pgimport')}}" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="modal-body">                    
                    <div class="form-group">
                        <div class="custom-file">
                            <input type="file" name="pgfile" class="custom-file-input" id="customFile">
                            <label class="custom-file-label" style="width:300px" for="customFile">Choose file</label>
                        </div>                  
                    </div>      
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal" style="border-radius: 5px">Close</button>
                        <button type="submit" class="btn btn-brand btn-sm" style="border-radius: 5px">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')
{{-- Doc allcheck --}}
<script>
    $('#selectAll').click(function (e) {
        $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);
    });
</script>
{{-- copy to clipboard --}}
<script>
    function selectElementContents(el) {
        var body = document.body, range, sel;
            if (document.createRange && window.getSelection) {
                range = document.createRange();
                sel = window.getSelection();
                sel.removeAllRanges();
                try {
                    range.selectNodeContents(el);
                    sel.addRange(range);
                } catch (e) {
                    range.selectNode(el);
                    sel.addRange(range);
                }
            } else if (body.createTextRange) {
                range = body.createTextRange();
                range.moveToElementText(el);
                range.copy();
            }
    }
</script>
{{-- Excel Export--}}
<script>
    function exportTableToExcel(tableID, filename = ''){
        var downloadLink;
        var dataType = 'application/vnd.ms-excel';
        var tableSelect = document.getElementById(tableID);
        var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
        // Specify file name
        filename = filename?filename+'.xls':'pgsheet.xls';
        // Create download link element
        downloadLink = document.createElement("a");
        document.body.appendChild(downloadLink);
        if(navigator.msSaveOrOpenBlob){
            var blob = new Blob(['\ufeff', tableHTML], {
                type: dataType
            });
            navigator.msSaveOrOpenBlob( blob, filename);
        }else{
            // Create a link to the file
            downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

            // Setting the file name
            downloadLink.download = filename;
            //triggering the function
            downloadLink.click();
        }
    }
</script>
{{-- CSV Export --}}
<script>
    $(document).ready(function() {

    function exportTableToCSV($table, filename) {
        var $rows = $table.find('tr:has(td)'),

        // Temporary delimiter characters unlikely to be typed by keyboard
        // This is to avoid accidentally splitting the actual contents
        tmpColDelim = String.fromCharCode(11), // vertical tab character
        tmpRowDelim = String.fromCharCode(0), // null character

        // actual delimiter characters for CSV format
        colDelim = '","',
        rowDelim = '"\r\n"',

        // Grab text from table into CSV formatted string
        csv = '"' + $rows.map(function(i, row) {
            var $row = $(row),
                $cols = $row.find('td');

        return $cols.map(function(j, col) {
            var $col = $(col),
            text = $col.text();

            return text.replace(/"/g, '""'); // escape double quotes

        }).get().join(tmpColDelim);

        }).get().join(tmpRowDelim)
            .split(tmpRowDelim).join(rowDelim)
            .split(tmpColDelim).join(colDelim) + '"';

        // Deliberate 'false', see comment below
        if (false && window.navigator.msSaveBlob) {

            var blob = new Blob([decodeURIComponent(csv)], {
            type: 'text/csv;charset=utf8'
        });

        // Crashes in IE 10, IE 11 and Microsoft Edge
        // See MS Edge Issue #10396033
        // Hence, the deliberate 'false'
        // This is here just for completeness
        // Remove the 'false' at your own risk
        window.navigator.msSaveBlob(blob, filename);

    } else if (window.Blob && window.URL) {
        // HTML5 Blob
        var blob = new Blob([csv], {
            type: 'text/csv;charset=utf-8'
        });
        var csvUrl = URL.createObjectURL(blob);

        $(this)
            .attr({
            'download': filename,
            'href': csvUrl
         });
    } else {
        // Data URI
        var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

        $(this)
            .attr({
            'download': filename,
            'href': csvData,
            'target': '_blank'
            });
        }
    }

    // This must be a hyperlink
    $(".export").on('click', function(event) {
    // CSV
        var args = [$('#kt_table_1>table'), 'pg.csv'];

        exportTableToCSV.apply(this, args);

        // If CSV, don't do event.preventDefault() or return false
        // We actually need this to be a typical hyperlink
        });
    });
</script>
{{-- PDF Export --}}
<script>
    function Export() {
        html2canvas(document.getElementById('kt_table_1'), {
            onrendered: function (canvas) {
                var data = canvas.toDataURL();
                var docDefinition = {
                    content: [{
                        image: data,
                        width: 500
                    }]
                };
                pdfMake.createPdf(docDefinition).download("pg.pdf");
            }
        });
    }
</script>
{{-- Print --}}
<script>
    function printData()
    {
    var divToPrint=document.getElementById("kt_table_1");
    newWin= window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    newWin.close();
    }

    $('#print').on('click',function(){
    printData();
    })
</script>
<!-- column visibility -->
<script>
    function hide_show_table(col_name){
        var checkbox_val=document.getElementById(col_name).value;
        if(checkbox_val=="hide"){
            var all_col=document.getElementsByClassName(col_name);
            for(var i=0;i<all_col.length;i++){
                all_col[i].style.display="none";
            }
            document.getElementById(col_name+"_head").style.display="none";
            document.getElementById(col_name).value="show";
        }
        else{
            var all_col=document.getElementsByClassName(col_name);
            for(var i=0;i<all_col.length;i++){
                all_col[i].style.display="table-cell";
            }
            document.getElementById(col_name+"_head").style.display="table-cell";
            document.getElementById(col_name).value="hide";
        }
    }
</script>
<script>
    $(function () {
        $("#chkPassport").click(function () {
            if ($(this).is(":checked")) {
                $("#dvPassport").show();
                $("#AddPassport").hide();
            } else {
                $("#dvPassport").hide();
                $("#AddPassport").show();
            }
        });
    });
</script>
{{-- field --}}
<script>
    $(function () {
        $(".field-wrapper .field-placeholder").on("click", function () {
        $(this).closest(".field-wrapper").find("input").focus();
    });
    $(".field-wrapper input").on("keyup", function () {
        var value = $.trim($(this).val());
        if (value) {
            $(this).closest(".field-wrapper").addClass("hasValue");
        } else {
            $(this).closest(".field-wrapper").removeClass("hasValue");
        }
        });
    });
</script>
{{-- country --}}
<script type="text/javascript">
    $('.pincode').change(function(){
    var PID = $(this).val();
    if(PID){
        $.ajax({
           type:"GET",
           url:"{{url('get-state-list')}}?pid_id="+PID,
           success:function(res){
            if(res){                
                $("#state").empty();
                // $("#state").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#state").append('<option value="'+key+'">'+value+'</option>');
                });

            }else{
               $("#state").empty();
            }
           }
        });
    }else{
        $("#state").empty();
        $("#city").empty();
    }
   });
    $('.pincode').on('change',function(){
    var stateID = $(this).val();
    if(stateID){
        $.ajax({
           type:"GET",
           url:"{{url('get-city-list')}}?pid_id="+stateID,
           success:function(res){
            if(res){
                $("#city").empty();
                $.each(res,function(key,value){
                    $("#city").append('<option value="'+key+'">'+value+'</option>');
                });

            }else{
               $("#city").empty();
            }
           }
        });
    }else{
        $("#city").empty();
    }
   });
</script>
{{-- country1 --}}
<script type="text/javascript">
    $('.pincode1').change(function(){
    var PID = $(this).val();
    if(PID){
        $.ajax({
           type:"GET",
           url:"{{url('get-state-list')}}?pid_id="+PID,
           success:function(res){
            if(res){                
                $("#state1").empty();
                // $("#state").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#state1").append('<option value="'+key+'">'+value+'</option>');
                });

            }else{
               $("#state1").empty();
            }
           }
        });
    }else{
        $("#state1").empty();
        $("#city1").empty();
    }
   });
    $('.pincode1').on('change',function(){
    var stateID = $(this).val();
    if(stateID){
        $.ajax({
           type:"GET",
           url:"{{url('get-city-list')}}?pid_id="+stateID,
           success:function(res){
            if(res){
                $("#city1").empty();
                $.each(res,function(key,value){
                    $("#city1").append('<option value="'+key+'">'+value+'</option>');
                });

            }else{
               $("#city1").empty();
            }
           }
        });
    }else{
        $("#city1").empty();
    }
   });
</script>
{{-- Multiple Mail --}}
<script type="text/javascript">
    $(document).ready(function() {
        var max_fields = 10; //Maximum allowed input fields
        var wrapper    = $(".wrapper1"); //Input fields wrapper
        var add_button = $(".add_email"); //Add button class or ID
        var x = 1; //Initial input field is set to 1

    //When user click on add input button
    $(add_button).click(function(e){
            e.preventDefault();
            //Check maximum allowed input fields
            if(x < max_fields){
                x++; //input field increment
                //add input field
                $(wrapper).append('<div><input type="text" name="email[]" placeholder="Enter Email" class="input1" /><a href="javascript:void(0);" style="margin-left:10px;" class="remove_field1 btn btn-brand btn-sm"><i class="fa fa-minus"></i></a></div>');
            }
        });

        //when user click on remove button
        $(wrapper).on("click",".remove_field1", function(e){
            e.preventDefault();
    $(this).parent('div').remove(); //remove inout field
    x--; //inout field decrement
        })
    });
</script>
{{-- multiple landline no --}}
<script type="text/javascript">
    $(document).ready(function() {
        var max_fields = 10; //Maximum allowed input fields
        var wrapper    = $(".wrapper2"); //Input fields wrapper
        var add_button = $(".add_landline"); //Add button class or ID
        var x = 1; //Initial input field is set to 1

    //When user click on add input button
    $(add_button).click(function(e){
            e.preventDefault();
            //Check maximum allowed input fields
            if(x < max_fields){
                x++; //input field increment
                //add input field
                $(wrapper).append('<div><input type="text" name="landlineno[]" placeholder="Enter Landline No" class="input1" /><a href="javascript:void(0);" style="margin-left:10px;" class="remove_field2 btn btn-brand btn-sm"><i class="fa fa-minus"></i></a></div>');
            }
        });

        //when user click on remove button
        $(wrapper).on("click",".remove_field2", function(e){
            e.preventDefault();
    $(this).parent('div').remove(); //remove inout field
    x--; //inout field decrement
        })
    });
</script>
{{-- multiple contact no --}}
<script type="text/javascript">
    $(document).ready(function() {
        var max_fields = 10; //Maximum allowed input fields
        var wrapper    = $(".wrapper3"); //Input fields wrapper
        var add_button = $(".add_contact"); //Add button class or ID
        var x = 1; //Initial input field is set to 1

    //When user click on add input button
    $(add_button).click(function(e){
            e.preventDefault();
            //Check maximum allowed input fields
            if(x < max_fields){
                x++; //input field increment
                //add input field
                $(wrapper).append('<div><input type="text" name="contact_no[]" placeholder="Enter Contact No" class="input1" /><a href="javascript:void(0);" style="margin-left:10px;" class="remove_field2 btn btn-brand btn-sm"><i class="fa fa-minus"></i></a></div>');
            }
        });

        //when user click on remove button
        $(wrapper).on("click",".remove_field2", function(e){
            e.preventDefault();
    $(this).parent('div').remove(); //remove inout field
    x--; //inout field decrement
        })
    });
</script>
<script src="{{asset('admin/assets/js/scripts.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/assets/js/pages/crud/datatables/advanced/column-visibility.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/assets/plugins/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/assets/js/pages/crud/forms/widgets/select2.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
@endsection
