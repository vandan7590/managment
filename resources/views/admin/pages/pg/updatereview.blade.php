@extends('admin.layouts.app')
@section('title')
    <title> RENEW DETAILS | PG-MANAGMENT</title>
@endsection
@section('content')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" style="background: #F0F3FF">
    <div class="kt-portlet kt-portlet--mobile" style="background: #F0F3FF">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h4 style="margin-top:20px"><i class="fa fa-plus-circle"> </i> Renew Details</h4>
            </div>
        </div>
    </div>
    @if(!$edit)
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet">
                <form class="kt-form kt-form--label-right" method="post" action="{{route('review_store',['id'=>$pg])}}">
                    {{csrf_field()}}
                    <input type="hidden" name="pg_id" value="{{$pg}}">
                    <div class="kt-portlet__body">
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label>Start Date</label>
                                    <div class="input-group date">
                                        <input type="text" class="form-control" name="start_date" readonly placeholder="Select date" id="kt_datepicker_2" />
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                        </div>
                                    </div>
                                <span class="error" style="color:red;">{{ $errors->first('start_date') }}</span>
                            </div>
                            <div class="col-lg-3">
                                <label>End Date</label>
                                <div class="input-group date">
                                    <input type="text" class="form-control" name="end_date" readonly placeholder="Select date" id="kt_datepicker_2" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                </div>
                                <span class="error" style="color:red;">{{ $errors->first('end_date') }}</span>
                            </div>   
                            <div class="col-lg-3">
                                <label>Renew Reminder Date</label>
                                <div class="input-group date">
                                    <input type="text" class="form-control" name="renew_reminder_date" readonly placeholder="Select date" id="kt_datepicker_2" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                </div>
                                <span class="error" style="color:red;">{{ $errors->first('renew_reminder_date') }}</span>
                            </div>
                            <div class="col-lg-3">
                                <label>Expire Date</label>
                                <div class="input-group date">
                                    <input type="text" class="form-control" name="expire_date" readonly placeholder="Select date" id="kt_datepicker_2" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>                                    
                            <span class="error" style="color:red;">{{ $errors->first('expire_date') }}</span>
                        </div>                        
                        <div class="form-group row">
                            <div class="col-lg-4">
                                <label for="exampleSelect1">Package</label>
                                <select class="form-control" name="package" id="exampleSelect1">
                                    @foreach($package as $list)
                                        <option>{{ucfirst(trans($list->name))}} : {{$list->amount}}</option>
                                    @endforeach
                                </select>
                                <span class="error" style="color:red;">{{ $errors->first('package') }}</span>
                            </div>
                            <div class="col-lg-4">
                                <label>Date of Payment</label>
                                <div class="input-group date">
                                    <input type="text" class="form-control" name="date_of_payment" readonly placeholder="Select date" id="kt_datepicker_2" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                </div>
                                <span class="error" style="color:red;">{{ $errors->first('date_of_payment') }}</span>
                            </div>                                 
                            <div class="col-lg-4">
                                <label>Date of Renew</label>
                                <div class="input-group date">
                                    <input type="text" class="form-control" name="date_of_renew" readonly placeholder="Select date" id="kt_datepicker_2" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                </div>
                                <span class="error" style="color:red;">{{ $errors->first('date_of_renew') }}</span>
                            </div>
                        </div>
                    </div>                    
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-6">
                                    <button type="submit" style="border-radius:5px" class="btn btn-brand btn-sm">Save</button>
                                    <a href="{{route('pg_view')}}" style="border-radius:5px" class="btn btn-secondary btn-sm">Cancel</a>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @else
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet">
                <form class="kt-form kt-form--label-right" method="get" action="{{route('updatereview_store',$edit->pg_id)}}">
                    {{csrf_field()}}
                    <input type="hidden" value="{{$edit->pg_id}}" name="pg_id">
                    <div class="kt-portlet__body">
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label>Start Date</label>
                                    <div class="input-group date">
                                        <input type="text" class="form-control" name="start_date" readonly value="{{$edit->start_date}}" id="kt_datepicker_2" />
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                        </div>
                                    </div>
                                <span class="error" style="color:red;">{{ $errors->first('start_date') }}</span>
                            </div>
                            <div class="col-lg-3">
                                <label>End Date</label>
                                <div class="input-group date">
                                    <input type="text" class="form-control" name="end_date" readonly value="{{$edit->end_date}}" id="kt_datepicker_2" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                </div>
                                <span class="error" style="color:red;">{{ $errors->first('end_date') }}</span>
                            </div>   
                            <div class="col-lg-3">
                                <label>Renew Reminder Date</label>
                                <div class="input-group date">
                                    <input type="text" class="form-control" name="renew_reminder_date" readonly value="{{$edit->renew_reminder_date}}" id="kt_datepicker_2" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                </div>
                                <span class="error" style="color:red;">{{ $errors->first('renew_reminder_date') }}</span>
                            </div>
                            <div class="col-lg-3">
                                <label>Expire Date</label>
                                <div class="input-group date">
                                    <input type="text" class="form-control" name="expire_date" readonly value="{{$edit->expire_date}}" id="kt_datepicker_2" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>                                    
                            <span class="error" style="color:red;">{{ $errors->first('expire_date') }}</span>
                        </div>                        
                        <div class="form-group row">
                            <div class="col-lg-4">
                                <label for="exampleSelect1">Package</label>
                                <select class="form-control" name="package" id="exampleSelect1">
                                    @foreach($package as $list)
                                        <option>{{ucfirst(trans($list->name))}} : {{$list->amount}}</option>
                                    @endforeach
                                </select>
                                <span class="error" style="color:red;">{{ $errors->first('package') }}</span>
                            </div>
                            <div class="col-lg-4">
                                <label>Date of Payment</label>
                                <div class="input-group date">
                                    <input type="text" class="form-control" name="date_of_payment" readonly value="{{$edit->date_of_payment}}" id="kt_datepicker_2" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                </div>
                                <span class="error" style="color:red;">{{ $errors->first('date_of_payment') }}</span>
                            </div>                                 
                            <div class="col-lg-4">
                                <label>Date of Renew</label>
                                <div class="input-group date">
                                    <input type="text" class="form-control" name="date_of_renew" readonly value="{{$edit->date_of_renew}}" id="kt_datepicker_2" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                </div>
                                <span class="error" style="color:red;">{{ $errors->first('date_of_renew') }}</span>
                            </div>
                        </div>
                    </div>                    
                    <div class="kt-portlet__foot" style="float:right">
                        <div class="kt-form__actions" >
                            <button type="submit" style="border-radius:10px" class="btn btn-brand btn-sm">Update</button>
                            <a href="#" style="border-radius:10px" class="btn btn-secondary btn-sm">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endif 
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <table class="table table-striped-  table-hover table-checkable" id="kt_table_1" style="background-color: white;">
                        <thead>
                            <tr style="text-align: center">
                                <th>
                                    <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand" style="margin-bottom: 12px"> 
                                        <input type="checkbox" id="selectAll"> <span></span>
                                    </label>
                                </th>
                                <th id="type_col_head">Start Date</th>
                                <th id="name_col_head">End Date</th>
                                <th id="doc_no_col_head">Renew ReminderDate</th>
                                <th id="doc_no_col_head">Expire Date</th>
                                <th id="doc_no_col_head">Package</th>
                                <th id="doc_no_col_head">Date of Payment</th>
                                <th id="doc_no_col_head">Date of Renew</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($update as $list)
                            <tr style="text-align: center">
                                <td>
                                    <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand" style="margin-bottom: 12px"> 
                                        <input type="checkbox"> <span></span>
                                    </label>    
                                </td>                                
                                <td> {{$list->start_date}} </td>
                                <td> {{$list->end_date}} </td>
                                <td> {{$list->renew_reminder_date}} </td>
                                <td> {{$list->expire_date}} </td>
                                <td> {{$list->package}} </td>
                                <td> {{$list->date_of_payment}} </td>
                                <td> {{$list->date_of_renew}} </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>                    
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        $('#local_record_selection').DataTable( {
            dom: 'Bfrtip',
            buttons: [{
                    extend: 'colvis',
                    columns: ':not(.noVis)',
                },
            ],
        } );
    } );
</script>
{{-- Doc allcheck --}}
<script>
    $('#selectAll').click(function (e) {
        $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);
    });
</script>
{{-- copy to clipboard --}}
<script>
    function selectElementContents(el) {
        var body = document.body, range, sel;
            if (document.createRange && window.getSelection) {
                range = document.createRange();
                sel = window.getSelection();
                sel.removeAllRanges();
                try {
                    range.selectNodeContents(el);
                    sel.addRange(range);
                } catch (e) {
                    range.selectNode(el);
                    sel.addRange(range);
                }
            } else if (body.createTextRange) {
                range = body.createTextRange();
                range.moveToElementText(el);
                range.copy();
            }
    }
</script>
{{-- Excel Export--}}
<script>
    function exportTableToExcel(tableID, filename = ''){
        var downloadLink;
        var dataType = 'application/vnd.ms-excel';
        var tableSelect = document.getElementById(tableID);
        var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
        // Specify file name
        filename = filename?filename+'.xls':'pg_excelsheet.xls';
        // Create download link element
        downloadLink = document.createElement("a");
        document.body.appendChild(downloadLink);
        if(navigator.msSaveOrOpenBlob){
            var blob = new Blob(['\ufeff', tableHTML], {
                type: dataType
            });
            navigator.msSaveOrOpenBlob( blob, filename);
        }else{
            // Create a link to the file
            downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
        
            // Setting the file name
            downloadLink.download = filename;
            //triggering the function
            downloadLink.click();
        }
    }
</script>
{{-- CSV Export --}}
<script>
    $(document).ready(function() {

    function exportTableToCSV($table, filename) {
        var $rows = $table.find('tr:has(td)'),

        // Temporary delimiter characters unlikely to be typed by keyboard
        // This is to avoid accidentally splitting the actual contents
        tmpColDelim = String.fromCharCode(11), // vertical tab character
        tmpRowDelim = String.fromCharCode(0), // null character

        // actual delimiter characters for CSV format
        colDelim = '","',
        rowDelim = '"\r\n"',

        // Grab text from table into CSV formatted string
        csv = '"' + $rows.map(function(i, row) {
            var $row = $(row),
                $cols = $row.find('td');

        return $cols.map(function(j, col) {
            var $col = $(col),
            text = $col.text();

            return text.replace(/"/g, '""'); // escape double quotes

        }).get().join(tmpColDelim);

        }).get().join(tmpRowDelim)
            .split(tmpRowDelim).join(rowDelim)
            .split(tmpColDelim).join(colDelim) + '"';

        // Deliberate 'false', see comment below
        if (false && window.navigator.msSaveBlob) {

            var blob = new Blob([decodeURIComponent(csv)], {
            type: 'text/csv;charset=utf8'
        });

        // Crashes in IE 10, IE 11 and Microsoft Edge
        // See MS Edge Issue #10396033
        // Hence, the deliberate 'false'
        // This is here just for completeness
        // Remove the 'false' at your own risk
        window.navigator.msSaveBlob(blob, filename);

    } else if (window.Blob && window.URL) {
        // HTML5 Blob        
        var blob = new Blob([csv], {
            type: 'text/csv;charset=utf-8'
        });
        var csvUrl = URL.createObjectURL(blob);

        $(this)
            .attr({
            'download': filename,
            'href': csvUrl
         });
    } else {
        // Data URI
        var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

        $(this)
            .attr({
            'download': filename,
            'href': csvData,
            'target': '_blank'
            });
        }
    }

    // This must be a hyperlink
    $(".export").on('click', function(event) {
    // CSV
        var args = [$('#local_record_selection>table'), 'pg.csv'];

        exportTableToCSV.apply(this, args);

        // If CSV, don't do event.preventDefault() or return false
        // We actually need this to be a typical hyperlink
        });
    });
</script>
{{-- PDF Export --}}
<script>
    function Export() {
        html2canvas(document.getElementById('local_record_selection'), {
            onrendered: function (canvas) {
                var data = canvas.toDataURL();
                var docDefinition = {
                    content: [{
                        image: data,
                        width: 500
                    }]
                };
                pdfMake.createPdf(docDefinition).download("PG.pdf");
            }
        });
    }
</script>
{{-- Print --}}
<script>
    function printData()
    {
    var divToPrint=document.getElementById("local_record_selection");
    newWin= window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    newWin.close();
    }

    $('#print').on('click',function(){
    printData();
    })
</script>
<script src="{{asset('admin/assets/js/scripts.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/assets/js/pages/crud/metronic-datatable/advanced/renew.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
<script src="{{asset('admin/assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>
@endsection