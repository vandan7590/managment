@extends('admin.layouts.app')
@section('title')
    <title>VIEW PG PROFILE DETAILS | PG-MANAGMENT</title>
@endsection
@section('styles')
{{-- <style>
    .nopadding {
    padding: 3px !important;
    /* margin: 0 !important; */
    }
</style> --}}
<link href="{{asset('admin/file.css')}}" rel="stylesheet">
<link href="{{asset('admin/textbox.css')}}" rel="stylesheet">
<link href="{{asset('admin/assets/view.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" style="background:#F3F2F6; margin-top: 10px;">
        <div class="row" style="margin-top:2px">
            <div class="col-xl-12">
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__body">
                        <div class="row kt-widget kt-widget--user-profile-3">
                            <div class="kt-widget__top col-sm-10">
                                <div class="kt-widget__media kt-hidden-">
                                    @if($view->pgimage)
                                    <form method="post" action="{{route('image_update',['id' => $view->user_id])}}" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <input type="hidden" value="{{$view->user_id}}" name="user_id">
                                        <div class="col-lg-9 col-xl-6">
                                            <div class="kt-avatar kt-avatar--outline" id="kt_user_avatar">                                                                                                  
                                                <div class="kt-avatar__holder" style="background-image: url({{asset('/PG/PGProfile/'.$view->user_id.'/'.$view->pgimage)}})"></div>
                                                <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change avatar">
                                                    <i class="fa fa-pen"></i>
                                                    <input type="file" name="pgimage" accept=".png, .jpg, .jpeg">
                                                </label>
                                                <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
                                                    <i class="fa fa-times"></i>
                                                </span>                                                
                                            </div>                                            
                                        </div>
                                        <button class="btn btn-brand btn-sm" style="margin-left: 14px; margin-top:7px; border-radius: 5px">Image Upload</button>
                                    </form>
                                    @else
                                    <form method="post" action="{{route('image_update',['user_id' => $view->user_id])}}" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <input type="hidden" value="{{$view->user_id}}" name="user_id">
                                        <div class="col-lg-9 col-xl-6">
                                            <div class="kt-avatar kt-avatar--outline" id="kt_user_avatar">                                                                           
                                                <div class="kt-avatar__holder" style="background-image: url({{asset('admin/assets/media/users/pgimg.jpg')}})"></div>
                                                <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change avatar">
                                                    <i class="fa fa-pen"></i>
                                                    <input type="file" name="pgimage" accept=".png, .jpg, .jpeg">
                                                </label>
                                                <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
                                                    <i class="fa fa-times"></i>
                                                </span>                                                
                                            </div>                                            
                                        </div>
                                        <button class="btn btn-brand btn-sm" style="margin-left: 20px; margin-top:7px; border-radius: 5px">Image Upload</button>
                                    </form>
                                    @endif
                                </div>                         
                                <div class="kt-widget__content">
                                    <div class="kt-widget__head" style="margin-top: 30px">
                                        <a href="#" class="kt-widget__username">
                                            {{$view->name}}                                        
                                        </a>                                            
                                    </div>
                                    <div class="kt-widget__action">
                                        @if($view->status == '0')         
                                            <span style="color:#1dc9b7" data-toggle="modal" data-target="#kt_modal_top">Active</span>
                                        @elseif($view->status == '1')
                                            <span style="color:tomato" data-toggle="modal" data-target="#kt_modal_top">Inactive</span>
                                        @elseif($view->status == '2')
                                            <span style="color:yellowgreen" data-toggle="modal" data-target="#kt_modal_top">Pending</span>
                                        @endif  
                                    </div>
                                </div>
                            </div>                            
                            <div class="kt-widget__top col-sm-2">
                                <button class="btn" style="background-color: #E2F2FB; float: right; margin-left: 120px; text-transform: uppercase;">{{$view->unique_id}}</button>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-12" style="margin-top:-1.3rem">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-toolbar">
                            <ul class="nav nav-pills nav-pills-sm" role="tablist" id="myTab">
                                <li class="nav-item active">
                                    <a class="nav-link" data-toggle="tab" href="#kt_tabs_7_4" role="tab">
                                        Profile Details
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#kt_tabs_7_5" role="tab">
                                        Image
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#kt_tabs_setting" role="tab">
                                        Setting
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#kt_tabs_7_1" role="tab">
                                        Document
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#kt_tabs_7_2" role="tab">
                                        Owner
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#kt_tabs_7_3" role="tab">
                                        Managment User
                                    </a>
                                </li>                                
                            </ul>
                        </div>
                    </div>
                    <div class="kt-portlet__body" style="height:auto">
                        <div class="tab-content">
                            {{-- Profile --}}
                            <div class="tab-pane active" id="kt_tabs_7_4" role="tabpanel">
                                <div class="kt-portlet__body" style="padding: 0 !important">
                                    <div>
                                        <button class="btn btn-sm" data-toggle="modal" data-target="#kt_modal_pgname" title="Edit Profile Details" style="background-color: #E2F2FB; float: right;"><i class="flaticon-edit"></i></button>
                                    </div>
                                    <div class="row kt-widget kt-widget--user-profile-3">
                                        <div class="kt-widget__top col-sm-6">
                                            <div class="kt-widget__content">
                                                <div class="kt-widget__head">
                                                    <a href="#" class="kt-widget__username">
                                                        {{$view->name}}
                                                    </a>
                                                </div>
                                                <div class="kt-widget5__info" style="padding-top:5px;">
                                                    <span>Email:</span>
                                                <span class="kt-font-info">&nbsp;{{$view->email}}</span>
                                                </div>
                                                <div class="kt-widget5__info" style="padding-top:5px;">
                                                    <span>Contact No:</span>
                                                    <span class="kt-font-info">&nbsp;{{$view->contact_no}}</span>
                                                </div>
                                                <div class="kt-widget5__info" style="padding-top:5px;">
                                                    <span>Address:</span>
                                                    <span class="kt-font-info">&nbsp;{{$view->address}}</span>
                                                    <span style="padding-left:10px;">City:</span>                                                    
                                                    @foreach($city as $list)                     
                                                    @if($view->city == $list->id)                                                                                         
                                                        <span class="kt-font-info">&nbsp;{{$list->city}}</span>
                                                    @endif
                                                    @endforeach
                                                    <span style="padding-left:10px;">PinCode:</span>
                                                    @foreach($pincodes as $list)                     
                                                    @if($view->pincode == $list->id)                                                                                         
                                                        <span class="kt-font-info">&nbsp;{{$list->pincode}}</span>
                                                    @endif
                                                    @endforeach                                                    
                                                    <span style="padding-left:10px;">State:</span>
                                                    @foreach($state as $list)                     
                                                    @if($view->state == $list->id)                                                                                         
                                                        <span class="kt-font-info">&nbsp;{{$list->state}}</span>
                                                    @endif
                                                    @endforeach                                                                                                        
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-widget__top col-sm-6">
                                           <div class="kt-portlet">
                                                <div class="kt-portlet__body  kt-portlet__body--fit">
                                                    <div class="row row-no-padding row-col-separator-lg">
                                                        <div class="col-md-12 col-lg-6 col-xl-4">
                                                            <div class="kt-widget24">
                                                                <div class="kt-widget24__details">
                                                                    <div class="kt-widget24__info">
                                                                        <h4 class="kt-widget24__title">
                                                                            Email
                                                                        </h4>
                                                                    </div>
                                                                </div>
                                                                <div class="kt-widget24__action">
                                                                    <span class="kt-widget24__change">
                                                                        {{$view->email}}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-lg-6 col-xl-4">
                                                            <div class="kt-widget24">
                                                                <div class="kt-widget24__details">
                                                                    <div class="kt-widget24__info">
                                                                        <h4 class="kt-widget24__title">
                                                                            Landline Number
                                                                        </h4>
                                                                    </div>
                                                                </div>
                                                                <div class="kt-widget24__action">
                                                                    <span class="kt-widget24__change">
                                                                        {{$view->landlineno}}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-lg-6 col-xl-4">
                                                            <div class="kt-widget24">
                                                                <div class="kt-widget24__details">
                                                                    <div class="kt-widget24__info">
                                                                        <h4 class="kt-widget24__title">
                                                                            Contact Number
                                                                        </h4>
                                                                    </div>
                                                                </div>
                                                                <div class="kt-widget24__action">
                                                                    <span class="kt-widget24__change">
                                                                        {{$view->contact_no}}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- Image --}}
                            <div class="tab-pane" id="kt_tabs_7_5" role="tabpanel">
                                <div class="kt-portlet__body">
                                    <div class="row">                                                                                
                                        <div>                                            
                                            <div class="kt-avatar kt-avatar--outline" id="kt_user_avatar">
                                                <div class="kt-avatar__holder" style="background-image: url({{asset('/PG/PGProfile/'.$view->user_id.'/'.$view->pgimage)}})"></div><br>
                                                <a href="{{route('remove_image',['user_id'=>$view->user_id])}}" class="btn btn-brand btn-sm" style="margin-left: 2px; border-radius: 5px">Remove Image</a>
                                            </div>                                             
                                        </div>
                                        <form action="{{route('image_update',$view->user_id)}}" method="post" enctype="multipart/form-data">
                                            {{csrf_field()}}
                                            <div style="margin-left: 100px; margin-top: 25px;">
                                                <div class="form-group">
                                                    <label>Upload Profile Image</label>
                                                    <div></div>
                                                    <div class="custom-file">
                                                        <input type="file" name="pgimage" class="custom-file-input" id="customFile">
                                                        <label class="custom-file-label" style="width:200px" for="customFile">Choose file</label>
                                                    </div>                                                
                                                </div>    
                                                <button type="submit" class="btn btn-brand btn-sm" style="float: right; border-radius: 5px">Upload Profile Image</button>
                                            </div>
                                        </form>                                        
                                    </div>
                                </div>
                            </div>
                            {{-- Document --}}
                            <div class="tab-pane" id="kt_tabs_7_1" role="tabpanel">
                                <button class="btn btn-bold btn-label-brand btn-sm" style="float: right" data-toggle="modal" data-target="#kt_modal_doc" title="Add Document"><span class="flaticon2-plus"> </span></button>
                                <div class="kt-portlet__body">
                                    <table class="table table-striped- table-hover table-checkable" id="kt_table_1" style="background-color: white;">
                                        <thead>
                                            <tr style="text-align: center">
                                                <th id="record_col_head" style="width:5%"><label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand"> <input type="checkbox" id="selectall2"> <span></span></label></th>
                                                <th id="type_col_head" style="width:23.7%">Type</th>
                                                <th id="name_col_head" style="width:23.7%">Name</th>
                                                <th id="doc_no_col_head" style="width:23.7%">Document Number</th>
                                                <th id="action_col_head" style="width:23.7%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($doc as $list)                                             
                                            <tr style="text-align: center">
                                                <td class="record_col"><label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand "> <input type="checkbox" id="chkPassport" class="case"> <span></span></label></td>
                                                <td class="type_col">{{$list->doc_type}}</td>
                                                <td class="name_col">{{$list->doc_name}}</td>
                                                <td class="doc_no_col">{{$list->doc_number}}</td>
                                                <td class="action_col">
                                                    <span style="overflow: visible; position: relative; width: 80px;">
                                                        <div class="dropdown">
                                                            <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md"><i class="flaticon-more-1"></i></a>
                                                            <div class="dropdown-menu dropdown-menu-right">
                                                                <ul class="kt-nav">  
                                                                    <li class="kt-nav__item">
                                                                        <a class="kt-nav__link" href="{{route('downloadfile',$list->id)}}" download=""><i class="kt-nav__link-icon flaticon2-download"></i><span class="kt-nav__link-text">Download</span></a>
                                                                    </li>
                                                                    <li class="kt-nav__item"> 
                                                                        <a class="kt-nav__link" href="{{route('document_delete',['id'=>$list->id])}}"><i class="kt-nav__link-icon flaticon2-rubbish-bin-delete-button"></i><span class="kt-nav__link-text">Delete</span></a> 
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </span>
                                                </td>
                                            </tr>                                                
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {{-- Owner --}}
                            <div class="tab-pane" id="kt_tabs_7_2" role="tabpanel">
                                <button class="btn btn-bold btn-label-brand btn-sm" style="float: right" data-toggle="modal" data-target="#kt_modal_owner" title="Add Owner"><span class="flaticon2-plus"> </span></button>
                                <div class="kt-portlet__body">
                                    <table class="table table-striped-  table-hover table-checkable" id="kt_table_1" style="background-color: white;">
                                        <thead>
                                            <tr style="text-align: center">
                                                <th id="record_col_head" style="width:5%"><label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand"> <input type="checkbox"  id="selectAll1"> <span></span></label></th>
                                                <th id="type_col_head" style="width:23%">Owner Name</th>
                                                <th id="name_col_head" style="width:23%">Email</th>
                                                <th id="doc_no_col_head" style="width:23%">Contact No</th>
                                                <th id="action_col_head" style="width:23%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($owner as $list)
                                                <tr style="text-align: center">
                                                    <td class="record_col"><label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand"> <input type="checkbox" class="case1"> <span></span></label></td>
                                                    <td class="type_col">{{$list->owner}}</td>
                                                    <td class="name_col">{{$list->email}}
                                                        @if($list->verify == '1')
                                                            <a class="kt-nav__link" href="#"><i class="kt-nav__link-icon flaticon2-check-mark"></i></a>
                                                        @else
                                                            <a class="kt-nav__link" href="#" data-toggle="modal" data-target="#kt_modal_pgowneremail-{{$list->id}}"><i class="kt-nav__link-icon flaticon2-delete"></i></a>
                                                            {{-- owner email otp --}}
                                                            <div class="modal fade" id="kt_modal_pgowneremail-{{$list->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title"> Enter Your Email Otp</h5>
                                                                        </button>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        </div>
                                                                        <form method="get" action="{{route('pgotpverify',$list->id)}}">
                                                                            {{csrf_field()}}
                                                                            <input type="hidden" value="{{$list->pg_id}}" name="pg_id">
                                                                            <div class="modal-body">
                                                                                <div class="row">
                                                                                    <div class="col-sm-8 nopadding">
                                                                                        <div class="field-wrapper">
                                                                                            <input type="text" name="emailotp" id="myText">
                                                                                            <div class="field-placeholder"><span>Enter Your Otp</span></div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-sm-4 nopadding">
                                                                                        <a href="{{route('pgemailverify',$list->id)}}" class="btn btn-brand btn-elevate btn-sm" style="margin-top: 7px; border-radius: 5px"> Click To Verify</a>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="alert-text" style="font-size:15px; margin-right: 255px" id="some_div"></div> 
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-secondary btn-sm" style="border-radius: 5px" data-dismiss="modal">Close</button>
                                                                                <button type="submit" class="btn btn-info btn-sm" style="border-radius: 5px">Submit</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </td>
                                                    <td class="doc_no_col">{{$list->contactno}}
                                                        @if($list->contactverify == '1')
                                                            <a class="kt-nav__link" href="#"><i class="kt-nav__link-icon flaticon2-check-mark"></i></a>
                                                        @else
                                                            <a class="kt-nav__link" href="#" data-toggle="modal" data-target=""><i class="kt-nav__link-icon flaticon2-delete"></i></a>                                                            
                                                        @endif
                                                    </td>
                                                    <td class="action_col">
                                                        <span style="overflow: visible; position: relative; width: 80px;">
                                                            <div class="dropdown">
                                                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md"><i class="flaticon-more-1"></i></a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <ul class="kt-nav">
                                                                        <li class="kt-nav__item">
                                                                            <a class="kt-nav__link" href="#" data-toggle="modal" data-target="#kt_modal_updateowner-{{$list->id}}"><i class="kt-nav__link-icon flaticon2-edit"></i><span class="kt-nav__link-text">Edit</span></a>
                                                                        </li>                                                                                                                                                                                                   
                                                                        <li class="kt-nav__item">
                                                                            <a class="kt-nav__link" href="{{route('remove_owner',['id'=>$list->id])}}"><i class="kt-nav__link-icon flaticon2-rubbish-bin-delete-button"></i><span class="kt-nav__link-text">Delete</span></a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </span>
                                                    </td>
                                                </tr>
                                                {{-- update Owner --}}
                                                <div class="modal fade" id="kt_modal_updateowner-{{$list->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLongTitle"> Owner Details</h5>
                                                            </button>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            </div>            
                                                            <form method="get" action="{{route('ownerupdate_store',['id'=>$view->user_id])}}">
                                                                {{csrf_field()}}
                                                                <input type="hidden" value="{{$list->id}}" name="id">
                                                                <div class="modal-body">
                                                                    {{-- <div class="kt-portlet__body"> --}}
                                                                        <div class="row">
                                                                            <div class="col-sm-4">
                                                                                <label>Owner Name</label>
                                                                                <input type="text" name="owner" value="{{$list->owner}}" class="form-control"> 
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <label>Email</label>
                                                                                <input type="email" name="email" value="{{$list->email}}" class="form-control" > 
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <label>Contact No</label>
                                                                                <input type="text" name="contactno" value="{{$list->contactno}}" class="form-control">
                                                                            </div>
                                                                        </div>
                                                                    {{-- </div> --}}
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary" style="border-radius: 5px" data-dismiss="modal">Close</button>
                                                                    <button type="submit" class="btn btn-brand" style="border-radius: 5px;">Submit</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div> 
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {{-- Managment User --}}
                            <div class="tab-pane" id="kt_tabs_7_3" role="tabpanel">
                                <button class="btn btn-bold btn-label-brand btn-sm" style="float: right" data-toggle="modal" data-target="#kt_modal_mgtuser" title="Add Managment User"><span class="flaticon2-plus"> </span></button>
                                <div class="kt-portlet__body">
                                    <table class="table table-striped- table-hover table-checkable" id="kt_table_1" style="background-color: white;">
                                        <thead>
                                            <tr style="text-align: center">
                                                <th id="record_col_head" style="width:14.2%"><label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand"> <input type="checkbox" id="selectAll"> <span></span></label></th>
                                                <th id="type_col_head" style="width:14.2%"> Managment Name </th>
                                                <th id="name_col_head" style="width:14.2%"> Email </th>
                                                <th id="doc_no_col_head" style="width:14.2%"> Contact No </th>
                                                <th id="doc_no_col_head" style="width:14.2%"> Access Control </th>
                                                <th id="doc_no_col_head" style="width:14.2%"> Enable Feature </th>
                                                <th id="action_col_head" style="width:14.2%"> Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($managment as $list)
                                            <tr style="text-align: center">
                                                <td class="record_col"><label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand"> <input type="checkbox" class="case3"> <span></span></label></td>
                                                <td class="type_col">{{$list->name}}</td>
                                                <td class="name_col">{{$list->email_verification}}
                                                    @if($list->verify == '1')
                                                        <a class="kt-nav__link" href="#"><i class="kt-nav__link-icon flaticon2-check-mark"></i></a>
                                                    @else
                                                        <a class="kt-nav__link" href="#" data-toggle="modal" data-target="#kt_modal_mv-{{$list->id}}"><i class="kt-nav__link-icon flaticon2-delete"></i></a>
                                                        {{-- mgtuser email otp --}}
                                                        <div class="modal fade" id="kt_modal_mv-{{$list->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title"> Enter Your Email Otp</h5>
                                                                    </button>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    </div>
                                                                    <form method="get" action="{{route('pgmgtotpverify',$list->id)}}">
                                                                        {{csrf_field()}}
                                                                        <input type="hidden" name="pg_id" value="{{$list->pg_id}}">
                                                                        <div class="modal-body">
                                                                            <div class="row">
                                                                                <div class="col-sm-8 nopadding">
                                                                                    <div class="field-wrapper">
                                                                                        <input type="text" name="emailotp" id="mgtuser1">
                                                                                        <div class="field-placeholder"><span>Enter Your Otp</span></div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-4 nopadding">
                                                                                    <a href="{{route('pgmgtverify',$list->id)}}" class="btn btn-brand btn-elevate btn-sm" style="margin-top: 7px; border-radius: 5px"> Click To Verify</a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="alert-text" style="font-size:15px; margin-right: 255px" id="mgtuser2"></div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary btn-sm" style="border-radius: 5px" data-dismiss="modal">Close</button>
                                                                            <button type="submit" class="btn btn-info btn-sm" style="border-radius: 5px">Submit</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                </td>
                                                <td class="doc_no_col">{{$list->mobile_verification}}
                                                    <a class="kt-nav__link" href="#" data-toggle="modal" data-target="#kt_modal_motp"><i class="kt-nav__link-icon flaticon2-delete"></i></a>
                                                </td>                                                
                                                <td>
                                                    @if($list->access_control == "read")
                                                        <label class="kt-checkbox kt-checkbox--bold kt-checkbox--primary"> <input type="checkbox" checked> Read <span></span></label>
                                                    @elseif($list->access_control == "write")
                                                        <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success"> <input type="checkbox" checked> Write <span></span></label>
                                                    @endif
                                                </td>                                                
                                                <td>
                                                    <label class="kt-checkbox kt-checkbox--bold kt-checkbox--info"> <input type="checkbox" checked> {{$list->enable_feature}} <span></span></label>
                                                </td>
                                                <td class="action_col">
                                                    <span style="overflow: visible; position: relative; width: 80px;">
                                                        <div class="dropdown">
                                                            <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md"><i class="flaticon-more-1"></i></a>
                                                            <div class="dropdown-menu dropdown-menu-right">
                                                                <ul class="kt-nav">
                                                                    <li class="kt-nav__item">
                                                                        <a class="kt-nav__link" href="#" data-toggle="modal" data-target="#kt_modal_updatemgtuser-{{$list->id}}"><i class="kt-nav__link-icon flaticon2-edit"></i><span class="kt-nav__link-text">Edit</span></a>
                                                                    </li>
                                                                    <li class="kt-nav__item">
                                                                        <a class="kt-nav__link" href="{{route('remove_pgmgtuser',$list->id)}}"><i class="kt-nav__link-icon flaticon2-rubbish-bin-delete-button"></i><span class="kt-nav__link-text">Delete</span></a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </span>
                                                </td>
                                            </tr>
                                            {{-- mgtuser Update --}}
                                            <div class="modal fade" id="kt_modal_updatemgtuser-{{$list->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLongTitle">Update Managment User</h5>
                                                        </button>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        </div>
                                                        <form method="get" action="{{route('pgmgtupdate_store',$view->user_id)}}">
                                                            {{csrf_field()}}
                                                            <input type="hidden" value="{{$list->id}}" name="id">
                                                            <div class="modal-body">
                                                                <div class="kt-portlet__body">
                                                                    <div class="form-group row">
                                                                        <div class="col-lg-4">
                                                                            <label>Managment Username</label>
                                                                            <input type="text" name="name" value="{{$list->name}}" style="width:250px" class="form-control">
                                                                        </div>
                                                                        <div class="col-lg-4">
                                                                            <label>Email</label>
                                                                            <input type="email" name="email_verification" value="{{$list->email_verification}}" style="width:250px" class="form-control">
                                                                        </div>
                                                                        <div class="col-lg-4">
                                                                            <label>Contact No</label>
                                                                            <input type="number" name="mobile_verification" value="{{$list->mobile_verification}}" style="width:250px" class="form-control">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <div class="col-lg-6">
                                                                            <label>Access Control</label>
                                                                            <div></div>
                                                                            <label class="kt-radio kt-radio--bold kt-radio--info">
                                                                                <input type="radio" name="access_control" value="read"> Read Access
                                                                                <span></span>
                                                                            </label> 
                                                                            <label class="kt-radio kt-radio--bold kt-radio--info" style="margin-left: 15px;">
                                                                                <input type="radio" name="access_control" value="write"> Write Access
                                                                                <span></span>
                                                                            </label>
                                                                        </div>
                                                                        <div class="col-lg-6">
                                                                            <label>Enable Feature</label>
                                                                            <div></div>
                                                                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--info">
                                                                                <input type="checkbox" name="enable_feature[]" value="payment"> Payment
                                                                                <span></span>
                                                                            </label> 
                                                                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--info" style="margin-left: 15px;">
                                                                                <input type="checkbox" name="enable_feature[]" value="complains"> Complains
                                                                                <span></span>
                                                                            </label>
                                                                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--info" style="margin-left: 15px;">
                                                                                <input type="checkbox" name="enable_feature[]" value="userprofile"> User Profile
                                                                                <span></span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary btn-sm" style="border-radius: 5px" data-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-brand btn-sm" style="border-radius: 5px">Submit</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('admin.pages.pg.popup')
@section('scripts')
<script type="text/javascript">
    $('#myTab a').click(function(e) {
        e.preventDefault();
        $(this).tab('show');
    });
    // store the currently selected tab in the hash value
    $("ul.nav-pills > li > a").on("shown.bs.tab", function(e) {
        var id = $(e.target).attr("href").substr(1);
        window.location.hash = id;
    });
    // on load of the page: switch to the currently selected tab
    var hash = window.location.hash;
    $('#myTab a[href="' + hash + '"]').tab('show');
</script>
@include('admin.pages.pg.js')
@endsection
@stop
