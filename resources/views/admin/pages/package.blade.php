@extends('admin.layouts.app')
@section('title')
    <title>PACKAGE DETAILS | PG-MANAGMENT</title>
@endsection
@section('styles')
@endsection
@section('content')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row" style="margin-top: 20px;">
        <div class="col-md-5">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Add Package Details 
                        </h3>
                    </div>
                </div>
                <form class="kt-form" method="POST" action="{{route('package_store')}}">
                    {{csrf_field()}}
                    <div class="kt-portlet__body">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" placeholder="Enter Name">
                            <span class="error" style="color:red;">{{ $errors->first('name') }}</span>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Amount</label>
                            <input type="number" name="amount" class="form-control" placeholder="Enter Amount">
                            <span class="error" style="color:red;">{{ $errors->first('amount') }}</span>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" class="btn btn-brand btn-sm" style="border-radius: 5px">Submit</button>
                            <button type="reset" class="btn btn-secondary btn-sm" style="border-radius: 5px">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-7">
            <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <span style="float: left; margin-top: 15px; margin-bottom: 10px;">
                    <a href="#" class="btn btn-bold btn-label-brand btn-sm" title="column visibility"><span class="flaticon-visible"> </span></a>                    
                </span>
                <span class="dropdown dropdown-inline" style="float:right; margin-top: 15px; margin-right:10px; margin-bottom: 10px;">
                    <button class="btn btn-bold btn-label-brand btn-sm" data-toggle="dropdown" title="Import"><span class="flaticon-upload"></span></button>    
                    <span class="dropdown-menu dropdown-menu-right"style="">
                        <a class="dropdown-item" href="#"><i class="flaticon-upload-1"></i> Import</a>
                        <a class="dropdown-item" href="#"><i class="flaticon2-download-2"></i> Export</a>
                    </span>
                </span>
                <span class="dropdown dropdown-inline" style="float:right; margin-top: 15px; margin-right: 10px; margin-bottom: 10px;">
                    <button type="button" class="btn btn-bold btn-label-brand btn-sm" data-toggle="dropdown" title="Export"><span class="flaticon-download-1"></span></button>    
                    <span class="dropdown-menu dropdown-menu-right">
                        <ul class="kt-nav">
                            <li class="kt-nav__item">
                                <a href="#" class="kt-nav__link" onclick="selectElementContents( document.getElementById('local_record_selection') );">
                                    <i class="kt-nav__link-icon flaticon2-copy"></i>
                                    <span class="kt-nav__link-text">Copy</span>
                                </a>
                            </li>
                            <li class="kt-nav__item">
                                <a href="#" class="kt-nav__link" onclick="exportTableToExcel('local_record_selection')">
                                    <i class="kt-nav__link-icon flaticon2-sheet"></i>
                                    <span class="kt-nav__link-text">Excel</span>
                                </a>
                            </li>
                            <li class="kt-nav__item">
                                <a href="#" class="kt-nav__link export">
                                    <i class="kt-nav__link-icon flaticon2-open-text-book"></i>
                                    <span class="kt-nav__link-text">CSV</span>
                                </a>
                            </li>
                            <li class="kt-nav__item">
                                <a href="#" class="kt-nav__link" id="btnExport" onclick="Export()">
                                    <i class="kt-nav__link-icon flaticon-file-2"></i>
                                    <span class="kt-nav__link-text">PDF</span>
                                </a>
                            </li>
                            <li class="kt-nav__item">
                                <a href="#" class="kt-nav__link" id="print">
                                    <i class="kt-nav__link-icon flaticon2-print"></i>
                                    <span class="kt-nav__link-text">Print</span>
                                </a>
                            </li>
                        </ul>
                    </span>
                </span>
                <div class="kt-portlet__body kt-portlet__body--fit">                    
                    <table class="table table-striped-  table-hover table-checkable" id="kt_table_1" style="background-color: white;">
                        <thead>
                            <tr style="text-align: center">
                                <th id="record_col_head" style="width:5%"><label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand"> <input type="checkbox"  id="selectall1"> <span></span></label></th>
                                <th id="type_col_head" style="width:23%">Package Name</th>                                
                                <th id="doc_no_col_head" style="width:23%">Amount</th>
                                <th id="action_col_head" style="width:23%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($view as $list)
                                <tr style="text-align: center">
                                    <td class="record_col"><label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand"> <input type="checkbox" class="case1"> <span></span></label></td>
                                    <td class="type_col">{{$list->name}}</td>
                                    <td class="name_col">{{$list->amount}}                                        
                                    </td>                                    
                                    <td class="action_col">
                                        <span style="overflow: visible; position: relative; width: 80px;">
                                            <div class="dropdown">
                                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md"><i class="flaticon-more-1"></i></a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <ul class="kt-nav">
                                                        <li class="kt-nav__item">
                                                            <a class="kt-nav__link" href="#" data-toggle="modal" data-target="#kt_modal_update-{{$list->id}}"><i class="kt-nav__link-icon flaticon2-edit"></i><span class="kt-nav__link-text">Edit</span></a>
                                                        </li>                                                                                                                                                                                                   
                                                        <li class="kt-nav__item">
                                                            <a class="kt-nav__link" href="{{route('remove_package',['id'=>$list->id])}}"><i class="kt-nav__link-icon flaticon2-rubbish-bin-delete-button"></i><span class="kt-nav__link-text">Delete</span></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </span>
                                    </td>
                                </tr>
                                {{-- update --}}
                                <div class="modal fade" id="kt_modal_update-{{$list->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLongTitle"> Package Details</h5>
                                            </button>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            </div>            
                                            <form method="get" action="{{route('package_edit_store',['id'=>$list->id])}}">
                                                {{csrf_field()}}
                                                <input type="hidden" value="{{$list->id}}" name="id">
                                                <div class="modal-body">
                                                    <div class="kt-portlet__body">
                                                        <div class="form-group row">
                                                            <div class="col-lg-6">
                                                                <label>Package Name</label>
                                                                <input type="text" name="name" value="{{$list->name}}" class="form-control" style="width:250px"> 
                                                            </div>                                                            
                                                            <div class="col-lg-6">
                                                                <label>Amount</label>
                                                                <input type="number" name="amount" value="{{$list->amount}}" class="form-control" style="width:250px">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>                
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" style="border-radius: 5px" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-brand" style="border-radius: 5px;">Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div> 
                            @endforeach
                        </tbody>
                    </table>                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        $('#local_record_selection').DataTable( {
            
        } );
    } );
</script>
{{-- copy to clipboard --}}
<script>
    function selectElementContents(el) {
        var body = document.body, range, sel;
            if (document.createRange && window.getSelection) {
                range = document.createRange();
                sel = window.getSelection();
                sel.removeAllRanges();
                try {
                    range.selectNodeContents(el);
                    sel.addRange(range);
                } catch (e) {
                    range.selectNode(el);
                    sel.addRange(range);
                }
            } else if (body.createTextRange) {
                range = body.createTextRange();
                range.moveToElementText(el);
                range.copy();
            }
    }
</script>
{{-- Excel Export--}}
<script>
    function exportTableToExcel(tableID, filename = ''){
        var downloadLink;
        var dataType = 'application/vnd.ms-excel';
        var tableSelect = document.getElementById(tableID);
        var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
        // Specify file name
        filename = filename?filename+'.xls':'librarysheet.xls';
        // Create download link element
        downloadLink = document.createElement("a");
        document.body.appendChild(downloadLink);
        if(navigator.msSaveOrOpenBlob){
            var blob = new Blob(['\ufeff', tableHTML], {
                type: dataType
            });
            navigator.msSaveOrOpenBlob( blob, filename);
        }else{
            // Create a link to the file
            downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
        
            // Setting the file name
            downloadLink.download = filename;
            //triggering the function
            downloadLink.click();
        }
    }
</script>
{{-- CSV Export --}}
<script>
    $(document).ready(function() {

    function exportTableToCSV($table, filename) {
        var $rows = $table.find('tr:has(td)'),

        // Temporary delimiter characters unlikely to be typed by keyboard
        // This is to avoid accidentally splitting the actual contents
        tmpColDelim = String.fromCharCode(11), // vertical tab character
        tmpRowDelim = String.fromCharCode(0), // null character

        // actual delimiter characters for CSV format
        colDelim = '","',
        rowDelim = '"\r\n"',

        // Grab text from table into CSV formatted string
        csv = '"' + $rows.map(function(i, row) {
            var $row = $(row),
                $cols = $row.find('td');

        return $cols.map(function(j, col) {
            var $col = $(col),
            text = $col.text();

            return text.replace(/"/g, '""'); // escape double quotes

        }).get().join(tmpColDelim);

        }).get().join(tmpRowDelim)
            .split(tmpRowDelim).join(rowDelim)
            .split(tmpColDelim).join(colDelim) + '"';

        // Deliberate 'false', see comment below
        if (false && window.navigator.msSaveBlob) {

            var blob = new Blob([decodeURIComponent(csv)], {
            type: 'text/csv;charset=utf8'
        });

        // Crashes in IE 10, IE 11 and Microsoft Edge
        // See MS Edge Issue #10396033
        // Hence, the deliberate 'false'
        // This is here just for completeness
        // Remove the 'false' at your own risk
        window.navigator.msSaveBlob(blob, filename);

    } else if (window.Blob && window.URL) {
        // HTML5 Blob        
        var blob = new Blob([csv], {
            type: 'text/csv;charset=utf-8'
        });
        var csvUrl = URL.createObjectURL(blob);

        $(this)
            .attr({
            'download': filename,
            'href': csvUrl
         });
    } else {
        // Data URI
        var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

        $(this)
            .attr({
            'download': filename,
            'href': csvData,
            'target': '_blank'
            });
        }
    }

    // This must be a hyperlink
    $(".export").on('click', function(event) {
    // CSV
        var args = [$('#local_record_selection>table'), 'library.csv'];

        exportTableToCSV.apply(this, args);

        // If CSV, don't do event.preventDefault() or return false
        // We actually need this to be a typical hyperlink
        });
    });
</script>
{{-- PDF Export --}}
<script>
    function Export() {
        html2canvas(document.getElementById('local_record_selection'), {
            onrendered: function (canvas) {
                var data = canvas.toDataURL();
                var docDefinition = {
                    content: [{
                        image: data,
                        width: 500
                    }]
                };
                pdfMake.createPdf(docDefinition).download("Library.pdf");
            }
        });
    }
</script>
{{-- Print --}}
<script>
    function printData()
    {
    var divToPrint=document.getElementById("local_record_selection");
    newWin= window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    newWin.close();
    }

    $('#print').on('click',function(){
    printData();
    })
</script>
<script src="{{asset('admin/assets/js/scripts.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/assets/js/pages/crud/metronic-datatable/advanced/package.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
@endsection
