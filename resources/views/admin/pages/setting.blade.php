@extends('admin.layouts.app')
@section('title')
    <title> SETTING | PG-MANAGMENT</title>
@endsection
@section('styles')
<style>
    .ui-select{width: 100%}
        /* This is to remove the arrow of select element in IE */
        select::-ms-expand {	display: none !important; }
        select{
            -webkit-appearance: none !important;
            appearance: none !important;
        }
    .dropdown {
        position: relative;
        display: inline-block;
    }
    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f9f9f9;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        padding: 12px 16px;
        z-index: 1;
    }
    .dropdown:hover .dropdown-content {
        display: block;
    }
	.btnn{
        background-color: #22b9ff;
        color:rgb(245, 243, 243);
        font-size: 12px;
        outline: none;
        margin-bottom: 5px;
    }
</style>
<link href="{{asset('admin/assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">   
    <div class="row" style="margin-top: 20px">
        <div class="col-md-6">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Add Pincode
                        </h3>
                    </div>
                </div>                
                <form class="kt-form" method="get" action="{{route('pincode.store')}}">
                    {{csrf_field()}}
                    <div class="kt-portlet__body">
                        <div class="form-group">
                            <label>Enter Pincode</label>
                            <input type="text" class="form-control" name="pincode" aria-describedby="emailHelp" > 
                        </div> 
                    </div>
                    <div class="kt-portlet__foot" style="float: right;">
                        <div class="kt-form__actions">
                            <button type="submit" class="btn btn-brand btn-sm" style="border-radius: 5px">Submit</button>
                            <button type="reset" class="btn btn-secondary btn-sm" style="border-radius: 5px">Cancel</button>
                        </div>
                    </div>
                </form>
            </div> 
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Add City
                        </h3>
                    </div>
                </div>                
                <form class="kt-form" method="post" action="{{route('city.store')}}">
                    {{csrf_field()}}
                    <div class="kt-portlet__body">                                                
                        <div class="form-group">
                            <label>Select Pincode</label>
                            <div></div>
                            <select class="custom-select form-control" name="pid_id">
                                @foreach($pincode as $list)
                                    <option value="{{$list->id}}">{{$list->pincode}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Enter City</label>
                            <input type="text" name="city" class="form-control" aria-describedby="emailHelp"> 
                        </div>                       
                    </div>
                    <div class="kt-portlet__foot" style="float: right">
                        <div class="kt-form__actions">
                            <button type="submit" class="btn btn-brand btn-sm" style="border-radius: 5px">Submit</button>
                            <button type="reset" class="btn btn-secondary btn-sm" style="border-radius: 5px">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>            
        </div>
        <div class="col-md-6">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Add State
                        </h3>
                    </div>
                </div>                
                <form class="kt-form" method="post" action="{{route('state.store')}}">
                    {{csrf_field()}}
                    <div class="kt-portlet__body">                        
                        <div class="form-group">
                            <label>Select Pincode</label>
                            <div></div>
                            <select class="custom-select form-control" name="pid_id">
                                @foreach($pincode as $list)
                                    <option value="{{$list->id}}">{{$list->pincode}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Enter State</label>
                            <input type="text" class="form-control" name="state" aria-describedby="emailHelp"> 
                        </div>                       
                    </div>
                    <div class="kt-portlet__foot" style="float: right">
                        <div class="kt-form__actions">
                            <button type="submit" class="btn btn-brand btn-sm" style="border-radius: 5px">Submit</button>
                            <button type="reset" class="btn btn-secondary btn-sm" style="border-radius: 5px">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="kt-portlet">
                <span style="float: left;">
                    <div class="col-sm-1 dropdown">
                        <span class="btnn dropdown btn btn-bold btn-label-brand btn-sm" data-toggle="dropdown" title="column Visibility"><span class="flaticon-visible"> </span></span>
                        <div id="checkbox_div" class="dropdown-content">
                            <div class="form-group">
                                <div class="kt-checkbox-list">
                                    <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
                                        <input type="checkbox" value="hide" id="pincode_col" onchange="hide_show_table(this.id);">Pincode<br>
                                        <span></span>
                                    </label>
                                </div>
                                <div class="kt-checkbox-list">
                                    <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
                                        <input type="checkbox" value="hide" id="state_col" onchange="hide_show_table(this.id);">State<br>
                                        <span></span>
                                    </label>
                                </div>
                                <div class="kt-checkbox-list">
                                    <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
                                        <input type="checkbox" value="hide" id="city_col" onchange="hide_show_table(this.id);">City<br>
                                        <span></span>
                                    </label>
                                </div>                    
                            </div>  
                        </div>
                    </div>
                </span>
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Pincode-State-City
                        </h3>
                    </div>
                </div>  
                <table class="table table-striped- table-hover table-checkable" id="kt_table_1" style="background-color: white;">
                    <thead>                        
                        <tr style="text-align: center">
                            <th class="record_col" style="width:5%; float: right">
                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand"> 
                                    <input type="checkbox" id="selectAll"> <span></span>
                                </label>
                            </th>
                            <th id="pincode_col_head" style="width:25%;">Pincode</th>
                            <th id="state_col_head" style="width:25%;">State</th>
                            <th id="city_col_head" style="width:25%;">City</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>                            
                        @foreach($pincode as $list)                                                      
                        <tr style="text-align: center">                            
                            <td class="record_col" style="width: 5%; float:right">
                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand"> 
                                    <input type="checkbox"> <span></span>
                                </label>
                            </td>
                            <td class="pincode_col" style="width:30%;">{{$list->pincode}}</td>
                            <td class="state_col" style="width:30%;">{{optional($list->states)->state ?? 'No state'}}</td>
                            <td class="city_col" style="width:30%;">{{optional($list->cities)->city ?? 'No city'}}</td>
                            <td>
                                <a href="{{route('remove_pincode',$list->id)}}" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Delete"><i class="flaticon-delete"></i></a>
                            </td>                                
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<!-- column visibility -->
<script>
    function hide_show_table(col_name){
        var checkbox_val=document.getElementById(col_name).value;
        if(checkbox_val=="hide"){
            var all_col=document.getElementsByClassName(col_name);
            for(var i=0;i<all_col.length;i++){
                all_col[i].style.display="none";
            }
            document.getElementById(col_name+"_head").style.display="none";
            document.getElementById(col_name).value="show";
        }
        else{
            var all_col=document.getElementsByClassName(col_name);
            for(var i=0;i<all_col.length;i++){
                all_col[i].style.display="table-cell";
            }
            document.getElementById(col_name+"_head").style.display="table-cell";
            document.getElementById(col_name).value="hide";
        }
    }
</script>
<script>
    $(function () {
        $("#chkPassport").click(function () {
            if ($(this).is(":checked")) {
                $("#dvPassport").show();
                $("#AddPassport").hide();
            } else {
                $("#dvPassport").hide();
                $("#AddPassport").show();
            }
        });
    });
</script>
{{-- Doc allcheck --}}
<script>
    $('#selectAll').click(function (e) {
        $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);
    });
</script>
<script src="{{asset('admin/assets/js/scripts.bundle.js')}}" type="text/javascript"></script>
@endsection
