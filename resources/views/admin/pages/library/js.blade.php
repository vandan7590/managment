<script src="{{asset('admin/assets/js/pages/crud/forms/widgets/select2.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/assets/js/scripts.bundle.js')}}" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="{{asset('admin/assets/js/pages/custom/user/profile.js')}}" type="text/javascript"></script>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
{{-- Doc allcheck --}}
<script>
    $('#selectAll').click(function (e) {
        $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);
    });
</script>
{{-- Doc allcheck --}}
<script>
    $('#selectAll1').click(function (e) {
        $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);
    });
</script>
{{-- Doc allcheck --}}
<script>
    $('#selectAll2').click(function (e) {
        $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);
    });
</script>
{{-- textbox --}}
<script>
    $(function () {
        $(".field-wrapper .field-placeholder").on("click", function () {
        $(this).closest(".field-wrapper").find("input").focus();
    });
    $(".field-wrapper input").on("keyup", function () {
        var value = $.trim($(this).val());
        if (value) {
            $(this).closest(".field-wrapper").addClass("hasValue");
        } else {
            $(this).closest(".field-wrapper").removeClass("hasValue");
        }
        });
    });
</script>
{{-- motp --}}
<script>
    var timeLeft1 = 120;
    var elem1 = document.getElementById('some_div1');
    var timerId = setInterval(countdown, 1000);

    function countdown() {
        if (timeLeft == -1) {
            clearTimeout(timerId);
            doSomething();
        } else {
            elem.innerHTML = timeLeft + ' seconds remaining';
            timeLeft--;
        }
    }
    function doSomething() {
        document.getElementById("myText").disabled = true;
    }
</script>
{{-- country --}}
<script type="text/javascript">
    $('.country').change(function(){
    var countryID = $(this).val();
    if(countryID){
        $.ajax({
           type:"GET",
           url:"{{url('get-state-list')}}?country_id="+countryID,
           success:function(res){
            if(res){
                $("#state").empty();
                // $("#state").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#state").append('<option value="'+key+'">'+value+'</option>');
                });

            }else{
               $("#state").empty();
            }
           }
        });
    }else{
        $("#state").empty();
        $("#city").empty();
    }
   });
    $('.country').on('change',function(){
    var stateID = $(this).val();
    if(stateID){
        $.ajax({
           type:"GET",
           url:"{{url('get-city-list')}}?country_id="+stateID,
           success:function(res){
            if(res){
                $("#city").empty();
                $.each(res,function(key,value){
                    $("#city").append('<option value="'+key+'">'+value+'</option>');
                });

            }else{
               $("#city").empty();
            }
           }
        });
    }else{
        $("#city").empty();
    }
   });
</script>
{{-- Multiple Mail --}}
<script type="text/javascript">
    $(document).ready(function() {
        var max_fields = 10; //Maximum allowed input fields
        var wrapper    = $(".wrapper1"); //Input fields wrapper
        var add_button = $(".add_email"); //Add button class or ID
        var x = 1; //Initial input field is set to 1

    //When user click on add input button
    $(add_button).click(function(e){
            e.preventDefault();
            //Check maximum allowed input fields
            if(x < max_fields){
                x++; //input field increment
                //add input field
                $(wrapper).append('<div><input type="text" name="email[]" placeholder="Enter Email" class="input1" /><a href="javascript:void(0);" style="margin-left:10px;" class="remove_field1 btn btn-brand btn-sm"><i class="fa fa-minus"></i></a></div>');
            }
        });

        //when user click on remove button
        $(wrapper).on("click",".remove_field1", function(e){
            e.preventDefault();
    $(this).parent('div').remove(); //remove inout field
    x--; //inout field decrement
        })
    });
</script>
{{-- multiple landline no --}}
<script type="text/javascript">
    $(document).ready(function() {
        var max_fields = 10; //Maximum allowed input fields
        var wrapper    = $(".wrapper2"); //Input fields wrapper
        var add_button = $(".add_landline"); //Add button class or ID
        var x = 1; //Initial input field is set to 1

    //When user click on add input button
    $(add_button).click(function(e){
            e.preventDefault();
            //Check maximum allowed input fields
            if(x < max_fields){
                x++; //input field increment
                //add input field
                $(wrapper).append('<div><input type="text" name="landline[]" placeholder="Enter Landline No" class="input1" /><a href="javascript:void(0);" style="margin-left:10px;" class="remove_field2 btn btn-brand btn-sm"><i class="fa fa-minus"></i></a></div>');
            }
        });

        //when user click on remove button
        $(wrapper).on("click",".remove_field2", function(e){
            e.preventDefault();
    $(this).parent('div').remove(); //remove inout field
    x--; //inout field decrement
        })
    });
</script>
{{-- multiple contact no --}}
<script type="text/javascript">
    $(document).ready(function() {
        var max_fields = 10; //Maximum allowed input fields
        var wrapper    = $(".wrapper3"); //Input fields wrapper
        var add_button = $(".add_contact"); //Add button class or ID
        var x = 1; //Initial input field is set to 1

    //When user click on add input button
    $(add_button).click(function(e){
            e.preventDefault();
            //Check maximum allowed input fields
            if(x < max_fields){
                x++; //input field increment
                //add input field
                $(wrapper).append('<div><input type="text" name="contact_no[]" placeholder="Enter Contact No" class="input1" /><a href="javascript:void(0);" style="margin-left:10px;" class="remove_field2 btn btn-brand btn-sm"><i class="fa fa-minus"></i></a></div>');
            }
        });

        //when user click on remove button
        $(wrapper).on("click",".remove_field2", function(e){
            e.preventDefault();
    $(this).parent('div').remove(); //remove inout field
    x--; //inout field decrement
        })
    });
</script>
{{-- image upload --}}
<script>
    $('#blah').click(function() {
        $('#imgInp').click();
    })

    //preview the image
    function readURL(input) {
        if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp").change(function() {
        readURL(this);
    });
</script>
 {{-- Owner Multiple Mail --}}
<script type="text/javascript">
    $(document).ready(function() {
        var max_fields = 10; //Maximum allowed input fields
        var wrapper    = $(".wrapper12"); //Input fields wrapper
        var add_button = $(".add_owner"); //Add button class or ID
        var x = 1; //Initial input field is set to 1

    //When user click on add input button
    $(add_button).click(function(e){
            e.preventDefault();
            //Check maximum allowed input fields
            if(x < max_fields){
                x++; //input field increment
                //add input field
                $(wrapper).append('<div><input type="text" name="owner[]" placeholder="Enter Owner Name" class="input1" /><a href="javascript:void(0);" style="margin-left:10px;" class="remove_field12 btn btn-brand btn-sm"><i class="fa fa-minus"></i></a></div>');
            }
        });

        //when user click on remove button
        $(wrapper).on("click",".remove_field12", function(e){
            e.preventDefault();
    $(this).parent('div').remove(); //remove inout field
    x--; //inout field decrement
        })
    });
</script>
{{-- multiple Email --}}
<script type="text/javascript">
    $(document).ready(function() {
        var max_fields = 10; //Maximum allowed input fields
        var wrapper    = $(".wrapper13"); //Input fields wrapper
        var add_button = $(".add_email"); //Add button class or ID
        var x = 1; //Initial input field is set to 1

    //When user click on add input button
    $(add_button).click(function(e){
            e.preventDefault();
            //Check maximum allowed input fields
            if(x < max_fields){
                x++; //input field increment
                //add input field
                $(wrapper).append('<div><input type="text" name="email[]" placeholder="Enter Email Address" class="input1" /><a href="javascript:void(0);" style="margin-left:10px;" class="remove_field12 btn btn-brand btn-sm"><i class="fa fa-minus"></i></a></div>');
            }
        });

        //when user click on remove button
        $(wrapper).on("click",".remove_field12", function(e){
            e.preventDefault();
    $(this).parent('div').remove(); //remove inout field
    x--; //inout field decrement
        })
    });
</script>
{{-- multiple contact no --}}
<script type="text/javascript">
    $(document).ready(function() {
        var max_fields = 10; //Maximum allowed input fields
        var wrapper    = $(".wrapper14"); //Input fields wrapper
        var add_button = $(".add_contact"); //Add button class or ID
        var x = 1; //Initial input field is set to 1

    //When user click on add input button
    $(add_button).click(function(e){
            e.preventDefault();
            //Check maximum allowed input fields
            if(x < max_fields){
                x++; //input field increment
                //add input field
                $(wrapper).append('<div><input type="text" name="contactno[]" placeholder="Enter Contact No" class="input1" /><a href="javascript:void(0);" style="margin-left:10px;" class="remove_field12 btn btn-brand btn-sm"><i class="fa fa-minus"></i></a></div>');
            }
        });

        //when user click on remove button
        $(wrapper).on("click",".remove_field12", function(e){
            e.preventDefault();
    $(this).parent('div').remove(); //remove inout field
    x--; //inout field decrement
        })
    });
</script>
{{-- owner email verify otp --}}
<script>
    var timeLeft = 300;
    var elem = document.getElementById('some_div');
    var timerId = setInterval(countdown, 1000);
    function countdown() {
        if (timeLeft == -1) {
            clearTimeout(timerId);
            doSomething();
        } else {
            elem.innerHTML = timeLeft + ' seconds remaining';
            timeLeft--;
        }
    }
    function doSomething() {
        document.getElementById("myText").disabled = true;
    }
</script>
{{-- owner contact verify otp --}}
<script>
    var timeLeft1 = 300;
    var elem1 = document.getElementById('contact_div');
    var timerId1 = setInterval(countdown1, 1000);
    function countdown1() {
        if (timeLeft1 == -1) {
            clearTimeout(timerId1);
            doSomething();
        } else {
            elem1.innerHTML = timeLeft1 + ' seconds remaining';
            timeLeft1--;
        }
    }
    function doSomething() {
        document.getElementById("contact_timer").disabled = true;
    }
</script>
{{-- owner email verify otp --}}
<script>
    var timeLeft3 = 300;
    var elem3 = document.getElementById('mgtuser2');
    var timerId3 = setInterval(countdown3, 1000);
    function countdown3() {
        if (timeLeft3 == -1) {
            clearTimeout(timerId3);
            doSomething();
        } else {
            elem3.innerHTML = timeLeft3 + ' seconds remaining';
            timeLeft3--;
        }
    }
    function doSomething() {
        document.getElementById("mgtuser1").disabled = true;
    }
</script>
