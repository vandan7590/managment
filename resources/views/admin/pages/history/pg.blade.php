@extends('admin.layouts.app')
@section('title')
    <title>PG HISTORY DETAILS | PG-MANAGMENT</title>
@endsection
@section('styles')
@endsection
@section('content')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="kt-portlet__body">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#" data-target="#kt_tabs_1_1">Update Log</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#" data-target="#kt_tabs_1_2">Delete Log</a>
                    </li>
                </ul>
                <div class="tab-content">
                    {{-- update tab panel --}}
                    <div class="tab-pane active" id="kt_tabs_1_1" role="tabpanel">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#" data-target="#kt_tabs_2_1">PG Details Log</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#" data-target="#kt_tabs_2_2">PG Owner Log</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#" data-target="#kt_tabs_2_3">PG Managmentuser Log</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#" data-target="#kt_tabs_2_4">Library Details Log</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#" data-target="#kt_tabs_2_5">Library Owner Log</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#" data-target="#kt_tabs_2_6">Library Managmentuser Log</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="kt_tabs_2_1" role="tabpanel">
                                <div class="kt-portlet__body">
                                    <table id="example1" class="table" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Contact No</th>
                                                <th>Address</th>
                                                <th>Country</th>
                                                <th>City</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($update as $list)
                                            @if($list->status_approval == "1")
                                            <tr>
                                                <td>{{$list->name}}</td>
                                                <td>{{$list->email}}</td>
                                                <td>{{$list->contact_no}}</td>
                                                <td>{{$list->address}}</td>
                                                <td>{{$list->country}}</td>
                                                <td>{{$list->city}}</td>
                                                <td>
                                                    @if($list->status == "active")
                                                        <span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">Active</></span>
                                                    @elseif($list->status == "inactive")
                                                        <span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--success">InActive</></span>
                                                    @else
                                                        <span>-</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Permanent Remove"><i class="flaticon2-delete"></i></a>
                                                </td>
                                            </tr>
                                            @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="kt_tabs_2_2" role="tabpanel">
                                <table id="example2" class="table" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Owner Name</th>
                                            <th>Email</th>
                                            <th>Contact No</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($update as $list)
                                        @if($list->status_approval == "2")
                                        <tr>
                                            <td>{{$list->owner}}</td>
                                            <td>{{$list->owner_email}}</td>
                                            <td>{{$list->contactno}}</td>
                                            <td>
                                                <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Permanent Remove"><i class="flaticon2-delete"></i></a>
                                            </td>
                                        </tr>
                                        @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane" id="kt_tabs_2_3" role="tabpanel">
                                <table id="example3" class="table" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Contact No</th>
                                            <th>Access Control</th>
                                            <th>Enable Feature</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($update as $list)
                                        @if($list->status_approval == "3")
                                        <tr>
                                            <td>{{$list->mgt_name}}</td>
                                            <td>{{$list->email_verification}}</td>
                                            <td>{{$list->mobile_verification}}</td>
                                            <td>
                                                @if($list->access_control == "read")
                                                    <label class="kt-checkbox kt-checkbox--bold kt-checkbox--primary"> <input type="checkbox" checked> Read <span></span></label>
                                                @elseif($list->access_control == "write")
                                                    <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success"> <input type="checkbox" checked> Write <span></span></label>
                                                @else
                                                    <span>-</span>
                                                @endif
                                            </td>
                                            <td><label class="kt-checkbox kt-checkbox--bold kt-checkbox--info"> <input type="checkbox" checked> {{$list->enable_feature}} <span></span></label> </td>
                                            <td>
                                                <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Permanent Remove"><i class="flaticon2-delete"></i></a>
                                            </td>
                                        </tr>
                                        @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane" id="kt_tabs_2_4" role="tabpanel">
                                <div class="kt-portlet__body">
                                    <table id="example4" class="table" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Contact No</th>
                                                <th>Address</th>
                                                <th>Country</th>
                                                <th>City</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($libraryupdate as $list)
                                            @if($list->status_approval == "1")
                                            <tr>
                                                <td>{{$list->name}}</td>
                                                <td>{{$list->email}}</td>
                                                <td>{{$list->contact_no}}</td>
                                                <td>{{$list->address}}</td>
                                                <td>{{$list->country}}</td>
                                                <td>{{$list->city}}</td>
                                                <td>
                                                    @if($list->status == "active")
                                                        <span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">Active</></span>
                                                    @elseif($list->status == "inactive")
                                                        <span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--success">InActive</></span>
                                                    @else
                                                        <span>-</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Permanent Remove"><i class="flaticon2-delete"></i></a>
                                                </td>
                                            </tr>
                                            @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="kt_tabs_2_5" role="tabpanel">
                                <table id="example5" class="table" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Owner Name</th>
                                            <th>Email</th>
                                            <th>Contact No</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($libraryupdate as $list)
                                        @if($list->status_approval == "2")
                                        <tr>
                                            <td>{{$list->owner}}</td>
                                            <td>{{$list->owner_email}}</td>
                                            <td>{{$list->contactno}}</td>
                                            <td>
                                                <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Permanent Remove"><i class="flaticon2-delete"></i></a>
                                            </td>
                                        </tr>
                                        @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane" id="kt_tabs_2_6" role="tabpanel">
                                <table id="example6" class="table" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Contact No</th>
                                            <th>Access Control</th>
                                            <th>Enable Feature</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($libraryupdate as $list)
                                        @if($list->status_approval == "3")
                                        <tr>
                                            <td>{{$list->mgt_name}}</td>
                                            <td>{{$list->email_verification}}</td>
                                            <td>{{$list->mobile_verification}}</td>
                                            <td>
                                                @if($list->access_control == "read")
                                                    <label class="kt-checkbox kt-checkbox--bold kt-checkbox--primary"> <input type="checkbox" checked> Read <span></span></label>
                                                @elseif($list->access_control == "write")
                                                    <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success"> <input type="checkbox" checked> Write <span></span></label>
                                                @else
                                                    <span>-</span>
                                                @endif
                                            </td>
                                            <td><label class="kt-checkbox kt-checkbox--bold kt-checkbox--info"> <input type="checkbox" checked> {{$list->enable_feature}} <span></span></label> </td>
                                            <td>
                                                <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Permanent Remove"><i class="flaticon2-delete"></i></a>
                                            </td>
                                        </tr>
                                        @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- delete tab panel --}}
                    <div class="tab-pane" id="kt_tabs_1_2" role="tabpanel">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#" data-target="#kt_tabs_3_1">PG Details Log</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#" data-target="#kt_tabs_3_2">PG Owner Log</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#" data-target="#kt_tabs_3_3">PG Managmentuser Log</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#" data-target="#kt_tabs_3_4">Library Details Log</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#" data-target="#kt_tabs_3_5">Library Owner Log</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#" data-target="#kt_tabs_3_6">Library Managmentuser Log</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="kt_tabs_3_1" role="tabpanel">
                                <div class="kt-portlet__body">
                                    <table id="example11" class="table" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Contact No</th>
                                                <th>Address</th>
                                                <th>Country</th>
                                                <th>City</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($pg as $list)
                                            <tr>
                                                <td>{{$list->name}}</td>
                                                <td>{{$list->email}}</td>
                                                <td>{{$list->contact_no}}</td>
                                                <td>{{$list->address}}</td>
                                                <td>{{$list->country}}</td>
                                                <td>{{$list->city}}</td>
                                                <td>
                                                    @if($list->status == "active")
                                                        <span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">Active</></span>
                                                    @elseif($list->status == "inactive")
                                                        <span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--success">InActive</></span>
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="{{route('pg_permanentdelete',$list->id)}}" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Permanent Remove"><i class="flaticon2-delete"></i></a>
                                                    <a href="{{route('pg_restore',$list->id)}}" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Restore"><i class="flaticon-refresh"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="kt_tabs_3_2" role="tabpanel">
                                <table id="example12" class="table" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Owner Name</th>
                                            <th>Email</th>
                                            <th>Contact No</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($owner as $list)
                                        <tr>
                                            <td>{{$list->owner}}</td>
                                            <td>{{$list->email}}</td>
                                            <td>{{$list->contactno}}</td>
                                            <td>
                                                <a href="{{route('pg_owner_permanentdelete',$list->id)}}" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Permanent Remove"><i class="flaticon2-delete"></i></a>
                                                <a href="{{route('pg_owner_restore',$list->id)}}" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Restore"><i class="flaticon-refresh"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane" id="kt_tabs_3_3" role="tabpanel">
                                <table id="example13" class="table" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Contact No</th>
                                            <th>Access Control</th>
                                            <th>Enable Feature</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($pgmanagment as $list)
                                        <tr>
                                            <td>{{$list->name}}</td>
                                            <td>{{$list->email_verification}}</td>
                                            <td>{{$list->mobile_verification}}</td>
                                            <td>
                                                @if($list->access_control == "read")
                                                    <label class="kt-checkbox kt-checkbox--bold kt-checkbox--primary"> <input type="checkbox" checked> Read <span></span></label>
                                                @elseif($list->access_control == "write")
                                                    <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success"> <input type="checkbox" checked> Write <span></span></label>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td><label class="kt-checkbox kt-checkbox--bold kt-checkbox--info"> <input type="checkbox" checked> {{$list->enable_feature}} <span></span></label> </td>
                                            <td>
                                                <a href="{{route('pg_mgtuser_permanentdelete',$list->id)}}" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Permanent Remove"><i class="flaticon2-delete"></i></a>
                                                <a href="{{route('pg_managment_restore',$list->id)}}" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Restore"><i class="flaticon-refresh"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane" id="kt_tabs_3_4" role="tabpanel">
                                <div class="kt-portlet__body">
                                    <table id="example14" class="table" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Contact No</th>
                                                <th>Address</th>
                                                <th>Country</th>
                                                <th>City</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($library as $list)
                                            <tr>
                                                <td>{{$list->name}}</td>
                                                <td>{{$list->email}}</td>
                                                <td>{{$list->contact_no}}</td>
                                                <td>{{$list->address}}</td>
                                                <td>{{$list->country}}</td>
                                                <td>{{$list->city}}</td>
                                                <td>
                                                    @if($list->status == "active")
                                                        <span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">Active</></span>
                                                    @elseif($list->status == "inactive")
                                                        <span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--success">InActive</></span>
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="{{route('library_permanentdelete',$list->id)}}" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Permanent Remove"><i class="flaticon2-delete"></i></a>
                                                    <a href="{{route('library_restore',$list->id)}}" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Restore"><i class="flaticon-refresh"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="kt_tabs_3_5" role="tabpanel">
                                <table id="example15" class="table" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Owner Name</th>
                                            <th>Email</th>
                                            <th>Contact No</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($libraryowner as $list)
                                        <tr>
                                            <td>{{$list->owner}}</td>
                                            <td>{{$list->email}}</td>
                                            <td>{{$list->contactno}}</td>
                                            <td>
                                                <a href="{{route('library_owner_permanentdelete',$list->id)}}" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Permanent Remove"><i class="flaticon2-delete"></i></a>
                                                <a href="{{route('library_owner_restore',$list->id)}}" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Restore"><i class="flaticon-refresh"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane" id="kt_tabs_3_6" role="tabpanel">
                                <table id="example16" class="table" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Contact No</th>
                                            <th>Access Control</th>
                                            <th>Enable Feature</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($librarymanagment as $list)
                                        <tr>
                                            <td>{{$list->mgt_name}}</td>
                                            <td>{{$list->email_verification}}</td>
                                            <td>{{$list->mobile_verification}}</td>
                                            <td>
                                                @if($list->access_control == "read")
                                                    <label class="kt-checkbox kt-checkbox--bold kt-checkbox--primary"> <input type="checkbox" checked> Read <span></span></label>
                                                @elseif($list->access_control == "write")
                                                    <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success"> <input type="checkbox" checked> Write <span></span></label>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td><label class="kt-checkbox kt-checkbox--bold kt-checkbox--info"> <input type="checkbox" checked> {{$list->enable_feature}} <span></span></label> </td>
                                            <td>
                                                <a href="{{route('library_mgtuser_permanentdelete',$list->id)}}" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Permanent Remove"><i class="flaticon2-delete"></i></a>
                                                <a href="{{route('library_mgtuser_restore',$list->id)}}" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Restore"><i class="flaticon-refresh"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>  
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        $('#example1').DataTable({
            "sDom": '<"top"i>rt<"bottom"flp><"clear">',
            searching:false,
            lengthChange:false
        });
    } );
</script>
<script>
    $(document).ready(function() {
        $('#example2').DataTable({
            "sDom": '<"top"i>rt<"bottom"flp><"clear">',
            searching:false,
            lengthChange:false
        });
    } );
</script>
<script>
    $(document).ready(function() {
        $('#example3').DataTable({
            "sDom": '<"top"i>rt<"bottom"flp><"clear">',
            searching:false,
            lengthChange:false
        });
    } );
</script>
<script>
    $(document).ready(function() {
        $('#example4').DataTable({
            "sDom": '<"top"i>rt<"bottom"flp><"clear">',
            searching:false,
            lengthChange:false
        });
    } );
</script>
<script>
    $(document).ready(function() {
        $('#example5').DataTable({
            "sDom": '<"top"i>rt<"bottom"flp><"clear">',
            searching:false,
            lengthChange:false
        });
    } );
</script>
<script>
    $(document).ready(function() {
        $('#example6').DataTable({
            "sDom": '<"top"i>rt<"bottom"flp><"clear">',
            searching:false,
            lengthChange:false
        });
    } );
</script>
<script>
    $(document).ready(function() {
        $('#example11').DataTable({
            "sDom": '<"top"i>rt<"bottom"flp><"clear">',
            searching:false,
            lengthChange:false
        });
    } );
</script>
<script>
    $(document).ready(function() {
        $('#example12').DataTable({
            "sDom": '<"top"i>rt<"bottom"flp><"clear">',
            searching:false,
            lengthChange:false
        });
    } );
</script>
<script>
    $(document).ready(function() {
        $('#example13').DataTable({
            "sDom": '<"top"i>rt<"bottom"flp><"clear">',
            searching:false,
            lengthChange:false
        });
    } );
</script>
<script>
    $(document).ready(function() {
        $('#example14').DataTable({
            "sDom": '<"top"i>rt<"bottom"flp><"clear">',
            searching:false,
            lengthChange:false
        });
    } );
</script>
<script>
    $(document).ready(function() {
        $('#example15').DataTable({
            "sDom": '<"top"i>rt<"bottom"flp><"clear">',
            searching:false,
            lengthChange:false
        });
    } );
</script>
<script>
    $(document).ready(function() {
        $('#example16').DataTable({
            "sDom": '<"top"i>rt<"bottom"flp><"clear">',
            searching:false,
            lengthChange:false
        });
    } );
</script>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
@endsection