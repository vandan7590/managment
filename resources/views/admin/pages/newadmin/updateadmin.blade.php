@extends('admin.layouts.app')
@section('title')
    <title>UPDATE NEW ADMIN DETAILS | PG-MANAGMENT</title>
@endsection
@section('content')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h4 style="margin-top:20px"><i class="fa fa-edit"> </i> Edit Admin Details</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet">
                <form class="kt-form kt-form--label-right" method="post" action="{{route('addadmin_editstore',['id'=>$edit->id])}}">
                    {{csrf_field()}}
                    <input type="hidden" name="unique_id">
                    <div class="kt-portlet__body">
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>Admin Name:</label>
                                <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text" id="basic-addon2"><i class="fa fa-user" aria-hidden="true"></i></span></div>
                                    <input type="text" class="form-control" value="{{$edit->name}}" name="name">
                                    <span class="error" style="color:red;">{{ $errors->first('name') }}</span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <label>Date Of Joining</label>
                                <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text" id="basic-addon2"><i class="fa fa-calendar" aria-hidden="true"></i></span></div>
                                    <input type="date" class="form-control" name="doj" value="{{ \Carbon\Carbon::createFromDate($edit->year,$edit->month,$edit->day)->format('Y-m-d')}}">
                                </div>
                                    <span class="error" style="color:red;">{{ $errors->first('doj') }}</span>
                            </div>                            
                        </div>                        
                        <div class="form-group row">
                            <div class="col-lg-6 wrapper1">
                                <label>Admin Email:</label>
                                <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text" id="basic-addon2"><i class="fa fa-envelope" aria-hidden="true"></i></span></div>
                                    <input type="email" class="form-control" name="email[]" value="{{$edit->email}}" required>
                                    <button class="add_fields1"><i class="fa fa-plus-circle"></i></button>
                                </div>
                                <span class="error" style="color:red;">{{ $errors->first('email') }}</span>
                            </div>
                            <div class="col-lg-6  wrapper">
                                <label class="">Admin Contact Number:</label>
                                <div class="input-group">                
                                    <div class="input-group-prepend"><span class="input-group-text" id="basic-addon2"><i class="fa fa-phone" aria-hidden="true"></i></span></div>
                                    <input type="tel" class="form-control" name="contact_no[]" value="{{$edit->contact_no}}" pattern="[1-9]{1}[0-9]{9}" title="Please Enter 10 Digit Mobile No" required>
                                    <button class="add_fields"><i class="fa fa-plus-circle"></i></button>                   
                                </div>
                                <span class="error" style="color:red;">{{ $errors->first('contact_no') }}</span>
                            </div>        
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label>Status:</label>
                                <div class="kt-radio-inline">
                                    <label class="kt-radio kt-radio--solid">
                                        <input type="radio" name="status" checked value="active"> Active
                                        <span></span>
                                    </label>
                                    <label class="kt-radio kt-radio--solid">
                                        <input type="radio" name="status" value="inactive"> Inactive
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>                    
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-6">
                                    <button type="submit" style="border-radius: 10px" class="btn btn-primary">Save</button>
                                    <a href="{{route('addadmin_view')}}" style="border-radius: 10px" class="btn btn-secondary">Cancel</a>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{asset('admin/assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<script type="text/javascript">
   $(document).ready(function() {
        var max_fields = 10; //Maximum allowed input fields 
        var wrapper    = $(".wrapper"); //Input fields wrapper
        var add_button = $(".add_fields"); //Add button class or ID
        var x = 1; //Initial input field is set to 1
 
    //When user click on add input button
    $(add_button).click(function(e){
            e.preventDefault();
    //Check maximum allowed input fields
            if(x < max_fields){ 
                x++; //input field increment
    //add input field
                $(wrapper).append('<br><div><input type="text" name="contact_no[]" placeholder="Enter Admin Contact number" class="input1"/> <a href="javascript:void(0);" class="remove_field btn btn-default"><i class="fa fa-minus"></i></a></div>');
            }
        });
 
        //when user click on remove button
        $(wrapper).on("click",".remove_field", function(e){ 
            e.preventDefault();
    $(this).parent('div').remove(); //remove inout field
    x--; //inout field decrement
        })
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        var max_fields = 10; //Maximum allowed input fields 
        var wrapper    = $(".wrapper1"); //Input fields wrapper
        var add_button = $(".add_fields1"); //Add button class or ID
        var x = 1; //Initial input field is set to 1
    
    //When user click on add input button
    $(add_button).click(function(e){
            e.preventDefault();
    //Check maximum allowed input fields
            if(x < max_fields){ 
                x++; //input field increment
    //add input field
                $(wrapper).append('<br><div><input type="text" name="email[]" placeholder="Enter Admin Email" class="input1"/> <a href="javascript:void(0);" class="remove_field1 btn btn-default"><i class="fa fa-minus"></i></a></div>');
            }
        });
    
        //when user click on remove button
        $(wrapper).on("click",".remove_field1", function(e){ 
            e.preventDefault();
    $(this).parent('div').remove(); //remove inout field
    x--; //inout field decrement
        })
    });
</script>
@endsection