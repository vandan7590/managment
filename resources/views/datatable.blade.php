@extends('admin.layouts.app')
@section('styles')
    <link href="{{asset('admin/DataTables/DataTables-1.10.20/css/jquery.dataTables.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/DataTables/Buttons-1.6.1/css/buttons.dataTables.min.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <table id="example" style="width:100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Contact No</th>
                <th>Address</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Tiger Nixon</td>
                <td>System Architect</td>
                <td>Edinburgh</td>
                <td>61</td>
                <td>2011/04/25</td>
                <td>$320,800</td>
            </tr>
        </tbody>
    </table>
</div>
@section('scripts')
<script>
    $(document).ready(function() {
        $('#example').DataTable( {
            "searching": false,
            dom: 'Bfrtip',
            columnDefs: [
                {
                    "targets": [0],
                    "visible":false,
                },
                {
                    "targets":[1],
                    "visible":false
                },
                {
                    "targets":[2],
                    "visible":false
                },
                {
                    "targets":[3],
                    "visible":false
                },
                {
                    "targets":[4],
                    "visible":false
                },
                {
                    "targets":[5],
                    "visible":false
                }
            ],
            buttons: [
                {
                    extend: 'colvis',
                    columns: ':not(.noVis)'
                }
            ]
        } );
    } );
</script>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="{{asset('admin/DataTables/DataTables-1.10.20/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/DataTables/Buttons-1.6.1/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('admin/DataTables/Buttons-1.6.1/js/buttons.colVis.min.js')}}"></script>
@endsection
@endsection