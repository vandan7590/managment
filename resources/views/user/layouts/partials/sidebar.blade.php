<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">
    <div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
        <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
            <ul class="kt-menu__nav ">
                <li class="kt-menu__item {{ setActive('pg/dashboard') }}"><a href="" class="kt-menu__link "><i class="kt-menu__link-icon flaticon2-protection"> </i> </a> <span class="kt-menu__link-text" style="margin-left: 15px;">Dashboard</span></li>
                <li class="kt-menu__item {{ setActive('pg/profile') }}"><a href="" class="kt-menu__link "><i class="kt-menu__link-icon flaticon2-avatar"></i></a> <span class="kt-menu__link-text" style="margin-left: 20px;">User Profile</span></li>
                <li class="kt-menu__item {{ setActive('pg/userverify') }}"><a href="" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-list"></i></a><span class="kt-menu__link-text" style="margin-left: 18px;">Approval</span></li>
            </ul>
        </div>
    </div>
</div>

