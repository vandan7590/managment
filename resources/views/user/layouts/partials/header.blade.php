@section('styles')
<style>
ul.nav a {
    cursor: pointer;
}
.active {
    color:skyblue;
    font-weight:bolder;
}
</style>
@endsection
<div id="kt_header" class="kt-header kt-grid kt-grid--ver  kt-header--fixed ">
    <div class="kt-header__brand kt-grid__item  " id="kt_header_brand">
        <div class="kt-header__brand-logo">
            <a href="">
                <img alt="Logo" src="{{asset('admin/assets/media/logos/logo-6.png')}}" />
            </a>
        </div>
    </div>
    <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
    <div class="kt-header-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_header_menu_wrapper">
        <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">
            
        </div>
    </div>

    <div class="kt-header__topbar">
        <div class="kt-header__topbar-item kt-header__topbar-item--search dropdown" id="kt_quick_search_toggle">
            <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                <span class="kt-header__topbar-icon"><i class="flaticon2-search-1"></i></span>
            </div>
            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-lg">
                <div class="kt-quick-search kt-quick-search--dropdown kt-quick-search--result-compact" id="kt_quick_search_dropdown">
                    <form method="get" class="kt-quick-search__form">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text"><i class="flaticon2-search-1"></i></span></div>
                            <input type="text" class="form-control kt-quick-search__input" placeholder="Search...">
                            <div class="input-group-append"><span class="input-group-text"><i class="la la-close kt-quick-search__close"></i></span></div>
                        </div>
                    </form>
                    <div class="kt-quick-search__wrapper kt-scroll" data-scroll="true" data-height="325" data-mobile-height="200">
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-header__topbar-item dropdown">
            <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                <span class="kt-header__topbar-icon kt-header__topbar-icon--success">
                    <i class="flaticon2-bell-alarm-symbol"></i>
                    {{-- <span class="kt-badge kt-badge--unified-brand kt-badge--bold" style="margin-bottom: 15px;"> {{\Auth::user()->count()}} </span> --}}
                </span>
                <span class="kt-hidden kt-badge kt-badge--danger"></span>
            </div>
            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">
                <form>                   
                    <div class="tab-content">
                        <div class="tab-pane active show" id="topbar_notifications_notifications" role="tabpanel">
                            <div class="kt-notification kt-margin-t-10 kt-margin-b-10 kt-scroll" data-scroll="true" data-height="300" data-mobile-height="200">
                                {{-- @foreach (Auth::user()->notifications as $notification)  
                                <span class="kt-notification__item">                                       
                                    <div class="kt-notification__item-details">
                                        <div class="kt-notification__item-title">
                                            <i class="flaticon2-box-1 kt-font-brand"></i>      
                                                <span style="color:navy; font-size:14px;"> {{ $notification->data['message'] }} </span>
                                        </div>        
                                    </div>
                                </span>
                                @endforeach  --}}
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="kt-header__topbar">
        <a href="{{route('logout')}}" class="btn btn-outline-info btn-elevate btn-circle btn-icon" style="margin-top: 20px;"><i class="fas fa-sign-out-alt"></i></a>
    </div>
</div>
@section('scripts')
<script>
    $(document).ready(function(){
        $('ul li a').click(function(){
            $('li a').removeClass("active");
            $(this).addClass("active");
        });
    });
</script>
@endsection