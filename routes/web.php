<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('logout',array('as'=>'logout','uses'=>'Session\SessionController@logout'));
Route::get('login',array('as'=>'pglogin','uses'=>'Session\SessionController@login'));
Route::post('login',array('as'=>'login_store','uses'=>'Session\SessionController@login_store'));

Route::get('signup',array('as'=>'signup','uses'=>'Session\SessionController@signup'));
Route::post('signup/store',array('as'=>'signup_store','uses'=>'Session\SessionController@signup_store'));

Route::get('otp','Session\SessionController@otp')->name('otp');
Route::post('otp/verify','Session\SessionController@verify_email_otp')->name('verify_email');
Route::get('otp/resend/{id}','Session\SessionController@resend_otp')->name('resend_otp');
Route::post('otp/contact/verify','Session\SessionController@verify_contact_otp')->name('verify_contact');
Route::get('otp/contact/resend/{id}','Session\SessionController@resend_contactotp')->name('resend_contactotp');
Route::get('confirmpassword','Session\SessionController@confirmpassword')->name('confirmpassword');
Route::post('confirmpassword/store','Session\SessionController@confirm_password_store')->name('confirmpassword_store');
Route::get('verify','Session\SessionController@verify')->name('verify');
Route::post('verify/pg','Session\SessionController@pg_verify')->name('pgverify');

Route::group(['middleware' => 'admin'], function()
{
    Route::get('dashboard',array('as'=>'dashboard','uses'=>'Pages\DashboardController@dashboard'));
    Route::get('Admin/display','PgController@json_view')->name('json_view');
    Route::get('Pg/Renew/Jsondata','Pages\ReviewController@renew_jsondata');
    Route::get('get-state-list','PgController@getStateList');
    Route::get('get-city-list','PgController@getCityList');
    Route::post('city/store','PgController@city_Store')->name('city.store');
    Route::get('downloadfile/{id}', 'PgController@downloadfile')->name('downloadfile');
    Route::get('librarydownloadfile/{id}', 'PgController@librarydownloadfile')->name('librarydownloadfile');
    
    //Admin Route
    Route::resource('admin/resource', 'pages\AdminController',['names'=>['index'=>'addadmin_view','create'=>'addadmin',
        'store'=>'addadmin_store']]);
    Route::resource('setting', 'pages\SettingController',['names'=>['index'=>'setting','create'=>'pincode.store',
        'store'=>'state.store','show'=>'remove_pincode']]);
    
    //Pg Routes
    Route::resource('pg/resource', 'pages\PgController',['names'=>['index'=>'pg_view','store'=>'pg_details_store',
        'edit'=>'pgupdate_store','show'=>'pg_destroy']]);
    Route::resource('pg/review', 'pages\ReviewController',['names'=>['show'=>'review','store'=>'review_store',
        'edit'=>'updatereview_store']]);
    Route::resource('pg/profile', 'pages\ProfileController',['names'=>['show'=>'profile','store'=>'image_update',
        'create'=>'remove_image','edit'=>'profile_status']]);
    Route::resource('pg/document', 'pages\DocumentController',['names'=>['store'=>'documentmodal_store',
        'show'=>'document_delete']]);
    Route::resource('pg/owner', 'pages\OwnerController',['names'=>['store'=>'owner_details_store',
        'edit'=>'ownerupdate_store','show'=>'remove_owner']]);
    Route::resource('pg/Verify', 'pages\PgverifyController',['names'=>['show'=>'pgemailverify',
        'edit'=>'pgotpverify']]);
    Route::resource('pg/mgt', 'pages\PgmgtController',['names'=>['create'=>'pgmanagment_store',
        'edit'=>'pgmgtupdate_store','show'=>'remove_pgmgtuser']]);
    Route::resource('pg/mgt/verify', 'pages\PgmgtverifyController',['names'=>['show'=>'pgmgtverify',
        'edit'=>'pgmgtotpverify']]);
    
    //Library Routes
    Route::resource('Library/View', 'pages\LibraryController',['names'=>['index'=>'library_view','store'=>'library_store',
        'edit'=>'libraryupdate_store','show'=>'library_destroy']]);        
    Route::resource('Library/review', 'pages\LibraryreviewController',['names'=>['show'=>'libraryreview','store'=>'libraryreview_store',
        'edit'=>'libraryupdatereview_store']]);
    Route::resource('Library/Profile','pages\LibraryprofileController',['names'=>['show'=>'libraryprofile',
        'store'=>'libraryimageupload','create'=>'libraryimage_remove','edit'=>'libraryprofile_status']]);
    Route::resource('Library/Document','pages\LibrarydocumentController',['names'=>['store'=>'document_store',
        'show'=>'librarydocument_remove']]);
    Route::resource('Library/Owner', 'pages\LibraryownerController',['names'=>['store'=>'libraryowner_store',
        'edit'=>'libraryownerupdate_store','show'=>'remove_libraryowner']]);
    Route::resource('Library/Verify', 'pages\LibraryverifyController',['names'=>['show'=>'libraryemailverify',
        'edit'=>'libraryotpverify']]);
    Route::resource('Library/mgt', 'pages\LibrarymgtController',['names'=>['create'=>'librarymanagment_store',
        'edit'=>'librarymgtupdate_store','show'=>'remove_mgtuser']]);
    Route::resource('Library/mgt/verify', 'pages\LibrarymgtverifyController',['names'=>['show'=>'librarymgtverify',
        'edit'=>'librarymgtotpverify']]);
   
    //Package Route
    Route::resource('package', 'pages\PackageController',['names'=>['index'=>'package','store'=>'package_store',
        'edit'=>'package_edit_store','show'=>'remove_package']]);
   
    //Import Export
    Route::resource('importexport', 'pages\ImportexportController',['names'=>['create'=>'pgexport','store'=>'pgimport']]);
    Route::get('Demo/Export', 'Pages\ImportExportController@demo_export')->name('demoexport');
    // Route::post('Pg/Import', 'Pages\ImportExportController@import')->name('import');
    Route::post('Pg/Import/Csv','Pages\ImportExportController@import_csv')->name('import_csv');
    
});

Route::group(['middleware' => 'pg'], function()
{
    Route::resource('pg/pgdetail', 'pg\PgdetailController',['names'=>['index'=>'pgdetail','store'=>'pgdetails_store',
        'create'=>'floor_store']]);
});

Route::group(['middleware' => 'user'],function()
{
    Route::get('userdashboard','Pages\DashboardController@user_dashboard')->name('userdashboard');
});

Route::group(['middleware' => 'library'], function()
{
    Route::get('library/dashboard',array('as'=>'librarydashboard','uses'=>'Pages\DashboardController@library_dashboard'));
    Route::get('library/libraryview',array('as'=>'library_libraryview','uses'=>'Library\LibraryController@libraryview'));
});

// Auth::routes();
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
